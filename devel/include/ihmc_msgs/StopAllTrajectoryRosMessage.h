// Generated by gencpp from file ihmc_msgs/StopAllTrajectoryRosMessage.msg
// DO NOT EDIT!


#ifndef IHMC_MSGS_MESSAGE_STOPALLTRAJECTORYROSMESSAGE_H
#define IHMC_MSGS_MESSAGE_STOPALLTRAJECTORYROSMESSAGE_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace ihmc_msgs
{
template <class ContainerAllocator>
struct StopAllTrajectoryRosMessage_
{
  typedef StopAllTrajectoryRosMessage_<ContainerAllocator> Type;

  StopAllTrajectoryRosMessage_()
    : unique_id(0)  {
    }
  StopAllTrajectoryRosMessage_(const ContainerAllocator& _alloc)
    : unique_id(0)  {
  (void)_alloc;
    }



   typedef int64_t _unique_id_type;
  _unique_id_type unique_id;




  typedef boost::shared_ptr< ::ihmc_msgs::StopAllTrajectoryRosMessage_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::ihmc_msgs::StopAllTrajectoryRosMessage_<ContainerAllocator> const> ConstPtr;

}; // struct StopAllTrajectoryRosMessage_

typedef ::ihmc_msgs::StopAllTrajectoryRosMessage_<std::allocator<void> > StopAllTrajectoryRosMessage;

typedef boost::shared_ptr< ::ihmc_msgs::StopAllTrajectoryRosMessage > StopAllTrajectoryRosMessagePtr;
typedef boost::shared_ptr< ::ihmc_msgs::StopAllTrajectoryRosMessage const> StopAllTrajectoryRosMessageConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::ihmc_msgs::StopAllTrajectoryRosMessage_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::ihmc_msgs::StopAllTrajectoryRosMessage_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace ihmc_msgs

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': True, 'IsMessage': True, 'HasHeader': False}
// {'ihmc_msgs': ['/home/stiliyan/catkin_ws/src/ihmc_msgs/msg'], 'std_msgs': ['/opt/ros/indigo/share/std_msgs/cmake/../msg'], 'geometry_msgs': ['/opt/ros/indigo/share/geometry_msgs/cmake/../msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::ihmc_msgs::StopAllTrajectoryRosMessage_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::ihmc_msgs::StopAllTrajectoryRosMessage_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::ihmc_msgs::StopAllTrajectoryRosMessage_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::ihmc_msgs::StopAllTrajectoryRosMessage_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::ihmc_msgs::StopAllTrajectoryRosMessage_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::ihmc_msgs::StopAllTrajectoryRosMessage_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::ihmc_msgs::StopAllTrajectoryRosMessage_<ContainerAllocator> >
{
  static const char* value()
  {
    return "72ca681aeeb68b4ac429d65851e2226f";
  }

  static const char* value(const ::ihmc_msgs::StopAllTrajectoryRosMessage_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x72ca681aeeb68b4aULL;
  static const uint64_t static_value2 = 0xc429d65851e2226fULL;
};

template<class ContainerAllocator>
struct DataType< ::ihmc_msgs::StopAllTrajectoryRosMessage_<ContainerAllocator> >
{
  static const char* value()
  {
    return "ihmc_msgs/StopAllTrajectoryRosMessage";
  }

  static const char* value(const ::ihmc_msgs::StopAllTrajectoryRosMessage_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::ihmc_msgs::StopAllTrajectoryRosMessage_<ContainerAllocator> >
{
  static const char* value()
  {
    return "## StopAllTrajectoryRosMessage\n\
# Stop the execution of any trajectory being executed. A message with a unique id equals to 0 will be\n\
# interpreted as invalid and will not be processed by the controller.\n\
\n\
# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id\n\
# in the top level message is used, the unique id in nested messages is ignored. Use\n\
# /output/last_received_message for feedback about when the last message was received. A message with\n\
# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.\n\
int64 unique_id\n\
\n\
\n\
";
  }

  static const char* value(const ::ihmc_msgs::StopAllTrajectoryRosMessage_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::ihmc_msgs::StopAllTrajectoryRosMessage_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.unique_id);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct StopAllTrajectoryRosMessage_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::ihmc_msgs::StopAllTrajectoryRosMessage_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::ihmc_msgs::StopAllTrajectoryRosMessage_<ContainerAllocator>& v)
  {
    s << indent << "unique_id: ";
    Printer<int64_t>::stream(s, indent + "  ", v.unique_id);
  }
};

} // namespace message_operations
} // namespace ros

#endif // IHMC_MSGS_MESSAGE_STOPALLTRAJECTORYROSMESSAGE_H
