; Auto-generated. Do not edit!


(cl:in-package ihmc_msgs-msg)


;//! \htmlinclude FootstepDataListRosMessage.msg.html

(cl:defclass <FootstepDataListRosMessage> (roslisp-msg-protocol:ros-message)
  ((footstep_data_list
    :reader footstep_data_list
    :initarg :footstep_data_list
    :type (cl:vector ihmc_msgs-msg:FootstepDataRosMessage)
   :initform (cl:make-array 0 :element-type 'ihmc_msgs-msg:FootstepDataRosMessage :initial-element (cl:make-instance 'ihmc_msgs-msg:FootstepDataRosMessage)))
   (execution_timing
    :reader execution_timing
    :initarg :execution_timing
    :type cl:fixnum
    :initform 0)
   (default_swing_duration
    :reader default_swing_duration
    :initarg :default_swing_duration
    :type cl:float
    :initform 0.0)
   (default_transfer_duration
    :reader default_transfer_duration
    :initarg :default_transfer_duration
    :type cl:float
    :initform 0.0)
   (final_transfer_duration
    :reader final_transfer_duration
    :initarg :final_transfer_duration
    :type cl:float
    :initform 0.0)
   (execution_mode
    :reader execution_mode
    :initarg :execution_mode
    :type cl:fixnum
    :initform 0)
   (previous_message_id
    :reader previous_message_id
    :initarg :previous_message_id
    :type cl:integer
    :initform 0)
   (unique_id
    :reader unique_id
    :initarg :unique_id
    :type cl:integer
    :initform 0))
)

(cl:defclass FootstepDataListRosMessage (<FootstepDataListRosMessage>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <FootstepDataListRosMessage>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'FootstepDataListRosMessage)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name ihmc_msgs-msg:<FootstepDataListRosMessage> is deprecated: use ihmc_msgs-msg:FootstepDataListRosMessage instead.")))

(cl:ensure-generic-function 'footstep_data_list-val :lambda-list '(m))
(cl:defmethod footstep_data_list-val ((m <FootstepDataListRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:footstep_data_list-val is deprecated.  Use ihmc_msgs-msg:footstep_data_list instead.")
  (footstep_data_list m))

(cl:ensure-generic-function 'execution_timing-val :lambda-list '(m))
(cl:defmethod execution_timing-val ((m <FootstepDataListRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:execution_timing-val is deprecated.  Use ihmc_msgs-msg:execution_timing instead.")
  (execution_timing m))

(cl:ensure-generic-function 'default_swing_duration-val :lambda-list '(m))
(cl:defmethod default_swing_duration-val ((m <FootstepDataListRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:default_swing_duration-val is deprecated.  Use ihmc_msgs-msg:default_swing_duration instead.")
  (default_swing_duration m))

(cl:ensure-generic-function 'default_transfer_duration-val :lambda-list '(m))
(cl:defmethod default_transfer_duration-val ((m <FootstepDataListRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:default_transfer_duration-val is deprecated.  Use ihmc_msgs-msg:default_transfer_duration instead.")
  (default_transfer_duration m))

(cl:ensure-generic-function 'final_transfer_duration-val :lambda-list '(m))
(cl:defmethod final_transfer_duration-val ((m <FootstepDataListRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:final_transfer_duration-val is deprecated.  Use ihmc_msgs-msg:final_transfer_duration instead.")
  (final_transfer_duration m))

(cl:ensure-generic-function 'execution_mode-val :lambda-list '(m))
(cl:defmethod execution_mode-val ((m <FootstepDataListRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:execution_mode-val is deprecated.  Use ihmc_msgs-msg:execution_mode instead.")
  (execution_mode m))

(cl:ensure-generic-function 'previous_message_id-val :lambda-list '(m))
(cl:defmethod previous_message_id-val ((m <FootstepDataListRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:previous_message_id-val is deprecated.  Use ihmc_msgs-msg:previous_message_id instead.")
  (previous_message_id m))

(cl:ensure-generic-function 'unique_id-val :lambda-list '(m))
(cl:defmethod unique_id-val ((m <FootstepDataListRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:unique_id-val is deprecated.  Use ihmc_msgs-msg:unique_id instead.")
  (unique_id m))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<FootstepDataListRosMessage>)))
    "Constants for message type '<FootstepDataListRosMessage>"
  '((:OVERRIDE . 0)
    (:QUEUE . 1)
    (:CONTROL_DURATIONS . 0)
    (:CONTROL_ABSOLUTE_TIMINGS . 1))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'FootstepDataListRosMessage)))
    "Constants for message type 'FootstepDataListRosMessage"
  '((:OVERRIDE . 0)
    (:QUEUE . 1)
    (:CONTROL_DURATIONS . 0)
    (:CONTROL_ABSOLUTE_TIMINGS . 1))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <FootstepDataListRosMessage>) ostream)
  "Serializes a message object of type '<FootstepDataListRosMessage>"
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'footstep_data_list))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'footstep_data_list))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'execution_timing)) ostream)
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'default_swing_duration))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'default_transfer_duration))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'final_transfer_duration))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'execution_mode)) ostream)
  (cl:let* ((signed (cl:slot-value msg 'previous_message_id)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'unique_id)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <FootstepDataListRosMessage>) istream)
  "Deserializes a message object of type '<FootstepDataListRosMessage>"
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'footstep_data_list) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'footstep_data_list)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'ihmc_msgs-msg:FootstepDataRosMessage))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'execution_timing)) (cl:read-byte istream))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'default_swing_duration) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'default_transfer_duration) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'final_transfer_duration) (roslisp-utils:decode-double-float-bits bits)))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'execution_mode)) (cl:read-byte istream))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'previous_message_id) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'unique_id) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<FootstepDataListRosMessage>)))
  "Returns string type for a message object of type '<FootstepDataListRosMessage>"
  "ihmc_msgs/FootstepDataListRosMessage")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'FootstepDataListRosMessage)))
  "Returns string type for a message object of type 'FootstepDataListRosMessage"
  "ihmc_msgs/FootstepDataListRosMessage")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<FootstepDataListRosMessage>)))
  "Returns md5sum for a message object of type '<FootstepDataListRosMessage>"
  "b72083904dfdce377bf72dbfc635d596")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'FootstepDataListRosMessage)))
  "Returns md5sum for a message object of type 'FootstepDataListRosMessage"
  "b72083904dfdce377bf72dbfc635d596")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<FootstepDataListRosMessage>)))
  "Returns full string definition for message of type '<FootstepDataListRosMessage>"
  (cl:format cl:nil "## FootstepDataListRosMessage~%# This message commands the controller to execute a list of footsteps. See FootstepDataMessage for~%# information about defining a footstep. A message with a unique id equals to 0 will be interpreted as~%# invalid and will not be processed by the controller. This rule does not apply to the fields of this~%# message.~%~%# Defines the list of footstep to perform.~%ihmc_msgs/FootstepDataRosMessage[] footstep_data_list~%~%# When CONTROL_DURATIONS is chosen:  The controller will try to achieve the swingDuration and the~%# transferDuration specified in the message. If a  footstep touches down early, the next step will not~%# be affected by this and the whole trajectory might finish  earlier then expected. When~%# CONTROL_ABSOLUTE_TIMINGS is chosen:  The controller will compute the expected times for swing start~%# and touchdown and attempt to start a footstep  at that time. If a footstep touches down early, the~%# following transfer will be extended to make up for this  time difference and the footstep plan will~%# finish at the expected time.~%uint8 execution_timing~%~%# The swingDuration is the time a foot is not in ground contact during a step. Each step in a list of~%# footsteps might have a different swing duration. The value specified here is a default value, used~%# if a footstep in this list was created without a swingDuration.~%float64 default_swing_duration~%~%# The transferDuration is the time spent with the feet in ground contact before a step. Each step in a~%# list of footsteps might have a different transfer duration. The value specified here is a default~%# value, used if a footstep in this list was created without a transferDuration.~%float64 default_transfer_duration~%~%# Specifies the time used to return to a stable standing stance after the execution of the footstep~%# list is finished. If the value is negative the defaultTransferDuration will be used.~%float64 final_transfer_duration~%~%# When OVERRIDE is chosen:  - The time of the first trajectory point can be zero, in which case the~%# controller will start directly at the first trajectory point. Otherwise the controller will prepend~%# a first trajectory point at the current desired position.  When QUEUE is chosen:  - The message must~%# carry the ID of the message it should be queued to.  - The very first message of a list of queued~%# messages has to be an OVERRIDE message.  - The trajectory point times are relative to the the last~%# trajectory point time of the previous message.  - The controller will queue the joint trajectory~%# messages as a per joint basis. The first trajectory point has to be greater than zero.~%uint8 execution_mode~%~%# Only needed when using QUEUE mode, it refers to the message Id to which this message should be~%# queued to. It is used by the controller to ensure that no message has been lost on the way. If a~%# message appears to be missing (previousMessageId different from the last message ID received by the~%# controller), the motion is aborted. If previousMessageId == 0, the controller will not check for the~%# ID of the last received message.~%int64 previous_message_id~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%# This message utilizes \"enums\". Enum value information for this message follows.~%~%# \"execution_mode\" enum values:~%uint8 OVERRIDE=0 # This message will override the previous.~%uint8 QUEUE=1 # The previous message will first be executed before executing this message. When sending a series of queued messages, the very first has to be declared as OVERRIDE.~%~%# \"execution_timing\" enum values:~%uint8 CONTROL_DURATIONS=0 # During the execution of this message the controller will attempt to achieve the given durations for segments of the whole trajectory.~%uint8 CONTROL_ABSOLUTE_TIMINGS=1 # During the execution of this message the controller will attempt to achieve the absolute timings at the knot points relative to the start of execution.~%~%~%================================================================================~%MSG: ihmc_msgs/FootstepDataRosMessage~%## FootstepDataRosMessage~%# This message specifies the position, orientation and side (left or right) of a desired footstep in~%# world frame.~%~%# Specifies which foot will swing to reach the foostep.~%uint8 robot_side~%~%# Specifies the position of the footstep (sole frame) in world frame.~%geometry_msgs/Point location~%~%# Specifies the orientation of the footstep (sole frame) in world frame.~%geometry_msgs/Quaternion orientation~%~%# predictedContactPoints specifies the vertices of the expected contact polygon between the foot and~%# the world. A value of null or an empty list will default to using the entire foot. Contact points~%# are expressed in sole frame. This ordering does not matter. For example: to tell the controller to~%# use the entire foot, the predicted contact points would be: predicted_contact_points: - {x: 0.5 *~%# foot_length, y: -0.5 * toe_width} - {x: 0.5 * foot_length, y: 0.5 * toe_width} - {x: -0.5 *~%# foot_length, y: -0.5 * heel_width} - {x: -0.5 * foot_length, y: 0.5 * heel_width} ~%ihmc_msgs/Point2dRosMessage[] predicted_contact_points~%~%# This contains information on what the swing trajectory should be for each step. Recomended is~%# DEFAULT.~%uint8 trajectory_type~%~%# Contains information on how high the robot should swing its foot. This affects trajectory types~%# DEFAULT and OBSTACLE_CLEARANCE.If a value smaller then the minumal swing height is chosen (e.g. 0.0)~%# the swing height will be changed to a default value.~%float64 swing_height~%~%# In case the trajectory type is set to CUSTOM two swing waypoints can be specified here. The~%# waypoints define sole positions.The controller will compute times and velocities at the waypoints.~%# This is a convinient way to shape the trajectory of the swing. If full control over the~%# swingtrajectory is desired use the trajectory type WAYPOINTS instead. The position waypoints are~%# expected in the trajectory frame.~%geometry_msgs/Point[] position_waypoints~%~%# In case the trajectory type is set to WAYPOINTS, swing waypoints can be specified here. The~%# waypoints do not include thestart point (which is set to the current foot state at lift-off) and the~%# touch down point (which is specified by the location and orientation fields).All waypoints are for~%# the sole frame and expressed in the trajectory frame. The maximum number of points can be found in~%# the Footstep class.~%ihmc_msgs/SE3TrajectoryPointRosMessage[] swing_trajectory~%~%# In case the trajectory type is set to WAYPOINTS, this value can be used to specify the trajectory~%# blend duration  in seconds. If greater than zero, waypoints that fall within the valid time window~%# (beginning at the start of the swing phase and spanning  the desired blend duration) will be~%# adjusted to account for the initial error between the actual and expected position and orientation~%# of the swing foot. Note that the expectedInitialLocation and expectedInitialOrientation fields must~%# be defined in order to enable trajectory blending.~%float64 swing_trajectory_blend_duration~%~%# The swingDuration is the time a foot is not in ground contact during a step. If the value of this~%# field is invalid (not positive) it will be replaced by a default swingDuration.~%float64 swing_duration~%~%# The transferDuration is the time spent with the feet in ground contact before a step. If the value~%# of this field is invalid (not positive) it will be replaced by a default transferDuration.~%float64 transfer_duration~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%# This message utilizes \"enums\". Enum value information for this message follows.~%~%# \"robot_side\" enum values:~%uint8 LEFT=0 # refers to the LEFT side of a robot~%uint8 RIGHT=1 # refers to the RIGHT side of a robot~%~%# \"trajectory_type\" enum values:~%uint8 DEFAULT=0 # The controller will execute a default trajectory.~%uint8 OBSTACLE_CLEARANCE=1 # The controller will attempt to step on/off an obstacle.~%uint8 CUSTOM=2 # In this mode two trajectory position waypoints can be specified.~%uint8 WAYPOINTS=3 # The swing trajectory is fully defined by the given waypoints.~%~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%================================================================================~%MSG: ihmc_msgs/Point2dRosMessage~%## Point2dRosMessage~%# This message represents a point on a 2d plane. The coordinates are referred to as \"x\" and \"y\"~%# The first coordinate of the point in a 2D plane~%float64 x~%~%# The second coordinate of the point in a 2D plane~%float64 y~%~%~%================================================================================~%MSG: ihmc_msgs/SE3TrajectoryPointRosMessage~%## SE3TrajectoryPointRosMessage~%# This class is used to build trajectory messages in taskspace. It holds the necessary information for~%# one trajectory point. Feel free to look at EuclideanTrajectoryPointMessage (translational) and~%# EuclideanTrajectoryPointMessage (rotational)~%~%# Time at which the trajectory point has to be reached. The time is relative to when the trajectory~%# starts.~%float64 time~%~%# Define the desired 3D position to be reached at this trajectory point.~%geometry_msgs/Point position~%~%# Define the desired 3D orientation to be reached at this trajectory point.~%geometry_msgs/Quaternion orientation~%~%# Define the desired 3D linear velocity to be reached at this trajectory point.~%geometry_msgs/Vector3 linear_velocity~%~%# Define the desired 3D angular velocity to be reached at this trajectory point.~%geometry_msgs/Vector3 angular_velocity~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%# It is only meant to represent a direction. Therefore, it does not~%# make sense to apply a translation to it (e.g., when applying a ~%# generic rigid transformation to a Vector3, tf2 will only apply the~%# rotation). If you want your data to be translatable too, use the~%# geometry_msgs/Point message instead.~%~%float64 x~%float64 y~%float64 z~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'FootstepDataListRosMessage)))
  "Returns full string definition for message of type 'FootstepDataListRosMessage"
  (cl:format cl:nil "## FootstepDataListRosMessage~%# This message commands the controller to execute a list of footsteps. See FootstepDataMessage for~%# information about defining a footstep. A message with a unique id equals to 0 will be interpreted as~%# invalid and will not be processed by the controller. This rule does not apply to the fields of this~%# message.~%~%# Defines the list of footstep to perform.~%ihmc_msgs/FootstepDataRosMessage[] footstep_data_list~%~%# When CONTROL_DURATIONS is chosen:  The controller will try to achieve the swingDuration and the~%# transferDuration specified in the message. If a  footstep touches down early, the next step will not~%# be affected by this and the whole trajectory might finish  earlier then expected. When~%# CONTROL_ABSOLUTE_TIMINGS is chosen:  The controller will compute the expected times for swing start~%# and touchdown and attempt to start a footstep  at that time. If a footstep touches down early, the~%# following transfer will be extended to make up for this  time difference and the footstep plan will~%# finish at the expected time.~%uint8 execution_timing~%~%# The swingDuration is the time a foot is not in ground contact during a step. Each step in a list of~%# footsteps might have a different swing duration. The value specified here is a default value, used~%# if a footstep in this list was created without a swingDuration.~%float64 default_swing_duration~%~%# The transferDuration is the time spent with the feet in ground contact before a step. Each step in a~%# list of footsteps might have a different transfer duration. The value specified here is a default~%# value, used if a footstep in this list was created without a transferDuration.~%float64 default_transfer_duration~%~%# Specifies the time used to return to a stable standing stance after the execution of the footstep~%# list is finished. If the value is negative the defaultTransferDuration will be used.~%float64 final_transfer_duration~%~%# When OVERRIDE is chosen:  - The time of the first trajectory point can be zero, in which case the~%# controller will start directly at the first trajectory point. Otherwise the controller will prepend~%# a first trajectory point at the current desired position.  When QUEUE is chosen:  - The message must~%# carry the ID of the message it should be queued to.  - The very first message of a list of queued~%# messages has to be an OVERRIDE message.  - The trajectory point times are relative to the the last~%# trajectory point time of the previous message.  - The controller will queue the joint trajectory~%# messages as a per joint basis. The first trajectory point has to be greater than zero.~%uint8 execution_mode~%~%# Only needed when using QUEUE mode, it refers to the message Id to which this message should be~%# queued to. It is used by the controller to ensure that no message has been lost on the way. If a~%# message appears to be missing (previousMessageId different from the last message ID received by the~%# controller), the motion is aborted. If previousMessageId == 0, the controller will not check for the~%# ID of the last received message.~%int64 previous_message_id~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%# This message utilizes \"enums\". Enum value information for this message follows.~%~%# \"execution_mode\" enum values:~%uint8 OVERRIDE=0 # This message will override the previous.~%uint8 QUEUE=1 # The previous message will first be executed before executing this message. When sending a series of queued messages, the very first has to be declared as OVERRIDE.~%~%# \"execution_timing\" enum values:~%uint8 CONTROL_DURATIONS=0 # During the execution of this message the controller will attempt to achieve the given durations for segments of the whole trajectory.~%uint8 CONTROL_ABSOLUTE_TIMINGS=1 # During the execution of this message the controller will attempt to achieve the absolute timings at the knot points relative to the start of execution.~%~%~%================================================================================~%MSG: ihmc_msgs/FootstepDataRosMessage~%## FootstepDataRosMessage~%# This message specifies the position, orientation and side (left or right) of a desired footstep in~%# world frame.~%~%# Specifies which foot will swing to reach the foostep.~%uint8 robot_side~%~%# Specifies the position of the footstep (sole frame) in world frame.~%geometry_msgs/Point location~%~%# Specifies the orientation of the footstep (sole frame) in world frame.~%geometry_msgs/Quaternion orientation~%~%# predictedContactPoints specifies the vertices of the expected contact polygon between the foot and~%# the world. A value of null or an empty list will default to using the entire foot. Contact points~%# are expressed in sole frame. This ordering does not matter. For example: to tell the controller to~%# use the entire foot, the predicted contact points would be: predicted_contact_points: - {x: 0.5 *~%# foot_length, y: -0.5 * toe_width} - {x: 0.5 * foot_length, y: 0.5 * toe_width} - {x: -0.5 *~%# foot_length, y: -0.5 * heel_width} - {x: -0.5 * foot_length, y: 0.5 * heel_width} ~%ihmc_msgs/Point2dRosMessage[] predicted_contact_points~%~%# This contains information on what the swing trajectory should be for each step. Recomended is~%# DEFAULT.~%uint8 trajectory_type~%~%# Contains information on how high the robot should swing its foot. This affects trajectory types~%# DEFAULT and OBSTACLE_CLEARANCE.If a value smaller then the minumal swing height is chosen (e.g. 0.0)~%# the swing height will be changed to a default value.~%float64 swing_height~%~%# In case the trajectory type is set to CUSTOM two swing waypoints can be specified here. The~%# waypoints define sole positions.The controller will compute times and velocities at the waypoints.~%# This is a convinient way to shape the trajectory of the swing. If full control over the~%# swingtrajectory is desired use the trajectory type WAYPOINTS instead. The position waypoints are~%# expected in the trajectory frame.~%geometry_msgs/Point[] position_waypoints~%~%# In case the trajectory type is set to WAYPOINTS, swing waypoints can be specified here. The~%# waypoints do not include thestart point (which is set to the current foot state at lift-off) and the~%# touch down point (which is specified by the location and orientation fields).All waypoints are for~%# the sole frame and expressed in the trajectory frame. The maximum number of points can be found in~%# the Footstep class.~%ihmc_msgs/SE3TrajectoryPointRosMessage[] swing_trajectory~%~%# In case the trajectory type is set to WAYPOINTS, this value can be used to specify the trajectory~%# blend duration  in seconds. If greater than zero, waypoints that fall within the valid time window~%# (beginning at the start of the swing phase and spanning  the desired blend duration) will be~%# adjusted to account for the initial error between the actual and expected position and orientation~%# of the swing foot. Note that the expectedInitialLocation and expectedInitialOrientation fields must~%# be defined in order to enable trajectory blending.~%float64 swing_trajectory_blend_duration~%~%# The swingDuration is the time a foot is not in ground contact during a step. If the value of this~%# field is invalid (not positive) it will be replaced by a default swingDuration.~%float64 swing_duration~%~%# The transferDuration is the time spent with the feet in ground contact before a step. If the value~%# of this field is invalid (not positive) it will be replaced by a default transferDuration.~%float64 transfer_duration~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%# This message utilizes \"enums\". Enum value information for this message follows.~%~%# \"robot_side\" enum values:~%uint8 LEFT=0 # refers to the LEFT side of a robot~%uint8 RIGHT=1 # refers to the RIGHT side of a robot~%~%# \"trajectory_type\" enum values:~%uint8 DEFAULT=0 # The controller will execute a default trajectory.~%uint8 OBSTACLE_CLEARANCE=1 # The controller will attempt to step on/off an obstacle.~%uint8 CUSTOM=2 # In this mode two trajectory position waypoints can be specified.~%uint8 WAYPOINTS=3 # The swing trajectory is fully defined by the given waypoints.~%~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%================================================================================~%MSG: ihmc_msgs/Point2dRosMessage~%## Point2dRosMessage~%# This message represents a point on a 2d plane. The coordinates are referred to as \"x\" and \"y\"~%# The first coordinate of the point in a 2D plane~%float64 x~%~%# The second coordinate of the point in a 2D plane~%float64 y~%~%~%================================================================================~%MSG: ihmc_msgs/SE3TrajectoryPointRosMessage~%## SE3TrajectoryPointRosMessage~%# This class is used to build trajectory messages in taskspace. It holds the necessary information for~%# one trajectory point. Feel free to look at EuclideanTrajectoryPointMessage (translational) and~%# EuclideanTrajectoryPointMessage (rotational)~%~%# Time at which the trajectory point has to be reached. The time is relative to when the trajectory~%# starts.~%float64 time~%~%# Define the desired 3D position to be reached at this trajectory point.~%geometry_msgs/Point position~%~%# Define the desired 3D orientation to be reached at this trajectory point.~%geometry_msgs/Quaternion orientation~%~%# Define the desired 3D linear velocity to be reached at this trajectory point.~%geometry_msgs/Vector3 linear_velocity~%~%# Define the desired 3D angular velocity to be reached at this trajectory point.~%geometry_msgs/Vector3 angular_velocity~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%# It is only meant to represent a direction. Therefore, it does not~%# make sense to apply a translation to it (e.g., when applying a ~%# generic rigid transformation to a Vector3, tf2 will only apply the~%# rotation). If you want your data to be translatable too, use the~%# geometry_msgs/Point message instead.~%~%float64 x~%float64 y~%float64 z~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <FootstepDataListRosMessage>))
  (cl:+ 0
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'footstep_data_list) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
     1
     8
     8
     8
     1
     8
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <FootstepDataListRosMessage>))
  "Converts a ROS message object to a list"
  (cl:list 'FootstepDataListRosMessage
    (cl:cons ':footstep_data_list (footstep_data_list msg))
    (cl:cons ':execution_timing (execution_timing msg))
    (cl:cons ':default_swing_duration (default_swing_duration msg))
    (cl:cons ':default_transfer_duration (default_transfer_duration msg))
    (cl:cons ':final_transfer_duration (final_transfer_duration msg))
    (cl:cons ':execution_mode (execution_mode msg))
    (cl:cons ':previous_message_id (previous_message_id msg))
    (cl:cons ':unique_id (unique_id msg))
))
