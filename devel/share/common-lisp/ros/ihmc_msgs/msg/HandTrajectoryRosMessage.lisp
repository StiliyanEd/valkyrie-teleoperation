; Auto-generated. Do not edit!


(cl:in-package ihmc_msgs-msg)


;//! \htmlinclude HandTrajectoryRosMessage.msg.html

(cl:defclass <HandTrajectoryRosMessage> (roslisp-msg-protocol:ros-message)
  ((robot_side
    :reader robot_side
    :initarg :robot_side
    :type cl:fixnum
    :initform 0)
   (taskspace_trajectory_points
    :reader taskspace_trajectory_points
    :initarg :taskspace_trajectory_points
    :type (cl:vector ihmc_msgs-msg:SE3TrajectoryPointRosMessage)
   :initform (cl:make-array 0 :element-type 'ihmc_msgs-msg:SE3TrajectoryPointRosMessage :initial-element (cl:make-instance 'ihmc_msgs-msg:SE3TrajectoryPointRosMessage)))
   (frame_information
    :reader frame_information
    :initarg :frame_information
    :type ihmc_msgs-msg:FrameInformationRosMessage
    :initform (cl:make-instance 'ihmc_msgs-msg:FrameInformationRosMessage))
   (use_custom_control_frame
    :reader use_custom_control_frame
    :initarg :use_custom_control_frame
    :type cl:boolean
    :initform cl:nil)
   (control_frame_pose
    :reader control_frame_pose
    :initarg :control_frame_pose
    :type geometry_msgs-msg:Transform
    :initform (cl:make-instance 'geometry_msgs-msg:Transform))
   (execution_mode
    :reader execution_mode
    :initarg :execution_mode
    :type cl:fixnum
    :initform 0)
   (previous_message_id
    :reader previous_message_id
    :initarg :previous_message_id
    :type cl:integer
    :initform 0)
   (unique_id
    :reader unique_id
    :initarg :unique_id
    :type cl:integer
    :initform 0))
)

(cl:defclass HandTrajectoryRosMessage (<HandTrajectoryRosMessage>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <HandTrajectoryRosMessage>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'HandTrajectoryRosMessage)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name ihmc_msgs-msg:<HandTrajectoryRosMessage> is deprecated: use ihmc_msgs-msg:HandTrajectoryRosMessage instead.")))

(cl:ensure-generic-function 'robot_side-val :lambda-list '(m))
(cl:defmethod robot_side-val ((m <HandTrajectoryRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:robot_side-val is deprecated.  Use ihmc_msgs-msg:robot_side instead.")
  (robot_side m))

(cl:ensure-generic-function 'taskspace_trajectory_points-val :lambda-list '(m))
(cl:defmethod taskspace_trajectory_points-val ((m <HandTrajectoryRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:taskspace_trajectory_points-val is deprecated.  Use ihmc_msgs-msg:taskspace_trajectory_points instead.")
  (taskspace_trajectory_points m))

(cl:ensure-generic-function 'frame_information-val :lambda-list '(m))
(cl:defmethod frame_information-val ((m <HandTrajectoryRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:frame_information-val is deprecated.  Use ihmc_msgs-msg:frame_information instead.")
  (frame_information m))

(cl:ensure-generic-function 'use_custom_control_frame-val :lambda-list '(m))
(cl:defmethod use_custom_control_frame-val ((m <HandTrajectoryRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:use_custom_control_frame-val is deprecated.  Use ihmc_msgs-msg:use_custom_control_frame instead.")
  (use_custom_control_frame m))

(cl:ensure-generic-function 'control_frame_pose-val :lambda-list '(m))
(cl:defmethod control_frame_pose-val ((m <HandTrajectoryRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:control_frame_pose-val is deprecated.  Use ihmc_msgs-msg:control_frame_pose instead.")
  (control_frame_pose m))

(cl:ensure-generic-function 'execution_mode-val :lambda-list '(m))
(cl:defmethod execution_mode-val ((m <HandTrajectoryRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:execution_mode-val is deprecated.  Use ihmc_msgs-msg:execution_mode instead.")
  (execution_mode m))

(cl:ensure-generic-function 'previous_message_id-val :lambda-list '(m))
(cl:defmethod previous_message_id-val ((m <HandTrajectoryRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:previous_message_id-val is deprecated.  Use ihmc_msgs-msg:previous_message_id instead.")
  (previous_message_id m))

(cl:ensure-generic-function 'unique_id-val :lambda-list '(m))
(cl:defmethod unique_id-val ((m <HandTrajectoryRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:unique_id-val is deprecated.  Use ihmc_msgs-msg:unique_id instead.")
  (unique_id m))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<HandTrajectoryRosMessage>)))
    "Constants for message type '<HandTrajectoryRosMessage>"
  '((:OVERRIDE . 0)
    (:QUEUE . 1)
    (:LEFT . 0)
    (:RIGHT . 1))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'HandTrajectoryRosMessage)))
    "Constants for message type 'HandTrajectoryRosMessage"
  '((:OVERRIDE . 0)
    (:QUEUE . 1)
    (:LEFT . 0)
    (:RIGHT . 1))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <HandTrajectoryRosMessage>) ostream)
  "Serializes a message object of type '<HandTrajectoryRosMessage>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'robot_side)) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'taskspace_trajectory_points))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'taskspace_trajectory_points))
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'frame_information) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'use_custom_control_frame) 1 0)) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'control_frame_pose) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'execution_mode)) ostream)
  (cl:let* ((signed (cl:slot-value msg 'previous_message_id)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'unique_id)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <HandTrajectoryRosMessage>) istream)
  "Deserializes a message object of type '<HandTrajectoryRosMessage>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'robot_side)) (cl:read-byte istream))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'taskspace_trajectory_points) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'taskspace_trajectory_points)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'ihmc_msgs-msg:SE3TrajectoryPointRosMessage))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'frame_information) istream)
    (cl:setf (cl:slot-value msg 'use_custom_control_frame) (cl:not (cl:zerop (cl:read-byte istream))))
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'control_frame_pose) istream)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'execution_mode)) (cl:read-byte istream))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'previous_message_id) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'unique_id) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<HandTrajectoryRosMessage>)))
  "Returns string type for a message object of type '<HandTrajectoryRosMessage>"
  "ihmc_msgs/HandTrajectoryRosMessage")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'HandTrajectoryRosMessage)))
  "Returns string type for a message object of type 'HandTrajectoryRosMessage"
  "ihmc_msgs/HandTrajectoryRosMessage")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<HandTrajectoryRosMessage>)))
  "Returns md5sum for a message object of type '<HandTrajectoryRosMessage>"
  "ad5b46b6b3de85a658f75c1495262d3a")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'HandTrajectoryRosMessage)))
  "Returns md5sum for a message object of type 'HandTrajectoryRosMessage"
  "ad5b46b6b3de85a658f75c1495262d3a")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<HandTrajectoryRosMessage>)))
  "Returns full string definition for message of type '<HandTrajectoryRosMessage>"
  (cl:format cl:nil "## HandTrajectoryRosMessage~%# This message commands the controller to move in taskspace a hand to the desired pose (position &~%# orientation) while going through the specified trajectory points. A third order polynomial function~%# is used to interpolate positions and a hermite based curve (third order) is used to interpolate the~%# orientations. To excute a single straight line trajectory to reach a desired hand pose, set only one~%# trajectory point with zero velocity and its time to be equal to the desired trajectory time. A~%# message with a unique id equals to 0 will be interpreted as invalid and will not be processed by the~%# controller. This rule does not apply to the fields of this message.~%~%# Specifies which hand will execute the trajectory.~%uint8 robot_side~%~%# List of trajectory points (in taskpsace) to go through while executing the trajectory. All the~%# information contained in these trajectory points needs to be expressed in world frame.~%ihmc_msgs/SE3TrajectoryPointRosMessage[] taskspace_trajectory_points~%~%# Frame information for this message.~%ihmc_msgs/FrameInformationRosMessage frame_information~%~%# Flag that tells the controller whether the use of a custom control frame is requested.~%bool use_custom_control_frame~%~%# Pose of custom control frame. This is the frame attached to the rigid body that the taskspace~%# trajectory is defined for.~%geometry_msgs/Transform control_frame_pose~%~%# When OVERRIDE is chosen:  - The time of the first trajectory point can be zero, in which case the~%# controller will start directly at the first trajectory point. Otherwise the controller will prepend~%# a first trajectory point at the current desired position.  When QUEUE is chosen:  - The message must~%# carry the ID of the message it should be queued to.  - The very first message of a list of queued~%# messages has to be an OVERRIDE message.  - The trajectory point times are relative to the the last~%# trajectory point time of the previous message.  - The controller will queue the joint trajectory~%# messages as a per joint basis. The first trajectory point has to be greater than zero.~%uint8 execution_mode~%~%# Only needed when using QUEUE mode, it refers to the message Id to which this message should be~%# queued to. It is used by the controller to ensure that no message has been lost on the way. If a~%# message appears to be missing (previousMessageId different from the last message ID received by the~%# controller), the motion is aborted. If previousMessageId == 0, the controller will not check for the~%# ID of the last received message.~%int64 previous_message_id~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%# This message utilizes \"enums\". Enum value information for this message follows.~%~%# \"execution_mode\" enum values:~%uint8 OVERRIDE=0 # This message will override the previous.~%uint8 QUEUE=1 # The previous message will first be executed before executing this message. When sending a series of queued messages, the very first has to be declared as OVERRIDE.~%~%# \"robot_side\" enum values:~%uint8 LEFT=0 # refers to the LEFT side of a robot~%uint8 RIGHT=1 # refers to the RIGHT side of a robot~%~%~%================================================================================~%MSG: ihmc_msgs/SE3TrajectoryPointRosMessage~%## SE3TrajectoryPointRosMessage~%# This class is used to build trajectory messages in taskspace. It holds the necessary information for~%# one trajectory point. Feel free to look at EuclideanTrajectoryPointMessage (translational) and~%# EuclideanTrajectoryPointMessage (rotational)~%~%# Time at which the trajectory point has to be reached. The time is relative to when the trajectory~%# starts.~%float64 time~%~%# Define the desired 3D position to be reached at this trajectory point.~%geometry_msgs/Point position~%~%# Define the desired 3D orientation to be reached at this trajectory point.~%geometry_msgs/Quaternion orientation~%~%# Define the desired 3D linear velocity to be reached at this trajectory point.~%geometry_msgs/Vector3 linear_velocity~%~%# Define the desired 3D angular velocity to be reached at this trajectory point.~%geometry_msgs/Vector3 angular_velocity~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%# It is only meant to represent a direction. Therefore, it does not~%# make sense to apply a translation to it (e.g., when applying a ~%# generic rigid transformation to a Vector3, tf2 will only apply the~%# rotation). If you want your data to be translatable too, use the~%# geometry_msgs/Point message instead.~%~%float64 x~%float64 y~%float64 z~%================================================================================~%MSG: ihmc_msgs/FrameInformationRosMessage~%## FrameInformationRosMessage~%# This is a holder for frame related information. Valid codes and their associated frames include:~%# MIDFEET_ZUP_FRAME = -100 PELVIS_ZUP_FRAME = -101 PELVIS_FRAME = -102 CHEST_FRAME = -103~%# CENTER_OF_MASS_FRAME = -104 LEFT_SOLE_FRAME = -105 RIGHT_SOLE_FRAME = -106~%~%# The ID of the reference frame that a trajectory is executed in.~%int64 trajectory_reference_frame_id~%~%# The ID of the reference frame that trajectory data in a packet is expressed in. The frame of the~%# trajectory data will be switched to the trajectory frame immediately when the message is received by~%# the controller.~%int64 data_reference_frame_id~%~%~%~%================================================================================~%MSG: geometry_msgs/Transform~%# This represents the transform between two coordinate frames in free space.~%~%Vector3 translation~%Quaternion rotation~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'HandTrajectoryRosMessage)))
  "Returns full string definition for message of type 'HandTrajectoryRosMessage"
  (cl:format cl:nil "## HandTrajectoryRosMessage~%# This message commands the controller to move in taskspace a hand to the desired pose (position &~%# orientation) while going through the specified trajectory points. A third order polynomial function~%# is used to interpolate positions and a hermite based curve (third order) is used to interpolate the~%# orientations. To excute a single straight line trajectory to reach a desired hand pose, set only one~%# trajectory point with zero velocity and its time to be equal to the desired trajectory time. A~%# message with a unique id equals to 0 will be interpreted as invalid and will not be processed by the~%# controller. This rule does not apply to the fields of this message.~%~%# Specifies which hand will execute the trajectory.~%uint8 robot_side~%~%# List of trajectory points (in taskpsace) to go through while executing the trajectory. All the~%# information contained in these trajectory points needs to be expressed in world frame.~%ihmc_msgs/SE3TrajectoryPointRosMessage[] taskspace_trajectory_points~%~%# Frame information for this message.~%ihmc_msgs/FrameInformationRosMessage frame_information~%~%# Flag that tells the controller whether the use of a custom control frame is requested.~%bool use_custom_control_frame~%~%# Pose of custom control frame. This is the frame attached to the rigid body that the taskspace~%# trajectory is defined for.~%geometry_msgs/Transform control_frame_pose~%~%# When OVERRIDE is chosen:  - The time of the first trajectory point can be zero, in which case the~%# controller will start directly at the first trajectory point. Otherwise the controller will prepend~%# a first trajectory point at the current desired position.  When QUEUE is chosen:  - The message must~%# carry the ID of the message it should be queued to.  - The very first message of a list of queued~%# messages has to be an OVERRIDE message.  - The trajectory point times are relative to the the last~%# trajectory point time of the previous message.  - The controller will queue the joint trajectory~%# messages as a per joint basis. The first trajectory point has to be greater than zero.~%uint8 execution_mode~%~%# Only needed when using QUEUE mode, it refers to the message Id to which this message should be~%# queued to. It is used by the controller to ensure that no message has been lost on the way. If a~%# message appears to be missing (previousMessageId different from the last message ID received by the~%# controller), the motion is aborted. If previousMessageId == 0, the controller will not check for the~%# ID of the last received message.~%int64 previous_message_id~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%# This message utilizes \"enums\". Enum value information for this message follows.~%~%# \"execution_mode\" enum values:~%uint8 OVERRIDE=0 # This message will override the previous.~%uint8 QUEUE=1 # The previous message will first be executed before executing this message. When sending a series of queued messages, the very first has to be declared as OVERRIDE.~%~%# \"robot_side\" enum values:~%uint8 LEFT=0 # refers to the LEFT side of a robot~%uint8 RIGHT=1 # refers to the RIGHT side of a robot~%~%~%================================================================================~%MSG: ihmc_msgs/SE3TrajectoryPointRosMessage~%## SE3TrajectoryPointRosMessage~%# This class is used to build trajectory messages in taskspace. It holds the necessary information for~%# one trajectory point. Feel free to look at EuclideanTrajectoryPointMessage (translational) and~%# EuclideanTrajectoryPointMessage (rotational)~%~%# Time at which the trajectory point has to be reached. The time is relative to when the trajectory~%# starts.~%float64 time~%~%# Define the desired 3D position to be reached at this trajectory point.~%geometry_msgs/Point position~%~%# Define the desired 3D orientation to be reached at this trajectory point.~%geometry_msgs/Quaternion orientation~%~%# Define the desired 3D linear velocity to be reached at this trajectory point.~%geometry_msgs/Vector3 linear_velocity~%~%# Define the desired 3D angular velocity to be reached at this trajectory point.~%geometry_msgs/Vector3 angular_velocity~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%# It is only meant to represent a direction. Therefore, it does not~%# make sense to apply a translation to it (e.g., when applying a ~%# generic rigid transformation to a Vector3, tf2 will only apply the~%# rotation). If you want your data to be translatable too, use the~%# geometry_msgs/Point message instead.~%~%float64 x~%float64 y~%float64 z~%================================================================================~%MSG: ihmc_msgs/FrameInformationRosMessage~%## FrameInformationRosMessage~%# This is a holder for frame related information. Valid codes and their associated frames include:~%# MIDFEET_ZUP_FRAME = -100 PELVIS_ZUP_FRAME = -101 PELVIS_FRAME = -102 CHEST_FRAME = -103~%# CENTER_OF_MASS_FRAME = -104 LEFT_SOLE_FRAME = -105 RIGHT_SOLE_FRAME = -106~%~%# The ID of the reference frame that a trajectory is executed in.~%int64 trajectory_reference_frame_id~%~%# The ID of the reference frame that trajectory data in a packet is expressed in. The frame of the~%# trajectory data will be switched to the trajectory frame immediately when the message is received by~%# the controller.~%int64 data_reference_frame_id~%~%~%~%================================================================================~%MSG: geometry_msgs/Transform~%# This represents the transform between two coordinate frames in free space.~%~%Vector3 translation~%Quaternion rotation~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <HandTrajectoryRosMessage>))
  (cl:+ 0
     1
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'taskspace_trajectory_points) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'frame_information))
     1
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'control_frame_pose))
     1
     8
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <HandTrajectoryRosMessage>))
  "Converts a ROS message object to a list"
  (cl:list 'HandTrajectoryRosMessage
    (cl:cons ':robot_side (robot_side msg))
    (cl:cons ':taskspace_trajectory_points (taskspace_trajectory_points msg))
    (cl:cons ':frame_information (frame_information msg))
    (cl:cons ':use_custom_control_frame (use_custom_control_frame msg))
    (cl:cons ':control_frame_pose (control_frame_pose msg))
    (cl:cons ':execution_mode (execution_mode msg))
    (cl:cons ':previous_message_id (previous_message_id msg))
    (cl:cons ':unique_id (unique_id msg))
))
