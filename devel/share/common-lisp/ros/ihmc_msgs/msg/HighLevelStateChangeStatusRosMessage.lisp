; Auto-generated. Do not edit!


(cl:in-package ihmc_msgs-msg)


;//! \htmlinclude HighLevelStateChangeStatusRosMessage.msg.html

(cl:defclass <HighLevelStateChangeStatusRosMessage> (roslisp-msg-protocol:ros-message)
  ((initial_state
    :reader initial_state
    :initarg :initial_state
    :type cl:fixnum
    :initform 0)
   (end_state
    :reader end_state
    :initarg :end_state
    :type cl:fixnum
    :initform 0)
   (unique_id
    :reader unique_id
    :initarg :unique_id
    :type cl:integer
    :initform 0))
)

(cl:defclass HighLevelStateChangeStatusRosMessage (<HighLevelStateChangeStatusRosMessage>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <HighLevelStateChangeStatusRosMessage>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'HighLevelStateChangeStatusRosMessage)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name ihmc_msgs-msg:<HighLevelStateChangeStatusRosMessage> is deprecated: use ihmc_msgs-msg:HighLevelStateChangeStatusRosMessage instead.")))

(cl:ensure-generic-function 'initial_state-val :lambda-list '(m))
(cl:defmethod initial_state-val ((m <HighLevelStateChangeStatusRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:initial_state-val is deprecated.  Use ihmc_msgs-msg:initial_state instead.")
  (initial_state m))

(cl:ensure-generic-function 'end_state-val :lambda-list '(m))
(cl:defmethod end_state-val ((m <HighLevelStateChangeStatusRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:end_state-val is deprecated.  Use ihmc_msgs-msg:end_state instead.")
  (end_state m))

(cl:ensure-generic-function 'unique_id-val :lambda-list '(m))
(cl:defmethod unique_id-val ((m <HighLevelStateChangeStatusRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:unique_id-val is deprecated.  Use ihmc_msgs-msg:unique_id instead.")
  (unique_id m))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<HighLevelStateChangeStatusRosMessage>)))
    "Constants for message type '<HighLevelStateChangeStatusRosMessage>"
  '((:WALKING . 0)
    (:DO_NOTHING_BEHAVIOR . 1)
    (:DIAGNOSTICS . 2)
    (:CALIBRATION . 3))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'HighLevelStateChangeStatusRosMessage)))
    "Constants for message type 'HighLevelStateChangeStatusRosMessage"
  '((:WALKING . 0)
    (:DO_NOTHING_BEHAVIOR . 1)
    (:DIAGNOSTICS . 2)
    (:CALIBRATION . 3))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <HighLevelStateChangeStatusRosMessage>) ostream)
  "Serializes a message object of type '<HighLevelStateChangeStatusRosMessage>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'initial_state)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'end_state)) ostream)
  (cl:let* ((signed (cl:slot-value msg 'unique_id)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <HighLevelStateChangeStatusRosMessage>) istream)
  "Deserializes a message object of type '<HighLevelStateChangeStatusRosMessage>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'initial_state)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'end_state)) (cl:read-byte istream))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'unique_id) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<HighLevelStateChangeStatusRosMessage>)))
  "Returns string type for a message object of type '<HighLevelStateChangeStatusRosMessage>"
  "ihmc_msgs/HighLevelStateChangeStatusRosMessage")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'HighLevelStateChangeStatusRosMessage)))
  "Returns string type for a message object of type 'HighLevelStateChangeStatusRosMessage"
  "ihmc_msgs/HighLevelStateChangeStatusRosMessage")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<HighLevelStateChangeStatusRosMessage>)))
  "Returns md5sum for a message object of type '<HighLevelStateChangeStatusRosMessage>"
  "abe0f16b5272df7cab1198f126f05a9f")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'HighLevelStateChangeStatusRosMessage)))
  "Returns md5sum for a message object of type 'HighLevelStateChangeStatusRosMessage"
  "abe0f16b5272df7cab1198f126f05a9f")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<HighLevelStateChangeStatusRosMessage>)))
  "Returns full string definition for message of type '<HighLevelStateChangeStatusRosMessage>"
  (cl:format cl:nil "## HighLevelStateChangeStatusRosMessage~%# This message notifies the user of a change in the high level state. This message's primary use is to~%# signal a requested state change is completed.~%~%# initialState gives the controller's state prior to transition~%uint8 initial_state~%~%# endState gives the state the controller has transitioned into~%uint8 end_state~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%# This message utilizes \"enums\". Enum value information for this message follows.~%~%# \"high_level_state\" enum values:~%uint8 WALKING=0 # whole body force control employing IHMC walking, balance, and manipulation algorithms~%uint8 DO_NOTHING_BEHAVIOR=1 # do nothing behavior. the robot will start in this behavior, and report this behavior when falling and ramping down the controller. This behavior is intended for feedback only. Requesting this behavior is not supported and can cause the robot to shut down.~%uint8 DIAGNOSTICS=2 # The robot is peforming an automated diagnostic routine~%uint8 CALIBRATION=3 # Automated calibration routine depending on the robot. For Valkyrie: estimation of the joint torque offsets.~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'HighLevelStateChangeStatusRosMessage)))
  "Returns full string definition for message of type 'HighLevelStateChangeStatusRosMessage"
  (cl:format cl:nil "## HighLevelStateChangeStatusRosMessage~%# This message notifies the user of a change in the high level state. This message's primary use is to~%# signal a requested state change is completed.~%~%# initialState gives the controller's state prior to transition~%uint8 initial_state~%~%# endState gives the state the controller has transitioned into~%uint8 end_state~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%# This message utilizes \"enums\". Enum value information for this message follows.~%~%# \"high_level_state\" enum values:~%uint8 WALKING=0 # whole body force control employing IHMC walking, balance, and manipulation algorithms~%uint8 DO_NOTHING_BEHAVIOR=1 # do nothing behavior. the robot will start in this behavior, and report this behavior when falling and ramping down the controller. This behavior is intended for feedback only. Requesting this behavior is not supported and can cause the robot to shut down.~%uint8 DIAGNOSTICS=2 # The robot is peforming an automated diagnostic routine~%uint8 CALIBRATION=3 # Automated calibration routine depending on the robot. For Valkyrie: estimation of the joint torque offsets.~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <HighLevelStateChangeStatusRosMessage>))
  (cl:+ 0
     1
     1
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <HighLevelStateChangeStatusRosMessage>))
  "Converts a ROS message object to a list"
  (cl:list 'HighLevelStateChangeStatusRosMessage
    (cl:cons ':initial_state (initial_state msg))
    (cl:cons ':end_state (end_state msg))
    (cl:cons ':unique_id (unique_id msg))
))
