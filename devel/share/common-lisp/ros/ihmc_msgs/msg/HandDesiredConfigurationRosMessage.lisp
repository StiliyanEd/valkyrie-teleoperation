; Auto-generated. Do not edit!


(cl:in-package ihmc_msgs-msg)


;//! \htmlinclude HandDesiredConfigurationRosMessage.msg.html

(cl:defclass <HandDesiredConfigurationRosMessage> (roslisp-msg-protocol:ros-message)
  ((robot_side
    :reader robot_side
    :initarg :robot_side
    :type cl:fixnum
    :initform 0)
   (hand_desired_configuration
    :reader hand_desired_configuration
    :initarg :hand_desired_configuration
    :type cl:fixnum
    :initform 0)
   (unique_id
    :reader unique_id
    :initarg :unique_id
    :type cl:integer
    :initform 0))
)

(cl:defclass HandDesiredConfigurationRosMessage (<HandDesiredConfigurationRosMessage>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <HandDesiredConfigurationRosMessage>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'HandDesiredConfigurationRosMessage)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name ihmc_msgs-msg:<HandDesiredConfigurationRosMessage> is deprecated: use ihmc_msgs-msg:HandDesiredConfigurationRosMessage instead.")))

(cl:ensure-generic-function 'robot_side-val :lambda-list '(m))
(cl:defmethod robot_side-val ((m <HandDesiredConfigurationRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:robot_side-val is deprecated.  Use ihmc_msgs-msg:robot_side instead.")
  (robot_side m))

(cl:ensure-generic-function 'hand_desired_configuration-val :lambda-list '(m))
(cl:defmethod hand_desired_configuration-val ((m <HandDesiredConfigurationRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:hand_desired_configuration-val is deprecated.  Use ihmc_msgs-msg:hand_desired_configuration instead.")
  (hand_desired_configuration m))

(cl:ensure-generic-function 'unique_id-val :lambda-list '(m))
(cl:defmethod unique_id-val ((m <HandDesiredConfigurationRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:unique_id-val is deprecated.  Use ihmc_msgs-msg:unique_id instead.")
  (unique_id m))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<HandDesiredConfigurationRosMessage>)))
    "Constants for message type '<HandDesiredConfigurationRosMessage>"
  '((:STOP . 0)
    (:OPEN . 1)
    (:CLOSE . 2)
    (:CRUSH . 3)
    (:HOOK . 4)
    (:BASIC_GRIP . 5)
    (:PINCH_GRIP . 6)
    (:WIDE_GRIP . 7)
    (:SCISSOR_GRIP . 8)
    (:RESET . 9)
    (:OPEN_FINGERS . 10)
    (:OPEN_THUMB . 11)
    (:CLOSE_FINGERS . 12)
    (:CLOSE_THUMB . 13)
    (:LEFT . 0)
    (:RIGHT . 1))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'HandDesiredConfigurationRosMessage)))
    "Constants for message type 'HandDesiredConfigurationRosMessage"
  '((:STOP . 0)
    (:OPEN . 1)
    (:CLOSE . 2)
    (:CRUSH . 3)
    (:HOOK . 4)
    (:BASIC_GRIP . 5)
    (:PINCH_GRIP . 6)
    (:WIDE_GRIP . 7)
    (:SCISSOR_GRIP . 8)
    (:RESET . 9)
    (:OPEN_FINGERS . 10)
    (:OPEN_THUMB . 11)
    (:CLOSE_FINGERS . 12)
    (:CLOSE_THUMB . 13)
    (:LEFT . 0)
    (:RIGHT . 1))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <HandDesiredConfigurationRosMessage>) ostream)
  "Serializes a message object of type '<HandDesiredConfigurationRosMessage>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'robot_side)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'hand_desired_configuration)) ostream)
  (cl:let* ((signed (cl:slot-value msg 'unique_id)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <HandDesiredConfigurationRosMessage>) istream)
  "Deserializes a message object of type '<HandDesiredConfigurationRosMessage>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'robot_side)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'hand_desired_configuration)) (cl:read-byte istream))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'unique_id) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<HandDesiredConfigurationRosMessage>)))
  "Returns string type for a message object of type '<HandDesiredConfigurationRosMessage>"
  "ihmc_msgs/HandDesiredConfigurationRosMessage")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'HandDesiredConfigurationRosMessage)))
  "Returns string type for a message object of type 'HandDesiredConfigurationRosMessage"
  "ihmc_msgs/HandDesiredConfigurationRosMessage")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<HandDesiredConfigurationRosMessage>)))
  "Returns md5sum for a message object of type '<HandDesiredConfigurationRosMessage>"
  "06ee4472791eaf16119740f0672d6905")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'HandDesiredConfigurationRosMessage)))
  "Returns md5sum for a message object of type 'HandDesiredConfigurationRosMessage"
  "06ee4472791eaf16119740f0672d6905")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<HandDesiredConfigurationRosMessage>)))
  "Returns full string definition for message of type '<HandDesiredConfigurationRosMessage>"
  (cl:format cl:nil "## HandDesiredConfigurationRosMessage~%# Packet for commanding the hands to perform various predefined grasps. A message with a unique id~%# equals to 0 will be interpreted as invalid and will not be processed by the controller.~%~%# Specifies the side of the robot that will execute the trajectory~%uint8 robot_side~%~%# Specifies the grasp to perform~%uint8 hand_desired_configuration~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%# This message utilizes \"enums\". Enum value information for this message follows.~%~%# \"hand_configuration\" enum values:~%uint8 STOP=0 # stops the fingers at their current position~%uint8 OPEN=1 # fully opens the fingers~%uint8 CLOSE=2 # fully closes the fingers~%uint8 CRUSH=3 # fully closes the fingers applying maximum force~%uint8 HOOK=4 # closes all but one finger to create a hook~%uint8 BASIC_GRIP=5 # sets gripper to use a standard grasp~%uint8 PINCH_GRIP=6 # sets gripper to use a pinch grasp where the thumb and fingers come together when closed~%uint8 WIDE_GRIP=7 # sets gripper to use a wide-spread finger grasp~%uint8 SCISSOR_GRIP=8 # sets gripper to use a scissor grasp where the index and middle finger come together when closed~%uint8 RESET=9 # sets all fingers to their zero position~%uint8 OPEN_FINGERS=10 # fully open all fingers except the thumb~%uint8 OPEN_THUMB=11 # fully open the thumb only~%uint8 CLOSE_FINGERS=12 # fully close all fingers except the thumb~%uint8 CLOSE_THUMB=13 # fully close the thumb only~%~%# \"robot_side\" enum values:~%uint8 LEFT=0 # refers to the LEFT side of a robot~%uint8 RIGHT=1 # refers to the RIGHT side of a robot~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'HandDesiredConfigurationRosMessage)))
  "Returns full string definition for message of type 'HandDesiredConfigurationRosMessage"
  (cl:format cl:nil "## HandDesiredConfigurationRosMessage~%# Packet for commanding the hands to perform various predefined grasps. A message with a unique id~%# equals to 0 will be interpreted as invalid and will not be processed by the controller.~%~%# Specifies the side of the robot that will execute the trajectory~%uint8 robot_side~%~%# Specifies the grasp to perform~%uint8 hand_desired_configuration~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%# This message utilizes \"enums\". Enum value information for this message follows.~%~%# \"hand_configuration\" enum values:~%uint8 STOP=0 # stops the fingers at their current position~%uint8 OPEN=1 # fully opens the fingers~%uint8 CLOSE=2 # fully closes the fingers~%uint8 CRUSH=3 # fully closes the fingers applying maximum force~%uint8 HOOK=4 # closes all but one finger to create a hook~%uint8 BASIC_GRIP=5 # sets gripper to use a standard grasp~%uint8 PINCH_GRIP=6 # sets gripper to use a pinch grasp where the thumb and fingers come together when closed~%uint8 WIDE_GRIP=7 # sets gripper to use a wide-spread finger grasp~%uint8 SCISSOR_GRIP=8 # sets gripper to use a scissor grasp where the index and middle finger come together when closed~%uint8 RESET=9 # sets all fingers to their zero position~%uint8 OPEN_FINGERS=10 # fully open all fingers except the thumb~%uint8 OPEN_THUMB=11 # fully open the thumb only~%uint8 CLOSE_FINGERS=12 # fully close all fingers except the thumb~%uint8 CLOSE_THUMB=13 # fully close the thumb only~%~%# \"robot_side\" enum values:~%uint8 LEFT=0 # refers to the LEFT side of a robot~%uint8 RIGHT=1 # refers to the RIGHT side of a robot~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <HandDesiredConfigurationRosMessage>))
  (cl:+ 0
     1
     1
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <HandDesiredConfigurationRosMessage>))
  "Converts a ROS message object to a list"
  (cl:list 'HandDesiredConfigurationRosMessage
    (cl:cons ':robot_side (robot_side msg))
    (cl:cons ':hand_desired_configuration (hand_desired_configuration msg))
    (cl:cons ':unique_id (unique_id msg))
))
