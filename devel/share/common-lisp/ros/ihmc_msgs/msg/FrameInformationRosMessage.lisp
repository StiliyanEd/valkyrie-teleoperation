; Auto-generated. Do not edit!


(cl:in-package ihmc_msgs-msg)


;//! \htmlinclude FrameInformationRosMessage.msg.html

(cl:defclass <FrameInformationRosMessage> (roslisp-msg-protocol:ros-message)
  ((trajectory_reference_frame_id
    :reader trajectory_reference_frame_id
    :initarg :trajectory_reference_frame_id
    :type cl:integer
    :initform 0)
   (data_reference_frame_id
    :reader data_reference_frame_id
    :initarg :data_reference_frame_id
    :type cl:integer
    :initform 0))
)

(cl:defclass FrameInformationRosMessage (<FrameInformationRosMessage>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <FrameInformationRosMessage>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'FrameInformationRosMessage)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name ihmc_msgs-msg:<FrameInformationRosMessage> is deprecated: use ihmc_msgs-msg:FrameInformationRosMessage instead.")))

(cl:ensure-generic-function 'trajectory_reference_frame_id-val :lambda-list '(m))
(cl:defmethod trajectory_reference_frame_id-val ((m <FrameInformationRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:trajectory_reference_frame_id-val is deprecated.  Use ihmc_msgs-msg:trajectory_reference_frame_id instead.")
  (trajectory_reference_frame_id m))

(cl:ensure-generic-function 'data_reference_frame_id-val :lambda-list '(m))
(cl:defmethod data_reference_frame_id-val ((m <FrameInformationRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:data_reference_frame_id-val is deprecated.  Use ihmc_msgs-msg:data_reference_frame_id instead.")
  (data_reference_frame_id m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <FrameInformationRosMessage>) ostream)
  "Serializes a message object of type '<FrameInformationRosMessage>"
  (cl:let* ((signed (cl:slot-value msg 'trajectory_reference_frame_id)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'data_reference_frame_id)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <FrameInformationRosMessage>) istream)
  "Deserializes a message object of type '<FrameInformationRosMessage>"
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'trajectory_reference_frame_id) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'data_reference_frame_id) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<FrameInformationRosMessage>)))
  "Returns string type for a message object of type '<FrameInformationRosMessage>"
  "ihmc_msgs/FrameInformationRosMessage")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'FrameInformationRosMessage)))
  "Returns string type for a message object of type 'FrameInformationRosMessage"
  "ihmc_msgs/FrameInformationRosMessage")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<FrameInformationRosMessage>)))
  "Returns md5sum for a message object of type '<FrameInformationRosMessage>"
  "771962161ec9f64c2d4c9b7abe2b7cfa")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'FrameInformationRosMessage)))
  "Returns md5sum for a message object of type 'FrameInformationRosMessage"
  "771962161ec9f64c2d4c9b7abe2b7cfa")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<FrameInformationRosMessage>)))
  "Returns full string definition for message of type '<FrameInformationRosMessage>"
  (cl:format cl:nil "## FrameInformationRosMessage~%# This is a holder for frame related information. Valid codes and their associated frames include:~%# MIDFEET_ZUP_FRAME = -100 PELVIS_ZUP_FRAME = -101 PELVIS_FRAME = -102 CHEST_FRAME = -103~%# CENTER_OF_MASS_FRAME = -104 LEFT_SOLE_FRAME = -105 RIGHT_SOLE_FRAME = -106~%~%# The ID of the reference frame that a trajectory is executed in.~%int64 trajectory_reference_frame_id~%~%# The ID of the reference frame that trajectory data in a packet is expressed in. The frame of the~%# trajectory data will be switched to the trajectory frame immediately when the message is received by~%# the controller.~%int64 data_reference_frame_id~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'FrameInformationRosMessage)))
  "Returns full string definition for message of type 'FrameInformationRosMessage"
  (cl:format cl:nil "## FrameInformationRosMessage~%# This is a holder for frame related information. Valid codes and their associated frames include:~%# MIDFEET_ZUP_FRAME = -100 PELVIS_ZUP_FRAME = -101 PELVIS_FRAME = -102 CHEST_FRAME = -103~%# CENTER_OF_MASS_FRAME = -104 LEFT_SOLE_FRAME = -105 RIGHT_SOLE_FRAME = -106~%~%# The ID of the reference frame that a trajectory is executed in.~%int64 trajectory_reference_frame_id~%~%# The ID of the reference frame that trajectory data in a packet is expressed in. The frame of the~%# trajectory data will be switched to the trajectory frame immediately when the message is received by~%# the controller.~%int64 data_reference_frame_id~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <FrameInformationRosMessage>))
  (cl:+ 0
     8
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <FrameInformationRosMessage>))
  "Converts a ROS message object to a list"
  (cl:list 'FrameInformationRosMessage
    (cl:cons ':trajectory_reference_frame_id (trajectory_reference_frame_id msg))
    (cl:cons ':data_reference_frame_id (data_reference_frame_id msg))
))
