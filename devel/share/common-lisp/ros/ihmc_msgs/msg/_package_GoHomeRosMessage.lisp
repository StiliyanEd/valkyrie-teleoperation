(cl:in-package ihmc_msgs-msg)
(cl:export '(BODY_PART-VAL
          BODY_PART
          ROBOT_SIDE-VAL
          ROBOT_SIDE
          TRAJECTORY_TIME-VAL
          TRAJECTORY_TIME
          UNIQUE_ID-VAL
          UNIQUE_ID
))