(cl:in-package ihmc_msgs-msg)
(cl:export '(TIME-VAL
          TIME
          POSITION-VAL
          POSITION
          ORIENTATION-VAL
          ORIENTATION
          LINEAR_VELOCITY-VAL
          LINEAR_VELOCITY
          ANGULAR_VELOCITY-VAL
          ANGULAR_VELOCITY
          UNIQUE_ID-VAL
          UNIQUE_ID
))