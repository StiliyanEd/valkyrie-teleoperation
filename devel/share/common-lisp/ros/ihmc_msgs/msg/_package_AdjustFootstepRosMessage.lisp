(cl:in-package ihmc_msgs-msg)
(cl:export '(ROBOT_SIDE-VAL
          ROBOT_SIDE
          LOCATION-VAL
          LOCATION
          ORIENTATION-VAL
          ORIENTATION
          PREDICTED_CONTACT_POINTS-VAL
          PREDICTED_CONTACT_POINTS
          UNIQUE_ID-VAL
          UNIQUE_ID
))