; Auto-generated. Do not edit!


(cl:in-package ihmc_msgs-msg)


;//! \htmlinclude SupportPolygonRosMessage.msg.html

(cl:defclass <SupportPolygonRosMessage> (roslisp-msg-protocol:ros-message)
  ((number_of_vertices
    :reader number_of_vertices
    :initarg :number_of_vertices
    :type cl:integer
    :initform 0)
   (vertices
    :reader vertices
    :initarg :vertices
    :type (cl:vector ihmc_msgs-msg:Point2dRosMessage)
   :initform (cl:make-array 0 :element-type 'ihmc_msgs-msg:Point2dRosMessage :initial-element (cl:make-instance 'ihmc_msgs-msg:Point2dRosMessage))))
)

(cl:defclass SupportPolygonRosMessage (<SupportPolygonRosMessage>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <SupportPolygonRosMessage>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'SupportPolygonRosMessage)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name ihmc_msgs-msg:<SupportPolygonRosMessage> is deprecated: use ihmc_msgs-msg:SupportPolygonRosMessage instead.")))

(cl:ensure-generic-function 'number_of_vertices-val :lambda-list '(m))
(cl:defmethod number_of_vertices-val ((m <SupportPolygonRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:number_of_vertices-val is deprecated.  Use ihmc_msgs-msg:number_of_vertices instead.")
  (number_of_vertices m))

(cl:ensure-generic-function 'vertices-val :lambda-list '(m))
(cl:defmethod vertices-val ((m <SupportPolygonRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:vertices-val is deprecated.  Use ihmc_msgs-msg:vertices instead.")
  (vertices m))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<SupportPolygonRosMessage>)))
    "Constants for message type '<SupportPolygonRosMessage>"
  '((:MAXIMUM_NUMBER_OF_VERTICES . 8))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'SupportPolygonRosMessage)))
    "Constants for message type 'SupportPolygonRosMessage"
  '((:MAXIMUM_NUMBER_OF_VERTICES . 8))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <SupportPolygonRosMessage>) ostream)
  "Serializes a message object of type '<SupportPolygonRosMessage>"
  (cl:let* ((signed (cl:slot-value msg 'number_of_vertices)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'vertices))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'vertices))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <SupportPolygonRosMessage>) istream)
  "Deserializes a message object of type '<SupportPolygonRosMessage>"
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'number_of_vertices) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'vertices) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'vertices)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'ihmc_msgs-msg:Point2dRosMessage))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<SupportPolygonRosMessage>)))
  "Returns string type for a message object of type '<SupportPolygonRosMessage>"
  "ihmc_msgs/SupportPolygonRosMessage")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'SupportPolygonRosMessage)))
  "Returns string type for a message object of type 'SupportPolygonRosMessage"
  "ihmc_msgs/SupportPolygonRosMessage")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<SupportPolygonRosMessage>)))
  "Returns md5sum for a message object of type '<SupportPolygonRosMessage>"
  "214687cf8edfa8626dde4b3a7d93f2fe")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'SupportPolygonRosMessage)))
  "Returns md5sum for a message object of type 'SupportPolygonRosMessage"
  "214687cf8edfa8626dde4b3a7d93f2fe")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<SupportPolygonRosMessage>)))
  "Returns full string definition for message of type '<SupportPolygonRosMessage>"
  (cl:format cl:nil "## SupportPolygonRosMessage~%# This message contains of an array of points that represent the convex hull of a support polygon for~%# a single robot foot~%# Constant defining max number of possible elements in the array of points~%int32 MAXIMUM_NUMBER_OF_VERTICES=8~%~%# The number of vertices in the array of points~%int32 number_of_vertices~%~%# The vertices of the support polygon~%ihmc_msgs/Point2dRosMessage[] vertices~%~%~%================================================================================~%MSG: ihmc_msgs/Point2dRosMessage~%## Point2dRosMessage~%# This message represents a point on a 2d plane. The coordinates are referred to as \"x\" and \"y\"~%# The first coordinate of the point in a 2D plane~%float64 x~%~%# The second coordinate of the point in a 2D plane~%float64 y~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'SupportPolygonRosMessage)))
  "Returns full string definition for message of type 'SupportPolygonRosMessage"
  (cl:format cl:nil "## SupportPolygonRosMessage~%# This message contains of an array of points that represent the convex hull of a support polygon for~%# a single robot foot~%# Constant defining max number of possible elements in the array of points~%int32 MAXIMUM_NUMBER_OF_VERTICES=8~%~%# The number of vertices in the array of points~%int32 number_of_vertices~%~%# The vertices of the support polygon~%ihmc_msgs/Point2dRosMessage[] vertices~%~%~%================================================================================~%MSG: ihmc_msgs/Point2dRosMessage~%## Point2dRosMessage~%# This message represents a point on a 2d plane. The coordinates are referred to as \"x\" and \"y\"~%# The first coordinate of the point in a 2D plane~%float64 x~%~%# The second coordinate of the point in a 2D plane~%float64 y~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <SupportPolygonRosMessage>))
  (cl:+ 0
     4
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'vertices) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <SupportPolygonRosMessage>))
  "Converts a ROS message object to a list"
  (cl:list 'SupportPolygonRosMessage
    (cl:cons ':number_of_vertices (number_of_vertices msg))
    (cl:cons ':vertices (vertices msg))
))
