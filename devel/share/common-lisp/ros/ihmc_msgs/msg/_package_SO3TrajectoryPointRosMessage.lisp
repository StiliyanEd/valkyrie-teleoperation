(cl:in-package ihmc_msgs-msg)
(cl:export '(TIME-VAL
          TIME
          ORIENTATION-VAL
          ORIENTATION
          ANGULAR_VELOCITY-VAL
          ANGULAR_VELOCITY
          UNIQUE_ID-VAL
          UNIQUE_ID
))