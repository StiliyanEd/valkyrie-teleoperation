; Auto-generated. Do not edit!


(cl:in-package ihmc_msgs-msg)


;//! \htmlinclude ArmTrajectoryRosMessage.msg.html

(cl:defclass <ArmTrajectoryRosMessage> (roslisp-msg-protocol:ros-message)
  ((robot_side
    :reader robot_side
    :initarg :robot_side
    :type cl:fixnum
    :initform 0)
   (joint_trajectory_messages
    :reader joint_trajectory_messages
    :initarg :joint_trajectory_messages
    :type (cl:vector ihmc_msgs-msg:OneDoFJointTrajectoryRosMessage)
   :initform (cl:make-array 0 :element-type 'ihmc_msgs-msg:OneDoFJointTrajectoryRosMessage :initial-element (cl:make-instance 'ihmc_msgs-msg:OneDoFJointTrajectoryRosMessage)))
   (execution_mode
    :reader execution_mode
    :initarg :execution_mode
    :type cl:fixnum
    :initform 0)
   (previous_message_id
    :reader previous_message_id
    :initarg :previous_message_id
    :type cl:integer
    :initform 0)
   (unique_id
    :reader unique_id
    :initarg :unique_id
    :type cl:integer
    :initform 0))
)

(cl:defclass ArmTrajectoryRosMessage (<ArmTrajectoryRosMessage>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <ArmTrajectoryRosMessage>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'ArmTrajectoryRosMessage)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name ihmc_msgs-msg:<ArmTrajectoryRosMessage> is deprecated: use ihmc_msgs-msg:ArmTrajectoryRosMessage instead.")))

(cl:ensure-generic-function 'robot_side-val :lambda-list '(m))
(cl:defmethod robot_side-val ((m <ArmTrajectoryRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:robot_side-val is deprecated.  Use ihmc_msgs-msg:robot_side instead.")
  (robot_side m))

(cl:ensure-generic-function 'joint_trajectory_messages-val :lambda-list '(m))
(cl:defmethod joint_trajectory_messages-val ((m <ArmTrajectoryRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:joint_trajectory_messages-val is deprecated.  Use ihmc_msgs-msg:joint_trajectory_messages instead.")
  (joint_trajectory_messages m))

(cl:ensure-generic-function 'execution_mode-val :lambda-list '(m))
(cl:defmethod execution_mode-val ((m <ArmTrajectoryRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:execution_mode-val is deprecated.  Use ihmc_msgs-msg:execution_mode instead.")
  (execution_mode m))

(cl:ensure-generic-function 'previous_message_id-val :lambda-list '(m))
(cl:defmethod previous_message_id-val ((m <ArmTrajectoryRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:previous_message_id-val is deprecated.  Use ihmc_msgs-msg:previous_message_id instead.")
  (previous_message_id m))

(cl:ensure-generic-function 'unique_id-val :lambda-list '(m))
(cl:defmethod unique_id-val ((m <ArmTrajectoryRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:unique_id-val is deprecated.  Use ihmc_msgs-msg:unique_id instead.")
  (unique_id m))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<ArmTrajectoryRosMessage>)))
    "Constants for message type '<ArmTrajectoryRosMessage>"
  '((:OVERRIDE . 0)
    (:QUEUE . 1)
    (:LEFT . 0)
    (:RIGHT . 1))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'ArmTrajectoryRosMessage)))
    "Constants for message type 'ArmTrajectoryRosMessage"
  '((:OVERRIDE . 0)
    (:QUEUE . 1)
    (:LEFT . 0)
    (:RIGHT . 1))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <ArmTrajectoryRosMessage>) ostream)
  "Serializes a message object of type '<ArmTrajectoryRosMessage>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'robot_side)) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'joint_trajectory_messages))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'joint_trajectory_messages))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'execution_mode)) ostream)
  (cl:let* ((signed (cl:slot-value msg 'previous_message_id)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'unique_id)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <ArmTrajectoryRosMessage>) istream)
  "Deserializes a message object of type '<ArmTrajectoryRosMessage>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'robot_side)) (cl:read-byte istream))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'joint_trajectory_messages) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'joint_trajectory_messages)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'ihmc_msgs-msg:OneDoFJointTrajectoryRosMessage))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'execution_mode)) (cl:read-byte istream))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'previous_message_id) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'unique_id) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<ArmTrajectoryRosMessage>)))
  "Returns string type for a message object of type '<ArmTrajectoryRosMessage>"
  "ihmc_msgs/ArmTrajectoryRosMessage")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'ArmTrajectoryRosMessage)))
  "Returns string type for a message object of type 'ArmTrajectoryRosMessage"
  "ihmc_msgs/ArmTrajectoryRosMessage")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<ArmTrajectoryRosMessage>)))
  "Returns md5sum for a message object of type '<ArmTrajectoryRosMessage>"
  "06aae82764b4105dee57afb4ba1296d2")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'ArmTrajectoryRosMessage)))
  "Returns md5sum for a message object of type 'ArmTrajectoryRosMessage"
  "06aae82764b4105dee57afb4ba1296d2")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<ArmTrajectoryRosMessage>)))
  "Returns full string definition for message of type '<ArmTrajectoryRosMessage>"
  (cl:format cl:nil "## ArmTrajectoryRosMessage~%# This message commands the controller to move an arm in jointspace to the desired joint angles while~%# going through the specified trajectory points. A third order polynomial function is used to~%# interpolate between trajectory points. The jointTrajectoryMessages can have different waypoint times~%# and different number of waypoints. If a joint trajectory message is empty, the controller will hold~%# the last desired joint position while executing the other joint trajectories. A message with a~%# unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%# This rule does not apply to the fields of this message.~%~%# Specifies the side of the robot that will execute the trajectory.~%uint8 robot_side~%~%# List of points in the trajectory.~%ihmc_msgs/OneDoFJointTrajectoryRosMessage[] joint_trajectory_messages~%~%# When OVERRIDE is chosen:  - The time of the first trajectory point can be zero, in which case the~%# controller will start directly at the first trajectory point. Otherwise the controller will prepend~%# a first trajectory point at the current desired position.  When QUEUE is chosen:  - The message must~%# carry the ID of the message it should be queued to.  - The very first message of a list of queued~%# messages has to be an OVERRIDE message.  - The trajectory point times are relative to the the last~%# trajectory point time of the previous message.  - The controller will queue the joint trajectory~%# messages as a per joint basis. The first trajectory point has to be greater than zero.~%uint8 execution_mode~%~%# Only needed when using QUEUE mode, it refers to the message Id to which this message should be~%# queued to. It is used by the controller to ensure that no message has been lost on the way. If a~%# message appears to be missing (previousMessageId different from the last message ID received by the~%# controller), the motion is aborted. If previousMessageId == 0, the controller will not check for the~%# ID of the last received message.~%int64 previous_message_id~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%# This message utilizes \"enums\". Enum value information for this message follows.~%~%# \"execution_mode\" enum values:~%uint8 OVERRIDE=0 # This message will override the previous.~%uint8 QUEUE=1 # The previous message will first be executed before executing this message. When sending a series of queued messages, the very first has to be declared as OVERRIDE.~%~%# \"robot_side\" enum values:~%uint8 LEFT=0 # refers to the LEFT side of a robot~%uint8 RIGHT=1 # refers to the RIGHT side of a robot~%~%~%================================================================================~%MSG: ihmc_msgs/OneDoFJointTrajectoryRosMessage~%## OneDoFJointTrajectoryRosMessage~%# This class is used to build trajectory messages in jointspace. It holds all the trajectory points to~%# go through with a one-dimensional trajectory. A third order polynomial function is used to~%# interpolate between trajectory points.~%~%# QP Weight, if Too low, in the event the qp can't achieve all of the objectives it may stop trying to~%# achieve the desireds, if too high, it will favor this joint over other objectives. If set to NaN it~%# will use the default weight for that joint~%float64 weight~%~%# List of trajectory points to go through while executing the trajectory.~%ihmc_msgs/TrajectoryPoint1DRosMessage[] trajectory_points~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%~%================================================================================~%MSG: ihmc_msgs/TrajectoryPoint1DRosMessage~%## TrajectoryPoint1DRosMessage~%# This class is used to build 1D trajectory messages including jointspace trajectory messages. For 3D~%# trajectory points look at EuclideanTrajectoryMessage (translational), SO3TrajectoryPointMessage~%# (rotational), and SE3TrajectoryPointMessage (translational AND rotational).~%~%# Time at which the trajectory point has to be reached. The time is relative to when the trajectory~%# starts.~%float64 time~%~%# Define the desired 1D position to be reached at this trajectory point.~%float64 position~%~%# Define the desired 1D velocity to be reached at this trajectory point.~%float64 velocity~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'ArmTrajectoryRosMessage)))
  "Returns full string definition for message of type 'ArmTrajectoryRosMessage"
  (cl:format cl:nil "## ArmTrajectoryRosMessage~%# This message commands the controller to move an arm in jointspace to the desired joint angles while~%# going through the specified trajectory points. A third order polynomial function is used to~%# interpolate between trajectory points. The jointTrajectoryMessages can have different waypoint times~%# and different number of waypoints. If a joint trajectory message is empty, the controller will hold~%# the last desired joint position while executing the other joint trajectories. A message with a~%# unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%# This rule does not apply to the fields of this message.~%~%# Specifies the side of the robot that will execute the trajectory.~%uint8 robot_side~%~%# List of points in the trajectory.~%ihmc_msgs/OneDoFJointTrajectoryRosMessage[] joint_trajectory_messages~%~%# When OVERRIDE is chosen:  - The time of the first trajectory point can be zero, in which case the~%# controller will start directly at the first trajectory point. Otherwise the controller will prepend~%# a first trajectory point at the current desired position.  When QUEUE is chosen:  - The message must~%# carry the ID of the message it should be queued to.  - The very first message of a list of queued~%# messages has to be an OVERRIDE message.  - The trajectory point times are relative to the the last~%# trajectory point time of the previous message.  - The controller will queue the joint trajectory~%# messages as a per joint basis. The first trajectory point has to be greater than zero.~%uint8 execution_mode~%~%# Only needed when using QUEUE mode, it refers to the message Id to which this message should be~%# queued to. It is used by the controller to ensure that no message has been lost on the way. If a~%# message appears to be missing (previousMessageId different from the last message ID received by the~%# controller), the motion is aborted. If previousMessageId == 0, the controller will not check for the~%# ID of the last received message.~%int64 previous_message_id~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%# This message utilizes \"enums\". Enum value information for this message follows.~%~%# \"execution_mode\" enum values:~%uint8 OVERRIDE=0 # This message will override the previous.~%uint8 QUEUE=1 # The previous message will first be executed before executing this message. When sending a series of queued messages, the very first has to be declared as OVERRIDE.~%~%# \"robot_side\" enum values:~%uint8 LEFT=0 # refers to the LEFT side of a robot~%uint8 RIGHT=1 # refers to the RIGHT side of a robot~%~%~%================================================================================~%MSG: ihmc_msgs/OneDoFJointTrajectoryRosMessage~%## OneDoFJointTrajectoryRosMessage~%# This class is used to build trajectory messages in jointspace. It holds all the trajectory points to~%# go through with a one-dimensional trajectory. A third order polynomial function is used to~%# interpolate between trajectory points.~%~%# QP Weight, if Too low, in the event the qp can't achieve all of the objectives it may stop trying to~%# achieve the desireds, if too high, it will favor this joint over other objectives. If set to NaN it~%# will use the default weight for that joint~%float64 weight~%~%# List of trajectory points to go through while executing the trajectory.~%ihmc_msgs/TrajectoryPoint1DRosMessage[] trajectory_points~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%~%================================================================================~%MSG: ihmc_msgs/TrajectoryPoint1DRosMessage~%## TrajectoryPoint1DRosMessage~%# This class is used to build 1D trajectory messages including jointspace trajectory messages. For 3D~%# trajectory points look at EuclideanTrajectoryMessage (translational), SO3TrajectoryPointMessage~%# (rotational), and SE3TrajectoryPointMessage (translational AND rotational).~%~%# Time at which the trajectory point has to be reached. The time is relative to when the trajectory~%# starts.~%float64 time~%~%# Define the desired 1D position to be reached at this trajectory point.~%float64 position~%~%# Define the desired 1D velocity to be reached at this trajectory point.~%float64 velocity~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <ArmTrajectoryRosMessage>))
  (cl:+ 0
     1
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'joint_trajectory_messages) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
     1
     8
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <ArmTrajectoryRosMessage>))
  "Converts a ROS message object to a list"
  (cl:list 'ArmTrajectoryRosMessage
    (cl:cons ':robot_side (robot_side msg))
    (cl:cons ':joint_trajectory_messages (joint_trajectory_messages msg))
    (cl:cons ':execution_mode (execution_mode msg))
    (cl:cons ':previous_message_id (previous_message_id msg))
    (cl:cons ':unique_id (unique_id msg))
))
