; Auto-generated. Do not edit!


(cl:in-package ihmc_msgs-msg)


;//! \htmlinclude OneDoFJointTrajectoryRosMessage.msg.html

(cl:defclass <OneDoFJointTrajectoryRosMessage> (roslisp-msg-protocol:ros-message)
  ((weight
    :reader weight
    :initarg :weight
    :type cl:float
    :initform 0.0)
   (trajectory_points
    :reader trajectory_points
    :initarg :trajectory_points
    :type (cl:vector ihmc_msgs-msg:TrajectoryPoint1DRosMessage)
   :initform (cl:make-array 0 :element-type 'ihmc_msgs-msg:TrajectoryPoint1DRosMessage :initial-element (cl:make-instance 'ihmc_msgs-msg:TrajectoryPoint1DRosMessage)))
   (unique_id
    :reader unique_id
    :initarg :unique_id
    :type cl:integer
    :initform 0))
)

(cl:defclass OneDoFJointTrajectoryRosMessage (<OneDoFJointTrajectoryRosMessage>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <OneDoFJointTrajectoryRosMessage>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'OneDoFJointTrajectoryRosMessage)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name ihmc_msgs-msg:<OneDoFJointTrajectoryRosMessage> is deprecated: use ihmc_msgs-msg:OneDoFJointTrajectoryRosMessage instead.")))

(cl:ensure-generic-function 'weight-val :lambda-list '(m))
(cl:defmethod weight-val ((m <OneDoFJointTrajectoryRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:weight-val is deprecated.  Use ihmc_msgs-msg:weight instead.")
  (weight m))

(cl:ensure-generic-function 'trajectory_points-val :lambda-list '(m))
(cl:defmethod trajectory_points-val ((m <OneDoFJointTrajectoryRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:trajectory_points-val is deprecated.  Use ihmc_msgs-msg:trajectory_points instead.")
  (trajectory_points m))

(cl:ensure-generic-function 'unique_id-val :lambda-list '(m))
(cl:defmethod unique_id-val ((m <OneDoFJointTrajectoryRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:unique_id-val is deprecated.  Use ihmc_msgs-msg:unique_id instead.")
  (unique_id m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <OneDoFJointTrajectoryRosMessage>) ostream)
  "Serializes a message object of type '<OneDoFJointTrajectoryRosMessage>"
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'weight))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'trajectory_points))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'trajectory_points))
  (cl:let* ((signed (cl:slot-value msg 'unique_id)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <OneDoFJointTrajectoryRosMessage>) istream)
  "Deserializes a message object of type '<OneDoFJointTrajectoryRosMessage>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'weight) (roslisp-utils:decode-double-float-bits bits)))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'trajectory_points) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'trajectory_points)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'ihmc_msgs-msg:TrajectoryPoint1DRosMessage))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'unique_id) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<OneDoFJointTrajectoryRosMessage>)))
  "Returns string type for a message object of type '<OneDoFJointTrajectoryRosMessage>"
  "ihmc_msgs/OneDoFJointTrajectoryRosMessage")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'OneDoFJointTrajectoryRosMessage)))
  "Returns string type for a message object of type 'OneDoFJointTrajectoryRosMessage"
  "ihmc_msgs/OneDoFJointTrajectoryRosMessage")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<OneDoFJointTrajectoryRosMessage>)))
  "Returns md5sum for a message object of type '<OneDoFJointTrajectoryRosMessage>"
  "98ee05851cb89b0052144e490d7754d5")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'OneDoFJointTrajectoryRosMessage)))
  "Returns md5sum for a message object of type 'OneDoFJointTrajectoryRosMessage"
  "98ee05851cb89b0052144e490d7754d5")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<OneDoFJointTrajectoryRosMessage>)))
  "Returns full string definition for message of type '<OneDoFJointTrajectoryRosMessage>"
  (cl:format cl:nil "## OneDoFJointTrajectoryRosMessage~%# This class is used to build trajectory messages in jointspace. It holds all the trajectory points to~%# go through with a one-dimensional trajectory. A third order polynomial function is used to~%# interpolate between trajectory points.~%~%# QP Weight, if Too low, in the event the qp can't achieve all of the objectives it may stop trying to~%# achieve the desireds, if too high, it will favor this joint over other objectives. If set to NaN it~%# will use the default weight for that joint~%float64 weight~%~%# List of trajectory points to go through while executing the trajectory.~%ihmc_msgs/TrajectoryPoint1DRosMessage[] trajectory_points~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%~%================================================================================~%MSG: ihmc_msgs/TrajectoryPoint1DRosMessage~%## TrajectoryPoint1DRosMessage~%# This class is used to build 1D trajectory messages including jointspace trajectory messages. For 3D~%# trajectory points look at EuclideanTrajectoryMessage (translational), SO3TrajectoryPointMessage~%# (rotational), and SE3TrajectoryPointMessage (translational AND rotational).~%~%# Time at which the trajectory point has to be reached. The time is relative to when the trajectory~%# starts.~%float64 time~%~%# Define the desired 1D position to be reached at this trajectory point.~%float64 position~%~%# Define the desired 1D velocity to be reached at this trajectory point.~%float64 velocity~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'OneDoFJointTrajectoryRosMessage)))
  "Returns full string definition for message of type 'OneDoFJointTrajectoryRosMessage"
  (cl:format cl:nil "## OneDoFJointTrajectoryRosMessage~%# This class is used to build trajectory messages in jointspace. It holds all the trajectory points to~%# go through with a one-dimensional trajectory. A third order polynomial function is used to~%# interpolate between trajectory points.~%~%# QP Weight, if Too low, in the event the qp can't achieve all of the objectives it may stop trying to~%# achieve the desireds, if too high, it will favor this joint over other objectives. If set to NaN it~%# will use the default weight for that joint~%float64 weight~%~%# List of trajectory points to go through while executing the trajectory.~%ihmc_msgs/TrajectoryPoint1DRosMessage[] trajectory_points~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%~%================================================================================~%MSG: ihmc_msgs/TrajectoryPoint1DRosMessage~%## TrajectoryPoint1DRosMessage~%# This class is used to build 1D trajectory messages including jointspace trajectory messages. For 3D~%# trajectory points look at EuclideanTrajectoryMessage (translational), SO3TrajectoryPointMessage~%# (rotational), and SE3TrajectoryPointMessage (translational AND rotational).~%~%# Time at which the trajectory point has to be reached. The time is relative to when the trajectory~%# starts.~%float64 time~%~%# Define the desired 1D position to be reached at this trajectory point.~%float64 position~%~%# Define the desired 1D velocity to be reached at this trajectory point.~%float64 velocity~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <OneDoFJointTrajectoryRosMessage>))
  (cl:+ 0
     8
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'trajectory_points) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <OneDoFJointTrajectoryRosMessage>))
  "Converts a ROS message object to a list"
  (cl:list 'OneDoFJointTrajectoryRosMessage
    (cl:cons ':weight (weight msg))
    (cl:cons ':trajectory_points (trajectory_points msg))
    (cl:cons ':unique_id (unique_id msg))
))
