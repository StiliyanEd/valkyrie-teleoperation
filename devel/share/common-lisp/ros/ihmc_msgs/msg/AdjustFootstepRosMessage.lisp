; Auto-generated. Do not edit!


(cl:in-package ihmc_msgs-msg)


;//! \htmlinclude AdjustFootstepRosMessage.msg.html

(cl:defclass <AdjustFootstepRosMessage> (roslisp-msg-protocol:ros-message)
  ((robot_side
    :reader robot_side
    :initarg :robot_side
    :type cl:fixnum
    :initform 0)
   (location
    :reader location
    :initarg :location
    :type geometry_msgs-msg:Point
    :initform (cl:make-instance 'geometry_msgs-msg:Point))
   (orientation
    :reader orientation
    :initarg :orientation
    :type geometry_msgs-msg:Quaternion
    :initform (cl:make-instance 'geometry_msgs-msg:Quaternion))
   (predicted_contact_points
    :reader predicted_contact_points
    :initarg :predicted_contact_points
    :type (cl:vector ihmc_msgs-msg:Point2dRosMessage)
   :initform (cl:make-array 0 :element-type 'ihmc_msgs-msg:Point2dRosMessage :initial-element (cl:make-instance 'ihmc_msgs-msg:Point2dRosMessage)))
   (unique_id
    :reader unique_id
    :initarg :unique_id
    :type cl:integer
    :initform 0))
)

(cl:defclass AdjustFootstepRosMessage (<AdjustFootstepRosMessage>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <AdjustFootstepRosMessage>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'AdjustFootstepRosMessage)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name ihmc_msgs-msg:<AdjustFootstepRosMessage> is deprecated: use ihmc_msgs-msg:AdjustFootstepRosMessage instead.")))

(cl:ensure-generic-function 'robot_side-val :lambda-list '(m))
(cl:defmethod robot_side-val ((m <AdjustFootstepRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:robot_side-val is deprecated.  Use ihmc_msgs-msg:robot_side instead.")
  (robot_side m))

(cl:ensure-generic-function 'location-val :lambda-list '(m))
(cl:defmethod location-val ((m <AdjustFootstepRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:location-val is deprecated.  Use ihmc_msgs-msg:location instead.")
  (location m))

(cl:ensure-generic-function 'orientation-val :lambda-list '(m))
(cl:defmethod orientation-val ((m <AdjustFootstepRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:orientation-val is deprecated.  Use ihmc_msgs-msg:orientation instead.")
  (orientation m))

(cl:ensure-generic-function 'predicted_contact_points-val :lambda-list '(m))
(cl:defmethod predicted_contact_points-val ((m <AdjustFootstepRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:predicted_contact_points-val is deprecated.  Use ihmc_msgs-msg:predicted_contact_points instead.")
  (predicted_contact_points m))

(cl:ensure-generic-function 'unique_id-val :lambda-list '(m))
(cl:defmethod unique_id-val ((m <AdjustFootstepRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:unique_id-val is deprecated.  Use ihmc_msgs-msg:unique_id instead.")
  (unique_id m))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<AdjustFootstepRosMessage>)))
    "Constants for message type '<AdjustFootstepRosMessage>"
  '((:LEFT . 0)
    (:RIGHT . 1))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'AdjustFootstepRosMessage)))
    "Constants for message type 'AdjustFootstepRosMessage"
  '((:LEFT . 0)
    (:RIGHT . 1))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <AdjustFootstepRosMessage>) ostream)
  "Serializes a message object of type '<AdjustFootstepRosMessage>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'robot_side)) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'location) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'orientation) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'predicted_contact_points))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'predicted_contact_points))
  (cl:let* ((signed (cl:slot-value msg 'unique_id)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <AdjustFootstepRosMessage>) istream)
  "Deserializes a message object of type '<AdjustFootstepRosMessage>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'robot_side)) (cl:read-byte istream))
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'location) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'orientation) istream)
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'predicted_contact_points) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'predicted_contact_points)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'ihmc_msgs-msg:Point2dRosMessage))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'unique_id) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<AdjustFootstepRosMessage>)))
  "Returns string type for a message object of type '<AdjustFootstepRosMessage>"
  "ihmc_msgs/AdjustFootstepRosMessage")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'AdjustFootstepRosMessage)))
  "Returns string type for a message object of type 'AdjustFootstepRosMessage"
  "ihmc_msgs/AdjustFootstepRosMessage")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<AdjustFootstepRosMessage>)))
  "Returns md5sum for a message object of type '<AdjustFootstepRosMessage>"
  "849a098cda5c5330c4c2e2dd6b32be3c")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'AdjustFootstepRosMessage)))
  "Returns md5sum for a message object of type 'AdjustFootstepRosMessage"
  "849a098cda5c5330c4c2e2dd6b32be3c")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<AdjustFootstepRosMessage>)))
  "Returns full string definition for message of type '<AdjustFootstepRosMessage>"
  (cl:format cl:nil "## AdjustFootstepRosMessage~%# The intent of this message is to adjust a footstep when the robot is executing it (a foot is~%# currently swinging to reach the footstep to be adjusted).~%~%# Specifies which foot is expected to be executing the footstep to be adjusted.~%uint8 robot_side~%~%# Specifies the adjusted position of the footstep. It is expressed in world frame.~%geometry_msgs/Point location~%~%# Specifies the adjusted orientation of the footstep. It is expressed in world frame.~%geometry_msgs/Quaternion orientation~%~%# predictedContactPoints specifies the vertices of the expected contact polygon between the foot and~%# the world. A value of null or an empty list will default to keep the contact points used for the~%# original footstep. Contact points  are expressed in sole frame. This ordering does not matter. For~%# example: to tell the controller to use the entire foot, the predicted contact points would be:~%# predicted_contact_points: - {x: 0.5 * foot_length, y: -0.5 * toe_width} - {x: 0.5 * foot_length, y:~%# 0.5 * toe_width} - {x: -0.5 * foot_length, y: -0.5 * heel_width} - {x: -0.5 * foot_length, y: 0.5 *~%# heel_width} ~%ihmc_msgs/Point2dRosMessage[] predicted_contact_points~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%# This message utilizes \"enums\". Enum value information for this message follows.~%~%# \"robot_side\" enum values:~%uint8 LEFT=0 # refers to the LEFT side of a robot~%uint8 RIGHT=1 # refers to the RIGHT side of a robot~%~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%================================================================================~%MSG: ihmc_msgs/Point2dRosMessage~%## Point2dRosMessage~%# This message represents a point on a 2d plane. The coordinates are referred to as \"x\" and \"y\"~%# The first coordinate of the point in a 2D plane~%float64 x~%~%# The second coordinate of the point in a 2D plane~%float64 y~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'AdjustFootstepRosMessage)))
  "Returns full string definition for message of type 'AdjustFootstepRosMessage"
  (cl:format cl:nil "## AdjustFootstepRosMessage~%# The intent of this message is to adjust a footstep when the robot is executing it (a foot is~%# currently swinging to reach the footstep to be adjusted).~%~%# Specifies which foot is expected to be executing the footstep to be adjusted.~%uint8 robot_side~%~%# Specifies the adjusted position of the footstep. It is expressed in world frame.~%geometry_msgs/Point location~%~%# Specifies the adjusted orientation of the footstep. It is expressed in world frame.~%geometry_msgs/Quaternion orientation~%~%# predictedContactPoints specifies the vertices of the expected contact polygon between the foot and~%# the world. A value of null or an empty list will default to keep the contact points used for the~%# original footstep. Contact points  are expressed in sole frame. This ordering does not matter. For~%# example: to tell the controller to use the entire foot, the predicted contact points would be:~%# predicted_contact_points: - {x: 0.5 * foot_length, y: -0.5 * toe_width} - {x: 0.5 * foot_length, y:~%# 0.5 * toe_width} - {x: -0.5 * foot_length, y: -0.5 * heel_width} - {x: -0.5 * foot_length, y: 0.5 *~%# heel_width} ~%ihmc_msgs/Point2dRosMessage[] predicted_contact_points~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%# This message utilizes \"enums\". Enum value information for this message follows.~%~%# \"robot_side\" enum values:~%uint8 LEFT=0 # refers to the LEFT side of a robot~%uint8 RIGHT=1 # refers to the RIGHT side of a robot~%~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%================================================================================~%MSG: ihmc_msgs/Point2dRosMessage~%## Point2dRosMessage~%# This message represents a point on a 2d plane. The coordinates are referred to as \"x\" and \"y\"~%# The first coordinate of the point in a 2D plane~%float64 x~%~%# The second coordinate of the point in a 2D plane~%float64 y~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <AdjustFootstepRosMessage>))
  (cl:+ 0
     1
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'location))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'orientation))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'predicted_contact_points) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <AdjustFootstepRosMessage>))
  "Converts a ROS message object to a list"
  (cl:list 'AdjustFootstepRosMessage
    (cl:cons ':robot_side (robot_side msg))
    (cl:cons ':location (location msg))
    (cl:cons ':orientation (orientation msg))
    (cl:cons ':predicted_contact_points (predicted_contact_points msg))
    (cl:cons ':unique_id (unique_id msg))
))
