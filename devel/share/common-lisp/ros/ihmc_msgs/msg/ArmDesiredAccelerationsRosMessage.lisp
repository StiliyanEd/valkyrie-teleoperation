; Auto-generated. Do not edit!


(cl:in-package ihmc_msgs-msg)


;//! \htmlinclude ArmDesiredAccelerationsRosMessage.msg.html

(cl:defclass <ArmDesiredAccelerationsRosMessage> (roslisp-msg-protocol:ros-message)
  ((robot_side
    :reader robot_side
    :initarg :robot_side
    :type cl:fixnum
    :initform 0)
   (desired_joint_accelerations
    :reader desired_joint_accelerations
    :initarg :desired_joint_accelerations
    :type (cl:vector cl:float)
   :initform (cl:make-array 0 :element-type 'cl:float :initial-element 0.0))
   (unique_id
    :reader unique_id
    :initarg :unique_id
    :type cl:integer
    :initform 0))
)

(cl:defclass ArmDesiredAccelerationsRosMessage (<ArmDesiredAccelerationsRosMessage>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <ArmDesiredAccelerationsRosMessage>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'ArmDesiredAccelerationsRosMessage)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name ihmc_msgs-msg:<ArmDesiredAccelerationsRosMessage> is deprecated: use ihmc_msgs-msg:ArmDesiredAccelerationsRosMessage instead.")))

(cl:ensure-generic-function 'robot_side-val :lambda-list '(m))
(cl:defmethod robot_side-val ((m <ArmDesiredAccelerationsRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:robot_side-val is deprecated.  Use ihmc_msgs-msg:robot_side instead.")
  (robot_side m))

(cl:ensure-generic-function 'desired_joint_accelerations-val :lambda-list '(m))
(cl:defmethod desired_joint_accelerations-val ((m <ArmDesiredAccelerationsRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:desired_joint_accelerations-val is deprecated.  Use ihmc_msgs-msg:desired_joint_accelerations instead.")
  (desired_joint_accelerations m))

(cl:ensure-generic-function 'unique_id-val :lambda-list '(m))
(cl:defmethod unique_id-val ((m <ArmDesiredAccelerationsRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:unique_id-val is deprecated.  Use ihmc_msgs-msg:unique_id instead.")
  (unique_id m))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<ArmDesiredAccelerationsRosMessage>)))
    "Constants for message type '<ArmDesiredAccelerationsRosMessage>"
  '((:LEFT . 0)
    (:RIGHT . 1))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'ArmDesiredAccelerationsRosMessage)))
    "Constants for message type 'ArmDesiredAccelerationsRosMessage"
  '((:LEFT . 0)
    (:RIGHT . 1))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <ArmDesiredAccelerationsRosMessage>) ostream)
  "Serializes a message object of type '<ArmDesiredAccelerationsRosMessage>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'robot_side)) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'desired_joint_accelerations))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-double-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream)))
   (cl:slot-value msg 'desired_joint_accelerations))
  (cl:let* ((signed (cl:slot-value msg 'unique_id)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <ArmDesiredAccelerationsRosMessage>) istream)
  "Deserializes a message object of type '<ArmDesiredAccelerationsRosMessage>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'robot_side)) (cl:read-byte istream))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'desired_joint_accelerations) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'desired_joint_accelerations)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-double-float-bits bits))))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'unique_id) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<ArmDesiredAccelerationsRosMessage>)))
  "Returns string type for a message object of type '<ArmDesiredAccelerationsRosMessage>"
  "ihmc_msgs/ArmDesiredAccelerationsRosMessage")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'ArmDesiredAccelerationsRosMessage)))
  "Returns string type for a message object of type 'ArmDesiredAccelerationsRosMessage"
  "ihmc_msgs/ArmDesiredAccelerationsRosMessage")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<ArmDesiredAccelerationsRosMessage>)))
  "Returns md5sum for a message object of type '<ArmDesiredAccelerationsRosMessage>"
  "88e7b174370cc44173f8406f21f7e3fa")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'ArmDesiredAccelerationsRosMessage)))
  "Returns md5sum for a message object of type 'ArmDesiredAccelerationsRosMessage"
  "88e7b174370cc44173f8406f21f7e3fa")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<ArmDesiredAccelerationsRosMessage>)))
  "Returns full string definition for message of type '<ArmDesiredAccelerationsRosMessage>"
  (cl:format cl:nil "## ArmDesiredAccelerationsRosMessage~%# This message gives the user the option to bypass IHMC feedback controllers for the arm joints by~%# sending desired arm joint accelerations. One needs experience in control when activating the bypass~%# as it can result in unexpected behaviors for unreasonable accelerations. A message with a unique id~%# equals to 0 will be interpreted as invalid and will not be processed by the controller.~%~%# Specifies the side of the robot that will execute the trajectory.~%uint8 robot_side~%~%# Specifies the desired joint accelerations.~%float64[] desired_joint_accelerations~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%# This message utilizes \"enums\". Enum value information for this message follows.~%~%# \"robot_side\" enum values:~%uint8 LEFT=0 # refers to the LEFT side of a robot~%uint8 RIGHT=1 # refers to the RIGHT side of a robot~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'ArmDesiredAccelerationsRosMessage)))
  "Returns full string definition for message of type 'ArmDesiredAccelerationsRosMessage"
  (cl:format cl:nil "## ArmDesiredAccelerationsRosMessage~%# This message gives the user the option to bypass IHMC feedback controllers for the arm joints by~%# sending desired arm joint accelerations. One needs experience in control when activating the bypass~%# as it can result in unexpected behaviors for unreasonable accelerations. A message with a unique id~%# equals to 0 will be interpreted as invalid and will not be processed by the controller.~%~%# Specifies the side of the robot that will execute the trajectory.~%uint8 robot_side~%~%# Specifies the desired joint accelerations.~%float64[] desired_joint_accelerations~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%# This message utilizes \"enums\". Enum value information for this message follows.~%~%# \"robot_side\" enum values:~%uint8 LEFT=0 # refers to the LEFT side of a robot~%uint8 RIGHT=1 # refers to the RIGHT side of a robot~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <ArmDesiredAccelerationsRosMessage>))
  (cl:+ 0
     1
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'desired_joint_accelerations) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 8)))
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <ArmDesiredAccelerationsRosMessage>))
  "Converts a ROS message object to a list"
  (cl:list 'ArmDesiredAccelerationsRosMessage
    (cl:cons ':robot_side (robot_side msg))
    (cl:cons ':desired_joint_accelerations (desired_joint_accelerations msg))
    (cl:cons ':unique_id (unique_id msg))
))
