; Auto-generated. Do not edit!


(cl:in-package ihmc_msgs-msg)


;//! \htmlinclude FootstepStatusRosMessage.msg.html

(cl:defclass <FootstepStatusRosMessage> (roslisp-msg-protocol:ros-message)
  ((status
    :reader status
    :initarg :status
    :type cl:fixnum
    :initform 0)
   (footstep_index
    :reader footstep_index
    :initarg :footstep_index
    :type cl:integer
    :initform 0)
   (robot_side
    :reader robot_side
    :initarg :robot_side
    :type cl:fixnum
    :initform 0)
   (desired_foot_position_in_world
    :reader desired_foot_position_in_world
    :initarg :desired_foot_position_in_world
    :type geometry_msgs-msg:Point
    :initform (cl:make-instance 'geometry_msgs-msg:Point))
   (desired_foot_orientation_in_world
    :reader desired_foot_orientation_in_world
    :initarg :desired_foot_orientation_in_world
    :type geometry_msgs-msg:Quaternion
    :initform (cl:make-instance 'geometry_msgs-msg:Quaternion))
   (actual_foot_position_in_world
    :reader actual_foot_position_in_world
    :initarg :actual_foot_position_in_world
    :type geometry_msgs-msg:Point
    :initform (cl:make-instance 'geometry_msgs-msg:Point))
   (actual_foot_orientation_in_world
    :reader actual_foot_orientation_in_world
    :initarg :actual_foot_orientation_in_world
    :type geometry_msgs-msg:Quaternion
    :initform (cl:make-instance 'geometry_msgs-msg:Quaternion))
   (unique_id
    :reader unique_id
    :initarg :unique_id
    :type cl:integer
    :initform 0))
)

(cl:defclass FootstepStatusRosMessage (<FootstepStatusRosMessage>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <FootstepStatusRosMessage>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'FootstepStatusRosMessage)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name ihmc_msgs-msg:<FootstepStatusRosMessage> is deprecated: use ihmc_msgs-msg:FootstepStatusRosMessage instead.")))

(cl:ensure-generic-function 'status-val :lambda-list '(m))
(cl:defmethod status-val ((m <FootstepStatusRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:status-val is deprecated.  Use ihmc_msgs-msg:status instead.")
  (status m))

(cl:ensure-generic-function 'footstep_index-val :lambda-list '(m))
(cl:defmethod footstep_index-val ((m <FootstepStatusRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:footstep_index-val is deprecated.  Use ihmc_msgs-msg:footstep_index instead.")
  (footstep_index m))

(cl:ensure-generic-function 'robot_side-val :lambda-list '(m))
(cl:defmethod robot_side-val ((m <FootstepStatusRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:robot_side-val is deprecated.  Use ihmc_msgs-msg:robot_side instead.")
  (robot_side m))

(cl:ensure-generic-function 'desired_foot_position_in_world-val :lambda-list '(m))
(cl:defmethod desired_foot_position_in_world-val ((m <FootstepStatusRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:desired_foot_position_in_world-val is deprecated.  Use ihmc_msgs-msg:desired_foot_position_in_world instead.")
  (desired_foot_position_in_world m))

(cl:ensure-generic-function 'desired_foot_orientation_in_world-val :lambda-list '(m))
(cl:defmethod desired_foot_orientation_in_world-val ((m <FootstepStatusRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:desired_foot_orientation_in_world-val is deprecated.  Use ihmc_msgs-msg:desired_foot_orientation_in_world instead.")
  (desired_foot_orientation_in_world m))

(cl:ensure-generic-function 'actual_foot_position_in_world-val :lambda-list '(m))
(cl:defmethod actual_foot_position_in_world-val ((m <FootstepStatusRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:actual_foot_position_in_world-val is deprecated.  Use ihmc_msgs-msg:actual_foot_position_in_world instead.")
  (actual_foot_position_in_world m))

(cl:ensure-generic-function 'actual_foot_orientation_in_world-val :lambda-list '(m))
(cl:defmethod actual_foot_orientation_in_world-val ((m <FootstepStatusRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:actual_foot_orientation_in_world-val is deprecated.  Use ihmc_msgs-msg:actual_foot_orientation_in_world instead.")
  (actual_foot_orientation_in_world m))

(cl:ensure-generic-function 'unique_id-val :lambda-list '(m))
(cl:defmethod unique_id-val ((m <FootstepStatusRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:unique_id-val is deprecated.  Use ihmc_msgs-msg:unique_id instead.")
  (unique_id m))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<FootstepStatusRosMessage>)))
    "Constants for message type '<FootstepStatusRosMessage>"
  '((:LEFT . 0)
    (:RIGHT . 1)
    (:STARTED . 0)
    (:COMPLETED . 1))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'FootstepStatusRosMessage)))
    "Constants for message type 'FootstepStatusRosMessage"
  '((:LEFT . 0)
    (:RIGHT . 1)
    (:STARTED . 0)
    (:COMPLETED . 1))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <FootstepStatusRosMessage>) ostream)
  "Serializes a message object of type '<FootstepStatusRosMessage>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'status)) ostream)
  (cl:let* ((signed (cl:slot-value msg 'footstep_index)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'robot_side)) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'desired_foot_position_in_world) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'desired_foot_orientation_in_world) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'actual_foot_position_in_world) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'actual_foot_orientation_in_world) ostream)
  (cl:let* ((signed (cl:slot-value msg 'unique_id)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <FootstepStatusRosMessage>) istream)
  "Deserializes a message object of type '<FootstepStatusRosMessage>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'status)) (cl:read-byte istream))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'footstep_index) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'robot_side)) (cl:read-byte istream))
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'desired_foot_position_in_world) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'desired_foot_orientation_in_world) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'actual_foot_position_in_world) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'actual_foot_orientation_in_world) istream)
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'unique_id) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<FootstepStatusRosMessage>)))
  "Returns string type for a message object of type '<FootstepStatusRosMessage>"
  "ihmc_msgs/FootstepStatusRosMessage")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'FootstepStatusRosMessage)))
  "Returns string type for a message object of type 'FootstepStatusRosMessage"
  "ihmc_msgs/FootstepStatusRosMessage")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<FootstepStatusRosMessage>)))
  "Returns md5sum for a message object of type '<FootstepStatusRosMessage>"
  "66721fbc5990add85a3987ffbe8de2ca")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'FootstepStatusRosMessage)))
  "Returns md5sum for a message object of type 'FootstepStatusRosMessage"
  "66721fbc5990add85a3987ffbe8de2ca")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<FootstepStatusRosMessage>)))
  "Returns full string definition for message of type '<FootstepStatusRosMessage>"
  (cl:format cl:nil "## FootstepStatusRosMessage~%# This message gives the status of the current footstep from the controller as well as the position~%# and orientation of the footstep in world cooredinates. ~%~%# The current footstep status enum value.~%uint8 status~%~%# footstepIndex starts at 0 and monotonically increases with each completed footstep in a given~%# FootstepDataListMessage.~%int32 footstep_index~%~%# The robot side (left or right) that this footstep status correlates to.~%uint8 robot_side~%~%# desiredFootPositionInWorld gives the position of the desired position sent to the controller as~%# opposed to where the foot actually landed~%geometry_msgs/Point desired_foot_position_in_world~%~%# desiredFootOrientationInWorld gives the desired orientation of the foot sent to the controller as~%# opposed to the orientation where the foot actually is~%geometry_msgs/Quaternion desired_foot_orientation_in_world~%~%# actualFootPositionInWorld gives the position of where the foot actually landed as opposed to the~%# desired position sent to the controller~%geometry_msgs/Point actual_foot_position_in_world~%~%# actualFootOrientationInWorld gives the orientation the foot is actually in as opposed to the desired~%# orientation sent to the controller~%geometry_msgs/Quaternion actual_foot_orientation_in_world~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%# This message utilizes \"enums\". Enum value information for this message follows.~%~%# \"robot_side\" enum values:~%uint8 LEFT=0 # refers to the LEFT side of a robot~%uint8 RIGHT=1 # refers to the RIGHT side of a robot~%~%# \"status\" enum values:~%uint8 STARTED=0 # execution of a footstep has begun. actualFootPositionInWorld and actualFootOrientationInWorld should be ignored in this state~%uint8 COMPLETED=1 # a footstep is completed~%~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'FootstepStatusRosMessage)))
  "Returns full string definition for message of type 'FootstepStatusRosMessage"
  (cl:format cl:nil "## FootstepStatusRosMessage~%# This message gives the status of the current footstep from the controller as well as the position~%# and orientation of the footstep in world cooredinates. ~%~%# The current footstep status enum value.~%uint8 status~%~%# footstepIndex starts at 0 and monotonically increases with each completed footstep in a given~%# FootstepDataListMessage.~%int32 footstep_index~%~%# The robot side (left or right) that this footstep status correlates to.~%uint8 robot_side~%~%# desiredFootPositionInWorld gives the position of the desired position sent to the controller as~%# opposed to where the foot actually landed~%geometry_msgs/Point desired_foot_position_in_world~%~%# desiredFootOrientationInWorld gives the desired orientation of the foot sent to the controller as~%# opposed to the orientation where the foot actually is~%geometry_msgs/Quaternion desired_foot_orientation_in_world~%~%# actualFootPositionInWorld gives the position of where the foot actually landed as opposed to the~%# desired position sent to the controller~%geometry_msgs/Point actual_foot_position_in_world~%~%# actualFootOrientationInWorld gives the orientation the foot is actually in as opposed to the desired~%# orientation sent to the controller~%geometry_msgs/Quaternion actual_foot_orientation_in_world~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%# This message utilizes \"enums\". Enum value information for this message follows.~%~%# \"robot_side\" enum values:~%uint8 LEFT=0 # refers to the LEFT side of a robot~%uint8 RIGHT=1 # refers to the RIGHT side of a robot~%~%# \"status\" enum values:~%uint8 STARTED=0 # execution of a footstep has begun. actualFootPositionInWorld and actualFootOrientationInWorld should be ignored in this state~%uint8 COMPLETED=1 # a footstep is completed~%~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <FootstepStatusRosMessage>))
  (cl:+ 0
     1
     4
     1
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'desired_foot_position_in_world))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'desired_foot_orientation_in_world))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'actual_foot_position_in_world))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'actual_foot_orientation_in_world))
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <FootstepStatusRosMessage>))
  "Converts a ROS message object to a list"
  (cl:list 'FootstepStatusRosMessage
    (cl:cons ':status (status msg))
    (cl:cons ':footstep_index (footstep_index msg))
    (cl:cons ':robot_side (robot_side msg))
    (cl:cons ':desired_foot_position_in_world (desired_foot_position_in_world msg))
    (cl:cons ':desired_foot_orientation_in_world (desired_foot_orientation_in_world msg))
    (cl:cons ':actual_foot_position_in_world (actual_foot_position_in_world msg))
    (cl:cons ':actual_foot_orientation_in_world (actual_foot_orientation_in_world msg))
    (cl:cons ':unique_id (unique_id msg))
))
