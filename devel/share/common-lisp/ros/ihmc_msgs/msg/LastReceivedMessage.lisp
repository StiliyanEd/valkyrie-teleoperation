; Auto-generated. Do not edit!


(cl:in-package ihmc_msgs-msg)


;//! \htmlinclude LastReceivedMessage.msg.html

(cl:defclass <LastReceivedMessage> (roslisp-msg-protocol:ros-message)
  ((type
    :reader type
    :initarg :type
    :type cl:string
    :initform "")
   (unique_id
    :reader unique_id
    :initarg :unique_id
    :type cl:integer
    :initform 0)
   (receive_timestamp
    :reader receive_timestamp
    :initarg :receive_timestamp
    :type cl:integer
    :initform 0)
   (time_since_last_received
    :reader time_since_last_received
    :initarg :time_since_last_received
    :type cl:float
    :initform 0.0))
)

(cl:defclass LastReceivedMessage (<LastReceivedMessage>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <LastReceivedMessage>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'LastReceivedMessage)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name ihmc_msgs-msg:<LastReceivedMessage> is deprecated: use ihmc_msgs-msg:LastReceivedMessage instead.")))

(cl:ensure-generic-function 'type-val :lambda-list '(m))
(cl:defmethod type-val ((m <LastReceivedMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:type-val is deprecated.  Use ihmc_msgs-msg:type instead.")
  (type m))

(cl:ensure-generic-function 'unique_id-val :lambda-list '(m))
(cl:defmethod unique_id-val ((m <LastReceivedMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:unique_id-val is deprecated.  Use ihmc_msgs-msg:unique_id instead.")
  (unique_id m))

(cl:ensure-generic-function 'receive_timestamp-val :lambda-list '(m))
(cl:defmethod receive_timestamp-val ((m <LastReceivedMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:receive_timestamp-val is deprecated.  Use ihmc_msgs-msg:receive_timestamp instead.")
  (receive_timestamp m))

(cl:ensure-generic-function 'time_since_last_received-val :lambda-list '(m))
(cl:defmethod time_since_last_received-val ((m <LastReceivedMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:time_since_last_received-val is deprecated.  Use ihmc_msgs-msg:time_since_last_received instead.")
  (time_since_last_received m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <LastReceivedMessage>) ostream)
  "Serializes a message object of type '<LastReceivedMessage>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'type))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'type))
  (cl:let* ((signed (cl:slot-value msg 'unique_id)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'receive_timestamp)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'time_since_last_received))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <LastReceivedMessage>) istream)
  "Deserializes a message object of type '<LastReceivedMessage>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'type) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'type) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'unique_id) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'receive_timestamp) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'time_since_last_received) (roslisp-utils:decode-double-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<LastReceivedMessage>)))
  "Returns string type for a message object of type '<LastReceivedMessage>"
  "ihmc_msgs/LastReceivedMessage")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'LastReceivedMessage)))
  "Returns string type for a message object of type 'LastReceivedMessage"
  "ihmc_msgs/LastReceivedMessage")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<LastReceivedMessage>)))
  "Returns md5sum for a message object of type '<LastReceivedMessage>"
  "1bfe87dec4247b73097478bd3aa9364d")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'LastReceivedMessage)))
  "Returns md5sum for a message object of type 'LastReceivedMessage"
  "1bfe87dec4247b73097478bd3aa9364d")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<LastReceivedMessage>)))
  "Returns full string definition for message of type '<LastReceivedMessage>"
  (cl:format cl:nil "## LastReceivedMessage~%# Last Received Message echo's back the ID and type of the last message received by the IHMC ROS API.~%# The type of the last message received~%string type~%~%# The Unique ID of the last message received.~%int64 unique_id~%~%# The timestamp at which the message was received.~%int64 receive_timestamp~%~%# The time since a message was received~%float64 time_since_last_received~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'LastReceivedMessage)))
  "Returns full string definition for message of type 'LastReceivedMessage"
  (cl:format cl:nil "## LastReceivedMessage~%# Last Received Message echo's back the ID and type of the last message received by the IHMC ROS API.~%# The type of the last message received~%string type~%~%# The Unique ID of the last message received.~%int64 unique_id~%~%# The timestamp at which the message was received.~%int64 receive_timestamp~%~%# The time since a message was received~%float64 time_since_last_received~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <LastReceivedMessage>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'type))
     8
     8
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <LastReceivedMessage>))
  "Converts a ROS message object to a list"
  (cl:list 'LastReceivedMessage
    (cl:cons ':type (type msg))
    (cl:cons ':unique_id (unique_id msg))
    (cl:cons ':receive_timestamp (receive_timestamp msg))
    (cl:cons ':time_since_last_received (time_since_last_received msg))
))
