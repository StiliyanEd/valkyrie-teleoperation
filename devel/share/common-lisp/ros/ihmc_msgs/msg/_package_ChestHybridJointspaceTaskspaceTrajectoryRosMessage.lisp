(cl:in-package ihmc_msgs-msg)
(cl:export '(EXECUTION_MODE-VAL
          EXECUTION_MODE
          PREVIOUS_MESSAGE_ID-VAL
          PREVIOUS_MESSAGE_ID
          UNIQUE_ID-VAL
          UNIQUE_ID
))