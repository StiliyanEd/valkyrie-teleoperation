(cl:in-package ihmc_msgs-msg)
(cl:export '(ROBOT_SIDE-VAL
          ROBOT_SIDE
          JOINT_TRAJECTORY_MESSAGES-VAL
          JOINT_TRAJECTORY_MESSAGES
          EXECUTION_MODE-VAL
          EXECUTION_MODE
          PREVIOUS_MESSAGE_ID-VAL
          PREVIOUS_MESSAGE_ID
          UNIQUE_ID-VAL
          UNIQUE_ID
))