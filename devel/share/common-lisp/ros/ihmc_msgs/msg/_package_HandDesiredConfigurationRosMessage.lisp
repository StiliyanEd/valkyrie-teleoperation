(cl:in-package ihmc_msgs-msg)
(cl:export '(ROBOT_SIDE-VAL
          ROBOT_SIDE
          HAND_DESIRED_CONFIGURATION-VAL
          HAND_DESIRED_CONFIGURATION
          UNIQUE_ID-VAL
          UNIQUE_ID
))