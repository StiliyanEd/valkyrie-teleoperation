; Auto-generated. Do not edit!


(cl:in-package ihmc_msgs-msg)


;//! \htmlinclude AbortWalkingRosMessage.msg.html

(cl:defclass <AbortWalkingRosMessage> (roslisp-msg-protocol:ros-message)
  ((unique_id
    :reader unique_id
    :initarg :unique_id
    :type cl:integer
    :initform 0))
)

(cl:defclass AbortWalkingRosMessage (<AbortWalkingRosMessage>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <AbortWalkingRosMessage>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'AbortWalkingRosMessage)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name ihmc_msgs-msg:<AbortWalkingRosMessage> is deprecated: use ihmc_msgs-msg:AbortWalkingRosMessage instead.")))

(cl:ensure-generic-function 'unique_id-val :lambda-list '(m))
(cl:defmethod unique_id-val ((m <AbortWalkingRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:unique_id-val is deprecated.  Use ihmc_msgs-msg:unique_id instead.")
  (unique_id m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <AbortWalkingRosMessage>) ostream)
  "Serializes a message object of type '<AbortWalkingRosMessage>"
  (cl:let* ((signed (cl:slot-value msg 'unique_id)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <AbortWalkingRosMessage>) istream)
  "Deserializes a message object of type '<AbortWalkingRosMessage>"
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'unique_id) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<AbortWalkingRosMessage>)))
  "Returns string type for a message object of type '<AbortWalkingRosMessage>"
  "ihmc_msgs/AbortWalkingRosMessage")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'AbortWalkingRosMessage)))
  "Returns string type for a message object of type 'AbortWalkingRosMessage"
  "ihmc_msgs/AbortWalkingRosMessage")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<AbortWalkingRosMessage>)))
  "Returns md5sum for a message object of type '<AbortWalkingRosMessage>"
  "72ca681aeeb68b4ac429d65851e2226f")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'AbortWalkingRosMessage)))
  "Returns md5sum for a message object of type 'AbortWalkingRosMessage"
  "72ca681aeeb68b4ac429d65851e2226f")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<AbortWalkingRosMessage>)))
  "Returns full string definition for message of type '<AbortWalkingRosMessage>"
  (cl:format cl:nil "## AbortWalkingRosMessage~%# This message is used to abort walking, forcing the robot to switch back to double support and clear~%# the footstep list.~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'AbortWalkingRosMessage)))
  "Returns full string definition for message of type 'AbortWalkingRosMessage"
  (cl:format cl:nil "## AbortWalkingRosMessage~%# This message is used to abort walking, forcing the robot to switch back to double support and clear~%# the footstep list.~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <AbortWalkingRosMessage>))
  (cl:+ 0
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <AbortWalkingRosMessage>))
  "Converts a ROS message object to a list"
  (cl:list 'AbortWalkingRosMessage
    (cl:cons ':unique_id (unique_id msg))
))
