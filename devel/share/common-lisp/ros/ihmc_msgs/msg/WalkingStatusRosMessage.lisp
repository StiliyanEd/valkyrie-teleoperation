; Auto-generated. Do not edit!


(cl:in-package ihmc_msgs-msg)


;//! \htmlinclude WalkingStatusRosMessage.msg.html

(cl:defclass <WalkingStatusRosMessage> (roslisp-msg-protocol:ros-message)
  ((status
    :reader status
    :initarg :status
    :type cl:fixnum
    :initform 0)
   (unique_id
    :reader unique_id
    :initarg :unique_id
    :type cl:integer
    :initform 0))
)

(cl:defclass WalkingStatusRosMessage (<WalkingStatusRosMessage>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <WalkingStatusRosMessage>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'WalkingStatusRosMessage)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name ihmc_msgs-msg:<WalkingStatusRosMessage> is deprecated: use ihmc_msgs-msg:WalkingStatusRosMessage instead.")))

(cl:ensure-generic-function 'status-val :lambda-list '(m))
(cl:defmethod status-val ((m <WalkingStatusRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:status-val is deprecated.  Use ihmc_msgs-msg:status instead.")
  (status m))

(cl:ensure-generic-function 'unique_id-val :lambda-list '(m))
(cl:defmethod unique_id-val ((m <WalkingStatusRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:unique_id-val is deprecated.  Use ihmc_msgs-msg:unique_id instead.")
  (unique_id m))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<WalkingStatusRosMessage>)))
    "Constants for message type '<WalkingStatusRosMessage>"
  '((:STARTED . 0)
    (:COMPLETED . 1)
    (:ABORT_REQUESTED . 2))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'WalkingStatusRosMessage)))
    "Constants for message type 'WalkingStatusRosMessage"
  '((:STARTED . 0)
    (:COMPLETED . 1)
    (:ABORT_REQUESTED . 2))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <WalkingStatusRosMessage>) ostream)
  "Serializes a message object of type '<WalkingStatusRosMessage>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'status)) ostream)
  (cl:let* ((signed (cl:slot-value msg 'unique_id)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <WalkingStatusRosMessage>) istream)
  "Deserializes a message object of type '<WalkingStatusRosMessage>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'status)) (cl:read-byte istream))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'unique_id) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<WalkingStatusRosMessage>)))
  "Returns string type for a message object of type '<WalkingStatusRosMessage>"
  "ihmc_msgs/WalkingStatusRosMessage")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'WalkingStatusRosMessage)))
  "Returns string type for a message object of type 'WalkingStatusRosMessage"
  "ihmc_msgs/WalkingStatusRosMessage")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<WalkingStatusRosMessage>)))
  "Returns md5sum for a message object of type '<WalkingStatusRosMessage>"
  "b0a73288471d327ae20e972dca1096b9")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'WalkingStatusRosMessage)))
  "Returns md5sum for a message object of type 'WalkingStatusRosMessage"
  "b0a73288471d327ae20e972dca1096b9")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<WalkingStatusRosMessage>)))
  "Returns full string definition for message of type '<WalkingStatusRosMessage>"
  (cl:format cl:nil "## WalkingStatusRosMessage~%# This class is used to report the status of walking.~%~%# Status of walking. Either STARTED, COMPLETED, or ABORT_REQUESTED.~%uint8 status~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%# This message utilizes \"enums\". Enum value information for this message follows.~%~%# \"status\" enum values:~%uint8 STARTED=0 # The robot has begun its initial transfer/sway at the start of a walking plan~%uint8 COMPLETED=1 # The robot has finished its final transfer/sway at the end of a walking plan~%uint8 ABORT_REQUESTED=2 # A walking abort has been requested~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'WalkingStatusRosMessage)))
  "Returns full string definition for message of type 'WalkingStatusRosMessage"
  (cl:format cl:nil "## WalkingStatusRosMessage~%# This class is used to report the status of walking.~%~%# Status of walking. Either STARTED, COMPLETED, or ABORT_REQUESTED.~%uint8 status~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%# This message utilizes \"enums\". Enum value information for this message follows.~%~%# \"status\" enum values:~%uint8 STARTED=0 # The robot has begun its initial transfer/sway at the start of a walking plan~%uint8 COMPLETED=1 # The robot has finished its final transfer/sway at the end of a walking plan~%uint8 ABORT_REQUESTED=2 # A walking abort has been requested~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <WalkingStatusRosMessage>))
  (cl:+ 0
     1
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <WalkingStatusRosMessage>))
  "Converts a ROS message object to a list"
  (cl:list 'WalkingStatusRosMessage
    (cl:cons ':status (status msg))
    (cl:cons ':unique_id (unique_id msg))
))
