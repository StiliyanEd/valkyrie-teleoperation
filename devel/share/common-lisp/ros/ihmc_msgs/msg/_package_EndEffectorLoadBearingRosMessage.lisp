(cl:in-package ihmc_msgs-msg)
(cl:export '(ROBOT_SIDE-VAL
          ROBOT_SIDE
          END_EFFECTOR-VAL
          END_EFFECTOR
          REQUEST-VAL
          REQUEST
          UNIQUE_ID-VAL
          UNIQUE_ID
))