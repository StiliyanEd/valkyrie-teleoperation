(cl:in-package ihmc_msgs-msg)
(cl:export '(WEIGHT-VAL
          WEIGHT
          TRAJECTORY_POINTS-VAL
          TRAJECTORY_POINTS
          UNIQUE_ID-VAL
          UNIQUE_ID
))