(cl:in-package ihmc_msgs-msg)
(cl:export '(TIME-VAL
          TIME
          POSITION-VAL
          POSITION
          VELOCITY-VAL
          VELOCITY
          UNIQUE_ID-VAL
          UNIQUE_ID
))