; Auto-generated. Do not edit!


(cl:in-package ihmc_msgs-msg)


;//! \htmlinclude EuclideanTrajectoryPointRosMessage.msg.html

(cl:defclass <EuclideanTrajectoryPointRosMessage> (roslisp-msg-protocol:ros-message)
  ((time
    :reader time
    :initarg :time
    :type cl:float
    :initform 0.0)
   (position
    :reader position
    :initarg :position
    :type geometry_msgs-msg:Point
    :initform (cl:make-instance 'geometry_msgs-msg:Point))
   (linear_velocity
    :reader linear_velocity
    :initarg :linear_velocity
    :type geometry_msgs-msg:Vector3
    :initform (cl:make-instance 'geometry_msgs-msg:Vector3))
   (unique_id
    :reader unique_id
    :initarg :unique_id
    :type cl:integer
    :initform 0))
)

(cl:defclass EuclideanTrajectoryPointRosMessage (<EuclideanTrajectoryPointRosMessage>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <EuclideanTrajectoryPointRosMessage>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'EuclideanTrajectoryPointRosMessage)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name ihmc_msgs-msg:<EuclideanTrajectoryPointRosMessage> is deprecated: use ihmc_msgs-msg:EuclideanTrajectoryPointRosMessage instead.")))

(cl:ensure-generic-function 'time-val :lambda-list '(m))
(cl:defmethod time-val ((m <EuclideanTrajectoryPointRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:time-val is deprecated.  Use ihmc_msgs-msg:time instead.")
  (time m))

(cl:ensure-generic-function 'position-val :lambda-list '(m))
(cl:defmethod position-val ((m <EuclideanTrajectoryPointRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:position-val is deprecated.  Use ihmc_msgs-msg:position instead.")
  (position m))

(cl:ensure-generic-function 'linear_velocity-val :lambda-list '(m))
(cl:defmethod linear_velocity-val ((m <EuclideanTrajectoryPointRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:linear_velocity-val is deprecated.  Use ihmc_msgs-msg:linear_velocity instead.")
  (linear_velocity m))

(cl:ensure-generic-function 'unique_id-val :lambda-list '(m))
(cl:defmethod unique_id-val ((m <EuclideanTrajectoryPointRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:unique_id-val is deprecated.  Use ihmc_msgs-msg:unique_id instead.")
  (unique_id m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <EuclideanTrajectoryPointRosMessage>) ostream)
  "Serializes a message object of type '<EuclideanTrajectoryPointRosMessage>"
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'time))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'position) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'linear_velocity) ostream)
  (cl:let* ((signed (cl:slot-value msg 'unique_id)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <EuclideanTrajectoryPointRosMessage>) istream)
  "Deserializes a message object of type '<EuclideanTrajectoryPointRosMessage>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'time) (roslisp-utils:decode-double-float-bits bits)))
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'position) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'linear_velocity) istream)
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'unique_id) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<EuclideanTrajectoryPointRosMessage>)))
  "Returns string type for a message object of type '<EuclideanTrajectoryPointRosMessage>"
  "ihmc_msgs/EuclideanTrajectoryPointRosMessage")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'EuclideanTrajectoryPointRosMessage)))
  "Returns string type for a message object of type 'EuclideanTrajectoryPointRosMessage"
  "ihmc_msgs/EuclideanTrajectoryPointRosMessage")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<EuclideanTrajectoryPointRosMessage>)))
  "Returns md5sum for a message object of type '<EuclideanTrajectoryPointRosMessage>"
  "73f74ff49bd13b2882ce3a1262278f0a")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'EuclideanTrajectoryPointRosMessage)))
  "Returns md5sum for a message object of type 'EuclideanTrajectoryPointRosMessage"
  "73f74ff49bd13b2882ce3a1262278f0a")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<EuclideanTrajectoryPointRosMessage>)))
  "Returns full string definition for message of type '<EuclideanTrajectoryPointRosMessage>"
  (cl:format cl:nil "## EuclideanTrajectoryPointRosMessage~%# This class is used to build trajectory messages in taskspace. It holds the only the translational~%# information for one trajectory point (position & linear velocity). Feel free to look at~%# SO3TrajectoryPointMessage (rotational) and SE3TrajectoryPointMessage (rotational AND translational)~%~%# Time at which the trajectory point has to be reached. The time is relative to when the trajectory~%# starts.~%float64 time~%~%# Define the desired 3D position to be reached at this trajectory point. It is expressed in world~%# frame.~%geometry_msgs/Point position~%~%# Define the desired 3D linear velocity to be reached at this trajectory point. It is expressed in~%# world frame.~%geometry_msgs/Vector3 linear_velocity~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%# It is only meant to represent a direction. Therefore, it does not~%# make sense to apply a translation to it (e.g., when applying a ~%# generic rigid transformation to a Vector3, tf2 will only apply the~%# rotation). If you want your data to be translatable too, use the~%# geometry_msgs/Point message instead.~%~%float64 x~%float64 y~%float64 z~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'EuclideanTrajectoryPointRosMessage)))
  "Returns full string definition for message of type 'EuclideanTrajectoryPointRosMessage"
  (cl:format cl:nil "## EuclideanTrajectoryPointRosMessage~%# This class is used to build trajectory messages in taskspace. It holds the only the translational~%# information for one trajectory point (position & linear velocity). Feel free to look at~%# SO3TrajectoryPointMessage (rotational) and SE3TrajectoryPointMessage (rotational AND translational)~%~%# Time at which the trajectory point has to be reached. The time is relative to when the trajectory~%# starts.~%float64 time~%~%# Define the desired 3D position to be reached at this trajectory point. It is expressed in world~%# frame.~%geometry_msgs/Point position~%~%# Define the desired 3D linear velocity to be reached at this trajectory point. It is expressed in~%# world frame.~%geometry_msgs/Vector3 linear_velocity~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%# It is only meant to represent a direction. Therefore, it does not~%# make sense to apply a translation to it (e.g., when applying a ~%# generic rigid transformation to a Vector3, tf2 will only apply the~%# rotation). If you want your data to be translatable too, use the~%# geometry_msgs/Point message instead.~%~%float64 x~%float64 y~%float64 z~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <EuclideanTrajectoryPointRosMessage>))
  (cl:+ 0
     8
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'position))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'linear_velocity))
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <EuclideanTrajectoryPointRosMessage>))
  "Converts a ROS message object to a list"
  (cl:list 'EuclideanTrajectoryPointRosMessage
    (cl:cons ':time (time msg))
    (cl:cons ':position (position msg))
    (cl:cons ':linear_velocity (linear_velocity msg))
    (cl:cons ':unique_id (unique_id msg))
))
