; Auto-generated. Do not edit!


(cl:in-package ihmc_msgs-msg)


;//! \htmlinclude PauseWalkingRosMessage.msg.html

(cl:defclass <PauseWalkingRosMessage> (roslisp-msg-protocol:ros-message)
  ((pause
    :reader pause
    :initarg :pause
    :type cl:boolean
    :initform cl:nil)
   (unique_id
    :reader unique_id
    :initarg :unique_id
    :type cl:integer
    :initform 0))
)

(cl:defclass PauseWalkingRosMessage (<PauseWalkingRosMessage>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <PauseWalkingRosMessage>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'PauseWalkingRosMessage)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name ihmc_msgs-msg:<PauseWalkingRosMessage> is deprecated: use ihmc_msgs-msg:PauseWalkingRosMessage instead.")))

(cl:ensure-generic-function 'pause-val :lambda-list '(m))
(cl:defmethod pause-val ((m <PauseWalkingRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:pause-val is deprecated.  Use ihmc_msgs-msg:pause instead.")
  (pause m))

(cl:ensure-generic-function 'unique_id-val :lambda-list '(m))
(cl:defmethod unique_id-val ((m <PauseWalkingRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:unique_id-val is deprecated.  Use ihmc_msgs-msg:unique_id instead.")
  (unique_id m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <PauseWalkingRosMessage>) ostream)
  "Serializes a message object of type '<PauseWalkingRosMessage>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'pause) 1 0)) ostream)
  (cl:let* ((signed (cl:slot-value msg 'unique_id)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <PauseWalkingRosMessage>) istream)
  "Deserializes a message object of type '<PauseWalkingRosMessage>"
    (cl:setf (cl:slot-value msg 'pause) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'unique_id) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<PauseWalkingRosMessage>)))
  "Returns string type for a message object of type '<PauseWalkingRosMessage>"
  "ihmc_msgs/PauseWalkingRosMessage")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'PauseWalkingRosMessage)))
  "Returns string type for a message object of type 'PauseWalkingRosMessage"
  "ihmc_msgs/PauseWalkingRosMessage")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<PauseWalkingRosMessage>)))
  "Returns md5sum for a message object of type '<PauseWalkingRosMessage>"
  "08ff35fb6945697adbf23cc720fd0003")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'PauseWalkingRosMessage)))
  "Returns md5sum for a message object of type 'PauseWalkingRosMessage"
  "08ff35fb6945697adbf23cc720fd0003")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<PauseWalkingRosMessage>)))
  "Returns full string definition for message of type '<PauseWalkingRosMessage>"
  (cl:format cl:nil "## PauseWalkingRosMessage~%# This message pauses the execution of a list of footsteps. If this message is sent in the middle of~%# executing a footstep, the robot will finish the step and pause when back in double support. A~%# message with a unique id equals to 0 will be interpreted as invalid and will not be processed by the~%# controller.~%~%# True to pause walking, false to unpause and resume an existing plan.~%bool pause~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'PauseWalkingRosMessage)))
  "Returns full string definition for message of type 'PauseWalkingRosMessage"
  (cl:format cl:nil "## PauseWalkingRosMessage~%# This message pauses the execution of a list of footsteps. If this message is sent in the middle of~%# executing a footstep, the robot will finish the step and pause when back in double support. A~%# message with a unique id equals to 0 will be interpreted as invalid and will not be processed by the~%# controller.~%~%# True to pause walking, false to unpause and resume an existing plan.~%bool pause~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <PauseWalkingRosMessage>))
  (cl:+ 0
     1
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <PauseWalkingRosMessage>))
  "Converts a ROS message object to a list"
  (cl:list 'PauseWalkingRosMessage
    (cl:cons ':pause (pause msg))
    (cl:cons ':unique_id (unique_id msg))
))
