(cl:in-package ihmc_msgs-msg)
(cl:export '(TYPE-VAL
          TYPE
          UNIQUE_ID-VAL
          UNIQUE_ID
          RECEIVE_TIMESTAMP-VAL
          RECEIVE_TIMESTAMP
          TIME_SINCE_LAST_RECEIVED-VAL
          TIME_SINCE_LAST_RECEIVED
))