; Auto-generated. Do not edit!


(cl:in-package ihmc_msgs-msg)


;//! \htmlinclude ChestHybridJointspaceTaskspaceTrajectoryRosMessage.msg.html

(cl:defclass <ChestHybridJointspaceTaskspaceTrajectoryRosMessage> (roslisp-msg-protocol:ros-message)
  ((execution_mode
    :reader execution_mode
    :initarg :execution_mode
    :type cl:fixnum
    :initform 0)
   (previous_message_id
    :reader previous_message_id
    :initarg :previous_message_id
    :type cl:integer
    :initform 0)
   (unique_id
    :reader unique_id
    :initarg :unique_id
    :type cl:integer
    :initform 0))
)

(cl:defclass ChestHybridJointspaceTaskspaceTrajectoryRosMessage (<ChestHybridJointspaceTaskspaceTrajectoryRosMessage>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <ChestHybridJointspaceTaskspaceTrajectoryRosMessage>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'ChestHybridJointspaceTaskspaceTrajectoryRosMessage)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name ihmc_msgs-msg:<ChestHybridJointspaceTaskspaceTrajectoryRosMessage> is deprecated: use ihmc_msgs-msg:ChestHybridJointspaceTaskspaceTrajectoryRosMessage instead.")))

(cl:ensure-generic-function 'execution_mode-val :lambda-list '(m))
(cl:defmethod execution_mode-val ((m <ChestHybridJointspaceTaskspaceTrajectoryRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:execution_mode-val is deprecated.  Use ihmc_msgs-msg:execution_mode instead.")
  (execution_mode m))

(cl:ensure-generic-function 'previous_message_id-val :lambda-list '(m))
(cl:defmethod previous_message_id-val ((m <ChestHybridJointspaceTaskspaceTrajectoryRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:previous_message_id-val is deprecated.  Use ihmc_msgs-msg:previous_message_id instead.")
  (previous_message_id m))

(cl:ensure-generic-function 'unique_id-val :lambda-list '(m))
(cl:defmethod unique_id-val ((m <ChestHybridJointspaceTaskspaceTrajectoryRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:unique_id-val is deprecated.  Use ihmc_msgs-msg:unique_id instead.")
  (unique_id m))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<ChestHybridJointspaceTaskspaceTrajectoryRosMessage>)))
    "Constants for message type '<ChestHybridJointspaceTaskspaceTrajectoryRosMessage>"
  '((:OVERRIDE . 0)
    (:QUEUE . 1))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'ChestHybridJointspaceTaskspaceTrajectoryRosMessage)))
    "Constants for message type 'ChestHybridJointspaceTaskspaceTrajectoryRosMessage"
  '((:OVERRIDE . 0)
    (:QUEUE . 1))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <ChestHybridJointspaceTaskspaceTrajectoryRosMessage>) ostream)
  "Serializes a message object of type '<ChestHybridJointspaceTaskspaceTrajectoryRosMessage>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'execution_mode)) ostream)
  (cl:let* ((signed (cl:slot-value msg 'previous_message_id)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'unique_id)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <ChestHybridJointspaceTaskspaceTrajectoryRosMessage>) istream)
  "Deserializes a message object of type '<ChestHybridJointspaceTaskspaceTrajectoryRosMessage>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'execution_mode)) (cl:read-byte istream))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'previous_message_id) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'unique_id) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<ChestHybridJointspaceTaskspaceTrajectoryRosMessage>)))
  "Returns string type for a message object of type '<ChestHybridJointspaceTaskspaceTrajectoryRosMessage>"
  "ihmc_msgs/ChestHybridJointspaceTaskspaceTrajectoryRosMessage")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'ChestHybridJointspaceTaskspaceTrajectoryRosMessage)))
  "Returns string type for a message object of type 'ChestHybridJointspaceTaskspaceTrajectoryRosMessage"
  "ihmc_msgs/ChestHybridJointspaceTaskspaceTrajectoryRosMessage")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<ChestHybridJointspaceTaskspaceTrajectoryRosMessage>)))
  "Returns md5sum for a message object of type '<ChestHybridJointspaceTaskspaceTrajectoryRosMessage>"
  "ca69dba5f95c9297dfc625b4f9d1fa1a")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'ChestHybridJointspaceTaskspaceTrajectoryRosMessage)))
  "Returns md5sum for a message object of type 'ChestHybridJointspaceTaskspaceTrajectoryRosMessage"
  "ca69dba5f95c9297dfc625b4f9d1fa1a")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<ChestHybridJointspaceTaskspaceTrajectoryRosMessage>)))
  "Returns full string definition for message of type '<ChestHybridJointspaceTaskspaceTrajectoryRosMessage>"
  (cl:format cl:nil "## ChestHybridJointspaceTaskspaceTrajectoryRosMessage~%# This message commands the controller to move the chest in both taskspace amd jointspace to the~%# desired orientation and joint angles while going through the specified trajectory points.~%~%# When OVERRIDE is chosen:  - The time of the first trajectory point can be zero, in which case the~%# controller will start directly at the first trajectory point. Otherwise the controller will prepend~%# a first trajectory point at the current desired position.  When QUEUE is chosen:  - The message must~%# carry the ID of the message it should be queued to.  - The very first message of a list of queued~%# messages has to be an OVERRIDE message.  - The trajectory point times are relative to the the last~%# trajectory point time of the previous message.  - The controller will queue the joint trajectory~%# messages as a per joint basis. The first trajectory point has to be greater than zero.~%uint8 execution_mode~%~%# Only needed when using QUEUE mode, it refers to the message Id to which this message should be~%# queued to. It is used by the controller to ensure that no message has been lost on the way. If a~%# message appears to be missing (previousMessageId different from the last message ID received by the~%# controller), the motion is aborted. If previousMessageId == 0, the controller will not check for the~%# ID of the last received message.~%int64 previous_message_id~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%# This message utilizes \"enums\". Enum value information for this message follows.~%~%# \"execution_mode\" enum values:~%uint8 OVERRIDE=0 # This message will override the previous.~%uint8 QUEUE=1 # The previous message will first be executed before executing this message. When sending a series of queued messages, the very first has to be declared as OVERRIDE.~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'ChestHybridJointspaceTaskspaceTrajectoryRosMessage)))
  "Returns full string definition for message of type 'ChestHybridJointspaceTaskspaceTrajectoryRosMessage"
  (cl:format cl:nil "## ChestHybridJointspaceTaskspaceTrajectoryRosMessage~%# This message commands the controller to move the chest in both taskspace amd jointspace to the~%# desired orientation and joint angles while going through the specified trajectory points.~%~%# When OVERRIDE is chosen:  - The time of the first trajectory point can be zero, in which case the~%# controller will start directly at the first trajectory point. Otherwise the controller will prepend~%# a first trajectory point at the current desired position.  When QUEUE is chosen:  - The message must~%# carry the ID of the message it should be queued to.  - The very first message of a list of queued~%# messages has to be an OVERRIDE message.  - The trajectory point times are relative to the the last~%# trajectory point time of the previous message.  - The controller will queue the joint trajectory~%# messages as a per joint basis. The first trajectory point has to be greater than zero.~%uint8 execution_mode~%~%# Only needed when using QUEUE mode, it refers to the message Id to which this message should be~%# queued to. It is used by the controller to ensure that no message has been lost on the way. If a~%# message appears to be missing (previousMessageId different from the last message ID received by the~%# controller), the motion is aborted. If previousMessageId == 0, the controller will not check for the~%# ID of the last received message.~%int64 previous_message_id~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%# This message utilizes \"enums\". Enum value information for this message follows.~%~%# \"execution_mode\" enum values:~%uint8 OVERRIDE=0 # This message will override the previous.~%uint8 QUEUE=1 # The previous message will first be executed before executing this message. When sending a series of queued messages, the very first has to be declared as OVERRIDE.~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <ChestHybridJointspaceTaskspaceTrajectoryRosMessage>))
  (cl:+ 0
     1
     8
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <ChestHybridJointspaceTaskspaceTrajectoryRosMessage>))
  "Converts a ROS message object to a list"
  (cl:list 'ChestHybridJointspaceTaskspaceTrajectoryRosMessage
    (cl:cons ':execution_mode (execution_mode msg))
    (cl:cons ':previous_message_id (previous_message_id msg))
    (cl:cons ':unique_id (unique_id msg))
))
