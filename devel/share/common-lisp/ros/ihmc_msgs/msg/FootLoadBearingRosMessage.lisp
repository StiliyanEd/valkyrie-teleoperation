; Auto-generated. Do not edit!


(cl:in-package ihmc_msgs-msg)


;//! \htmlinclude FootLoadBearingRosMessage.msg.html

(cl:defclass <FootLoadBearingRosMessage> (roslisp-msg-protocol:ros-message)
  ((robot_side
    :reader robot_side
    :initarg :robot_side
    :type cl:fixnum
    :initform 0)
   (request
    :reader request
    :initarg :request
    :type cl:fixnum
    :initform 0)
   (unique_id
    :reader unique_id
    :initarg :unique_id
    :type cl:integer
    :initform 0))
)

(cl:defclass FootLoadBearingRosMessage (<FootLoadBearingRosMessage>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <FootLoadBearingRosMessage>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'FootLoadBearingRosMessage)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name ihmc_msgs-msg:<FootLoadBearingRosMessage> is deprecated: use ihmc_msgs-msg:FootLoadBearingRosMessage instead.")))

(cl:ensure-generic-function 'robot_side-val :lambda-list '(m))
(cl:defmethod robot_side-val ((m <FootLoadBearingRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:robot_side-val is deprecated.  Use ihmc_msgs-msg:robot_side instead.")
  (robot_side m))

(cl:ensure-generic-function 'request-val :lambda-list '(m))
(cl:defmethod request-val ((m <FootLoadBearingRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:request-val is deprecated.  Use ihmc_msgs-msg:request instead.")
  (request m))

(cl:ensure-generic-function 'unique_id-val :lambda-list '(m))
(cl:defmethod unique_id-val ((m <FootLoadBearingRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:unique_id-val is deprecated.  Use ihmc_msgs-msg:unique_id instead.")
  (unique_id m))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<FootLoadBearingRosMessage>)))
    "Constants for message type '<FootLoadBearingRosMessage>"
  '((:LEFT . 0)
    (:RIGHT . 1)
    (:LOAD . 0)
    (:UNLOAD . 1))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'FootLoadBearingRosMessage)))
    "Constants for message type 'FootLoadBearingRosMessage"
  '((:LEFT . 0)
    (:RIGHT . 1)
    (:LOAD . 0)
    (:UNLOAD . 1))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <FootLoadBearingRosMessage>) ostream)
  "Serializes a message object of type '<FootLoadBearingRosMessage>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'robot_side)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'request)) ostream)
  (cl:let* ((signed (cl:slot-value msg 'unique_id)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <FootLoadBearingRosMessage>) istream)
  "Deserializes a message object of type '<FootLoadBearingRosMessage>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'robot_side)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'request)) (cl:read-byte istream))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'unique_id) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<FootLoadBearingRosMessage>)))
  "Returns string type for a message object of type '<FootLoadBearingRosMessage>"
  "ihmc_msgs/FootLoadBearingRosMessage")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'FootLoadBearingRosMessage)))
  "Returns string type for a message object of type 'FootLoadBearingRosMessage"
  "ihmc_msgs/FootLoadBearingRosMessage")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<FootLoadBearingRosMessage>)))
  "Returns md5sum for a message object of type '<FootLoadBearingRosMessage>"
  "f1438e1fc87468e72f62ca38b2e425aa")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'FootLoadBearingRosMessage)))
  "Returns md5sum for a message object of type 'FootLoadBearingRosMessage"
  "f1438e1fc87468e72f62ca38b2e425aa")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<FootLoadBearingRosMessage>)))
  "Returns full string definition for message of type '<FootLoadBearingRosMessage>"
  (cl:format cl:nil "## FootLoadBearingRosMessage~%# This message commands the controller to start loading a foot that was unloaded to support the robot~%# weight.  When the robot is performing a 'flamingo stance' (one foot in the air not actually walking)~%# and the user wants the robot to switch back to double support. A message with a unique id equals to~%# 0 will be interpreted as invalid and will not be processed by the controller.~%~%# Needed to identify a side dependent end-effector.~%uint8 robot_side~%~%# Wether the end-effector should be loaded or unloaded.~%uint8 request~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%# This message utilizes \"enums\". Enum value information for this message follows.~%~%# \"robot_side\" enum values:~%uint8 LEFT=0 # refers to the LEFT side of a robot~%uint8 RIGHT=1 # refers to the RIGHT side of a robot~%~%# \"load_bearing_request\" enum values:~%uint8 LOAD=0 # Request to load the given end-effector.~%uint8 UNLOAD=1 # Request to unload the given end-effector.~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'FootLoadBearingRosMessage)))
  "Returns full string definition for message of type 'FootLoadBearingRosMessage"
  (cl:format cl:nil "## FootLoadBearingRosMessage~%# This message commands the controller to start loading a foot that was unloaded to support the robot~%# weight.  When the robot is performing a 'flamingo stance' (one foot in the air not actually walking)~%# and the user wants the robot to switch back to double support. A message with a unique id equals to~%# 0 will be interpreted as invalid and will not be processed by the controller.~%~%# Needed to identify a side dependent end-effector.~%uint8 robot_side~%~%# Wether the end-effector should be loaded or unloaded.~%uint8 request~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%# This message utilizes \"enums\". Enum value information for this message follows.~%~%# \"robot_side\" enum values:~%uint8 LEFT=0 # refers to the LEFT side of a robot~%uint8 RIGHT=1 # refers to the RIGHT side of a robot~%~%# \"load_bearing_request\" enum values:~%uint8 LOAD=0 # Request to load the given end-effector.~%uint8 UNLOAD=1 # Request to unload the given end-effector.~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <FootLoadBearingRosMessage>))
  (cl:+ 0
     1
     1
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <FootLoadBearingRosMessage>))
  "Converts a ROS message object to a list"
  (cl:list 'FootLoadBearingRosMessage
    (cl:cons ':robot_side (robot_side msg))
    (cl:cons ':request (request msg))
    (cl:cons ':unique_id (unique_id msg))
))
