(cl:in-package ihmc_msgs-msg)
(cl:export '(ROBOT_SIDE-VAL
          ROBOT_SIDE
          DESIRED_JOINT_ACCELERATIONS-VAL
          DESIRED_JOINT_ACCELERATIONS
          UNIQUE_ID-VAL
          UNIQUE_ID
))