(cl:in-package ihmc_msgs-msg)
(cl:export '(TIME-VAL
          TIME
          POSITION-VAL
          POSITION
          LINEAR_VELOCITY-VAL
          LINEAR_VELOCITY
          UNIQUE_ID-VAL
          UNIQUE_ID
))