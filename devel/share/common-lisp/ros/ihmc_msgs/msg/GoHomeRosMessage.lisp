; Auto-generated. Do not edit!


(cl:in-package ihmc_msgs-msg)


;//! \htmlinclude GoHomeRosMessage.msg.html

(cl:defclass <GoHomeRosMessage> (roslisp-msg-protocol:ros-message)
  ((body_part
    :reader body_part
    :initarg :body_part
    :type cl:fixnum
    :initform 0)
   (robot_side
    :reader robot_side
    :initarg :robot_side
    :type cl:fixnum
    :initform 0)
   (trajectory_time
    :reader trajectory_time
    :initarg :trajectory_time
    :type cl:float
    :initform 0.0)
   (unique_id
    :reader unique_id
    :initarg :unique_id
    :type cl:integer
    :initform 0))
)

(cl:defclass GoHomeRosMessage (<GoHomeRosMessage>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <GoHomeRosMessage>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'GoHomeRosMessage)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name ihmc_msgs-msg:<GoHomeRosMessage> is deprecated: use ihmc_msgs-msg:GoHomeRosMessage instead.")))

(cl:ensure-generic-function 'body_part-val :lambda-list '(m))
(cl:defmethod body_part-val ((m <GoHomeRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:body_part-val is deprecated.  Use ihmc_msgs-msg:body_part instead.")
  (body_part m))

(cl:ensure-generic-function 'robot_side-val :lambda-list '(m))
(cl:defmethod robot_side-val ((m <GoHomeRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:robot_side-val is deprecated.  Use ihmc_msgs-msg:robot_side instead.")
  (robot_side m))

(cl:ensure-generic-function 'trajectory_time-val :lambda-list '(m))
(cl:defmethod trajectory_time-val ((m <GoHomeRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:trajectory_time-val is deprecated.  Use ihmc_msgs-msg:trajectory_time instead.")
  (trajectory_time m))

(cl:ensure-generic-function 'unique_id-val :lambda-list '(m))
(cl:defmethod unique_id-val ((m <GoHomeRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:unique_id-val is deprecated.  Use ihmc_msgs-msg:unique_id instead.")
  (unique_id m))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<GoHomeRosMessage>)))
    "Constants for message type '<GoHomeRosMessage>"
  '((:ARM . 0)
    (:CHEST . 1)
    (:PELVIS . 2)
    (:LEFT . 0)
    (:RIGHT . 1))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'GoHomeRosMessage)))
    "Constants for message type 'GoHomeRosMessage"
  '((:ARM . 0)
    (:CHEST . 1)
    (:PELVIS . 2)
    (:LEFT . 0)
    (:RIGHT . 1))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <GoHomeRosMessage>) ostream)
  "Serializes a message object of type '<GoHomeRosMessage>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'body_part)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'robot_side)) ostream)
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'trajectory_time))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let* ((signed (cl:slot-value msg 'unique_id)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <GoHomeRosMessage>) istream)
  "Deserializes a message object of type '<GoHomeRosMessage>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'body_part)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'robot_side)) (cl:read-byte istream))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'trajectory_time) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'unique_id) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<GoHomeRosMessage>)))
  "Returns string type for a message object of type '<GoHomeRosMessage>"
  "ihmc_msgs/GoHomeRosMessage")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'GoHomeRosMessage)))
  "Returns string type for a message object of type 'GoHomeRosMessage"
  "ihmc_msgs/GoHomeRosMessage")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<GoHomeRosMessage>)))
  "Returns md5sum for a message object of type '<GoHomeRosMessage>"
  "9525f8d8f78f6bd3a198afa9a1376cb4")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'GoHomeRosMessage)))
  "Returns md5sum for a message object of type 'GoHomeRosMessage"
  "9525f8d8f78f6bd3a198afa9a1376cb4")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<GoHomeRosMessage>)))
  "Returns full string definition for message of type '<GoHomeRosMessage>"
  (cl:format cl:nil "## GoHomeRosMessage~%# The message commands the controller to bring the given part of the body back to a default~%# configuration called 'home'. It is useful to get back to a safe configuration before walking.~%~%# Specifies the part of the body the user wants to move back to it home configuration.~%uint8 body_part~%~%# Needed to identify a side dependent end-effector.~%uint8 robot_side~%~%# How long the trajectory will spline from the current desired to the home configuration.~%float64 trajectory_time~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%# This message utilizes \"enums\". Enum value information for this message follows.~%~%# \"body_part\" enum values:~%uint8 ARM=0 # Request the chest to go back to a straight up configuration.~%uint8 CHEST=1 # Request the arm to go to a preconfigured home configuration that is elbow lightly flexed, forearm pointing forward, and upper pointing downward.~%uint8 PELVIS=2 # Request the pelvis to go back to between the feet, zero pitch and roll, and headed in the same direction as the feet.~%~%# \"robot_side\" enum values:~%uint8 LEFT=0 # refers to the LEFT side of a robot~%uint8 RIGHT=1 # refers to the RIGHT side of a robot~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'GoHomeRosMessage)))
  "Returns full string definition for message of type 'GoHomeRosMessage"
  (cl:format cl:nil "## GoHomeRosMessage~%# The message commands the controller to bring the given part of the body back to a default~%# configuration called 'home'. It is useful to get back to a safe configuration before walking.~%~%# Specifies the part of the body the user wants to move back to it home configuration.~%uint8 body_part~%~%# Needed to identify a side dependent end-effector.~%uint8 robot_side~%~%# How long the trajectory will spline from the current desired to the home configuration.~%float64 trajectory_time~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%# This message utilizes \"enums\". Enum value information for this message follows.~%~%# \"body_part\" enum values:~%uint8 ARM=0 # Request the chest to go back to a straight up configuration.~%uint8 CHEST=1 # Request the arm to go to a preconfigured home configuration that is elbow lightly flexed, forearm pointing forward, and upper pointing downward.~%uint8 PELVIS=2 # Request the pelvis to go back to between the feet, zero pitch and roll, and headed in the same direction as the feet.~%~%# \"robot_side\" enum values:~%uint8 LEFT=0 # refers to the LEFT side of a robot~%uint8 RIGHT=1 # refers to the RIGHT side of a robot~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <GoHomeRosMessage>))
  (cl:+ 0
     1
     1
     8
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <GoHomeRosMessage>))
  "Converts a ROS message object to a list"
  (cl:list 'GoHomeRosMessage
    (cl:cons ':body_part (body_part msg))
    (cl:cons ':robot_side (robot_side msg))
    (cl:cons ':trajectory_time (trajectory_time msg))
    (cl:cons ':unique_id (unique_id msg))
))
