; Auto-generated. Do not edit!


(cl:in-package ihmc_msgs-msg)


;//! \htmlinclude FootstepDataRosMessage.msg.html

(cl:defclass <FootstepDataRosMessage> (roslisp-msg-protocol:ros-message)
  ((robot_side
    :reader robot_side
    :initarg :robot_side
    :type cl:fixnum
    :initform 0)
   (location
    :reader location
    :initarg :location
    :type geometry_msgs-msg:Point
    :initform (cl:make-instance 'geometry_msgs-msg:Point))
   (orientation
    :reader orientation
    :initarg :orientation
    :type geometry_msgs-msg:Quaternion
    :initform (cl:make-instance 'geometry_msgs-msg:Quaternion))
   (predicted_contact_points
    :reader predicted_contact_points
    :initarg :predicted_contact_points
    :type (cl:vector ihmc_msgs-msg:Point2dRosMessage)
   :initform (cl:make-array 0 :element-type 'ihmc_msgs-msg:Point2dRosMessage :initial-element (cl:make-instance 'ihmc_msgs-msg:Point2dRosMessage)))
   (trajectory_type
    :reader trajectory_type
    :initarg :trajectory_type
    :type cl:fixnum
    :initform 0)
   (swing_height
    :reader swing_height
    :initarg :swing_height
    :type cl:float
    :initform 0.0)
   (position_waypoints
    :reader position_waypoints
    :initarg :position_waypoints
    :type (cl:vector geometry_msgs-msg:Point)
   :initform (cl:make-array 0 :element-type 'geometry_msgs-msg:Point :initial-element (cl:make-instance 'geometry_msgs-msg:Point)))
   (swing_trajectory
    :reader swing_trajectory
    :initarg :swing_trajectory
    :type (cl:vector ihmc_msgs-msg:SE3TrajectoryPointRosMessage)
   :initform (cl:make-array 0 :element-type 'ihmc_msgs-msg:SE3TrajectoryPointRosMessage :initial-element (cl:make-instance 'ihmc_msgs-msg:SE3TrajectoryPointRosMessage)))
   (swing_trajectory_blend_duration
    :reader swing_trajectory_blend_duration
    :initarg :swing_trajectory_blend_duration
    :type cl:float
    :initform 0.0)
   (swing_duration
    :reader swing_duration
    :initarg :swing_duration
    :type cl:float
    :initform 0.0)
   (transfer_duration
    :reader transfer_duration
    :initarg :transfer_duration
    :type cl:float
    :initform 0.0)
   (unique_id
    :reader unique_id
    :initarg :unique_id
    :type cl:integer
    :initform 0))
)

(cl:defclass FootstepDataRosMessage (<FootstepDataRosMessage>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <FootstepDataRosMessage>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'FootstepDataRosMessage)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name ihmc_msgs-msg:<FootstepDataRosMessage> is deprecated: use ihmc_msgs-msg:FootstepDataRosMessage instead.")))

(cl:ensure-generic-function 'robot_side-val :lambda-list '(m))
(cl:defmethod robot_side-val ((m <FootstepDataRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:robot_side-val is deprecated.  Use ihmc_msgs-msg:robot_side instead.")
  (robot_side m))

(cl:ensure-generic-function 'location-val :lambda-list '(m))
(cl:defmethod location-val ((m <FootstepDataRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:location-val is deprecated.  Use ihmc_msgs-msg:location instead.")
  (location m))

(cl:ensure-generic-function 'orientation-val :lambda-list '(m))
(cl:defmethod orientation-val ((m <FootstepDataRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:orientation-val is deprecated.  Use ihmc_msgs-msg:orientation instead.")
  (orientation m))

(cl:ensure-generic-function 'predicted_contact_points-val :lambda-list '(m))
(cl:defmethod predicted_contact_points-val ((m <FootstepDataRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:predicted_contact_points-val is deprecated.  Use ihmc_msgs-msg:predicted_contact_points instead.")
  (predicted_contact_points m))

(cl:ensure-generic-function 'trajectory_type-val :lambda-list '(m))
(cl:defmethod trajectory_type-val ((m <FootstepDataRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:trajectory_type-val is deprecated.  Use ihmc_msgs-msg:trajectory_type instead.")
  (trajectory_type m))

(cl:ensure-generic-function 'swing_height-val :lambda-list '(m))
(cl:defmethod swing_height-val ((m <FootstepDataRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:swing_height-val is deprecated.  Use ihmc_msgs-msg:swing_height instead.")
  (swing_height m))

(cl:ensure-generic-function 'position_waypoints-val :lambda-list '(m))
(cl:defmethod position_waypoints-val ((m <FootstepDataRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:position_waypoints-val is deprecated.  Use ihmc_msgs-msg:position_waypoints instead.")
  (position_waypoints m))

(cl:ensure-generic-function 'swing_trajectory-val :lambda-list '(m))
(cl:defmethod swing_trajectory-val ((m <FootstepDataRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:swing_trajectory-val is deprecated.  Use ihmc_msgs-msg:swing_trajectory instead.")
  (swing_trajectory m))

(cl:ensure-generic-function 'swing_trajectory_blend_duration-val :lambda-list '(m))
(cl:defmethod swing_trajectory_blend_duration-val ((m <FootstepDataRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:swing_trajectory_blend_duration-val is deprecated.  Use ihmc_msgs-msg:swing_trajectory_blend_duration instead.")
  (swing_trajectory_blend_duration m))

(cl:ensure-generic-function 'swing_duration-val :lambda-list '(m))
(cl:defmethod swing_duration-val ((m <FootstepDataRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:swing_duration-val is deprecated.  Use ihmc_msgs-msg:swing_duration instead.")
  (swing_duration m))

(cl:ensure-generic-function 'transfer_duration-val :lambda-list '(m))
(cl:defmethod transfer_duration-val ((m <FootstepDataRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:transfer_duration-val is deprecated.  Use ihmc_msgs-msg:transfer_duration instead.")
  (transfer_duration m))

(cl:ensure-generic-function 'unique_id-val :lambda-list '(m))
(cl:defmethod unique_id-val ((m <FootstepDataRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ihmc_msgs-msg:unique_id-val is deprecated.  Use ihmc_msgs-msg:unique_id instead.")
  (unique_id m))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<FootstepDataRosMessage>)))
    "Constants for message type '<FootstepDataRosMessage>"
  '((:LEFT . 0)
    (:RIGHT . 1)
    (:DEFAULT . 0)
    (:OBSTACLE_CLEARANCE . 1)
    (:CUSTOM . 2)
    (:WAYPOINTS . 3))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'FootstepDataRosMessage)))
    "Constants for message type 'FootstepDataRosMessage"
  '((:LEFT . 0)
    (:RIGHT . 1)
    (:DEFAULT . 0)
    (:OBSTACLE_CLEARANCE . 1)
    (:CUSTOM . 2)
    (:WAYPOINTS . 3))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <FootstepDataRosMessage>) ostream)
  "Serializes a message object of type '<FootstepDataRosMessage>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'robot_side)) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'location) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'orientation) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'predicted_contact_points))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'predicted_contact_points))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'trajectory_type)) ostream)
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'swing_height))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'position_waypoints))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'position_waypoints))
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'swing_trajectory))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'swing_trajectory))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'swing_trajectory_blend_duration))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'swing_duration))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'transfer_duration))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let* ((signed (cl:slot-value msg 'unique_id)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <FootstepDataRosMessage>) istream)
  "Deserializes a message object of type '<FootstepDataRosMessage>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'robot_side)) (cl:read-byte istream))
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'location) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'orientation) istream)
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'predicted_contact_points) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'predicted_contact_points)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'ihmc_msgs-msg:Point2dRosMessage))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'trajectory_type)) (cl:read-byte istream))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'swing_height) (roslisp-utils:decode-double-float-bits bits)))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'position_waypoints) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'position_waypoints)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'geometry_msgs-msg:Point))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'swing_trajectory) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'swing_trajectory)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'ihmc_msgs-msg:SE3TrajectoryPointRosMessage))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'swing_trajectory_blend_duration) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'swing_duration) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'transfer_duration) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'unique_id) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<FootstepDataRosMessage>)))
  "Returns string type for a message object of type '<FootstepDataRosMessage>"
  "ihmc_msgs/FootstepDataRosMessage")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'FootstepDataRosMessage)))
  "Returns string type for a message object of type 'FootstepDataRosMessage"
  "ihmc_msgs/FootstepDataRosMessage")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<FootstepDataRosMessage>)))
  "Returns md5sum for a message object of type '<FootstepDataRosMessage>"
  "3ac3a1e9f8317a1e479220132a188752")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'FootstepDataRosMessage)))
  "Returns md5sum for a message object of type 'FootstepDataRosMessage"
  "3ac3a1e9f8317a1e479220132a188752")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<FootstepDataRosMessage>)))
  "Returns full string definition for message of type '<FootstepDataRosMessage>"
  (cl:format cl:nil "## FootstepDataRosMessage~%# This message specifies the position, orientation and side (left or right) of a desired footstep in~%# world frame.~%~%# Specifies which foot will swing to reach the foostep.~%uint8 robot_side~%~%# Specifies the position of the footstep (sole frame) in world frame.~%geometry_msgs/Point location~%~%# Specifies the orientation of the footstep (sole frame) in world frame.~%geometry_msgs/Quaternion orientation~%~%# predictedContactPoints specifies the vertices of the expected contact polygon between the foot and~%# the world. A value of null or an empty list will default to using the entire foot. Contact points~%# are expressed in sole frame. This ordering does not matter. For example: to tell the controller to~%# use the entire foot, the predicted contact points would be: predicted_contact_points: - {x: 0.5 *~%# foot_length, y: -0.5 * toe_width} - {x: 0.5 * foot_length, y: 0.5 * toe_width} - {x: -0.5 *~%# foot_length, y: -0.5 * heel_width} - {x: -0.5 * foot_length, y: 0.5 * heel_width} ~%ihmc_msgs/Point2dRosMessage[] predicted_contact_points~%~%# This contains information on what the swing trajectory should be for each step. Recomended is~%# DEFAULT.~%uint8 trajectory_type~%~%# Contains information on how high the robot should swing its foot. This affects trajectory types~%# DEFAULT and OBSTACLE_CLEARANCE.If a value smaller then the minumal swing height is chosen (e.g. 0.0)~%# the swing height will be changed to a default value.~%float64 swing_height~%~%# In case the trajectory type is set to CUSTOM two swing waypoints can be specified here. The~%# waypoints define sole positions.The controller will compute times and velocities at the waypoints.~%# This is a convinient way to shape the trajectory of the swing. If full control over the~%# swingtrajectory is desired use the trajectory type WAYPOINTS instead. The position waypoints are~%# expected in the trajectory frame.~%geometry_msgs/Point[] position_waypoints~%~%# In case the trajectory type is set to WAYPOINTS, swing waypoints can be specified here. The~%# waypoints do not include thestart point (which is set to the current foot state at lift-off) and the~%# touch down point (which is specified by the location and orientation fields).All waypoints are for~%# the sole frame and expressed in the trajectory frame. The maximum number of points can be found in~%# the Footstep class.~%ihmc_msgs/SE3TrajectoryPointRosMessage[] swing_trajectory~%~%# In case the trajectory type is set to WAYPOINTS, this value can be used to specify the trajectory~%# blend duration  in seconds. If greater than zero, waypoints that fall within the valid time window~%# (beginning at the start of the swing phase and spanning  the desired blend duration) will be~%# adjusted to account for the initial error between the actual and expected position and orientation~%# of the swing foot. Note that the expectedInitialLocation and expectedInitialOrientation fields must~%# be defined in order to enable trajectory blending.~%float64 swing_trajectory_blend_duration~%~%# The swingDuration is the time a foot is not in ground contact during a step. If the value of this~%# field is invalid (not positive) it will be replaced by a default swingDuration.~%float64 swing_duration~%~%# The transferDuration is the time spent with the feet in ground contact before a step. If the value~%# of this field is invalid (not positive) it will be replaced by a default transferDuration.~%float64 transfer_duration~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%# This message utilizes \"enums\". Enum value information for this message follows.~%~%# \"robot_side\" enum values:~%uint8 LEFT=0 # refers to the LEFT side of a robot~%uint8 RIGHT=1 # refers to the RIGHT side of a robot~%~%# \"trajectory_type\" enum values:~%uint8 DEFAULT=0 # The controller will execute a default trajectory.~%uint8 OBSTACLE_CLEARANCE=1 # The controller will attempt to step on/off an obstacle.~%uint8 CUSTOM=2 # In this mode two trajectory position waypoints can be specified.~%uint8 WAYPOINTS=3 # The swing trajectory is fully defined by the given waypoints.~%~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%================================================================================~%MSG: ihmc_msgs/Point2dRosMessage~%## Point2dRosMessage~%# This message represents a point on a 2d plane. The coordinates are referred to as \"x\" and \"y\"~%# The first coordinate of the point in a 2D plane~%float64 x~%~%# The second coordinate of the point in a 2D plane~%float64 y~%~%~%================================================================================~%MSG: ihmc_msgs/SE3TrajectoryPointRosMessage~%## SE3TrajectoryPointRosMessage~%# This class is used to build trajectory messages in taskspace. It holds the necessary information for~%# one trajectory point. Feel free to look at EuclideanTrajectoryPointMessage (translational) and~%# EuclideanTrajectoryPointMessage (rotational)~%~%# Time at which the trajectory point has to be reached. The time is relative to when the trajectory~%# starts.~%float64 time~%~%# Define the desired 3D position to be reached at this trajectory point.~%geometry_msgs/Point position~%~%# Define the desired 3D orientation to be reached at this trajectory point.~%geometry_msgs/Quaternion orientation~%~%# Define the desired 3D linear velocity to be reached at this trajectory point.~%geometry_msgs/Vector3 linear_velocity~%~%# Define the desired 3D angular velocity to be reached at this trajectory point.~%geometry_msgs/Vector3 angular_velocity~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%# It is only meant to represent a direction. Therefore, it does not~%# make sense to apply a translation to it (e.g., when applying a ~%# generic rigid transformation to a Vector3, tf2 will only apply the~%# rotation). If you want your data to be translatable too, use the~%# geometry_msgs/Point message instead.~%~%float64 x~%float64 y~%float64 z~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'FootstepDataRosMessage)))
  "Returns full string definition for message of type 'FootstepDataRosMessage"
  (cl:format cl:nil "## FootstepDataRosMessage~%# This message specifies the position, orientation and side (left or right) of a desired footstep in~%# world frame.~%~%# Specifies which foot will swing to reach the foostep.~%uint8 robot_side~%~%# Specifies the position of the footstep (sole frame) in world frame.~%geometry_msgs/Point location~%~%# Specifies the orientation of the footstep (sole frame) in world frame.~%geometry_msgs/Quaternion orientation~%~%# predictedContactPoints specifies the vertices of the expected contact polygon between the foot and~%# the world. A value of null or an empty list will default to using the entire foot. Contact points~%# are expressed in sole frame. This ordering does not matter. For example: to tell the controller to~%# use the entire foot, the predicted contact points would be: predicted_contact_points: - {x: 0.5 *~%# foot_length, y: -0.5 * toe_width} - {x: 0.5 * foot_length, y: 0.5 * toe_width} - {x: -0.5 *~%# foot_length, y: -0.5 * heel_width} - {x: -0.5 * foot_length, y: 0.5 * heel_width} ~%ihmc_msgs/Point2dRosMessage[] predicted_contact_points~%~%# This contains information on what the swing trajectory should be for each step. Recomended is~%# DEFAULT.~%uint8 trajectory_type~%~%# Contains information on how high the robot should swing its foot. This affects trajectory types~%# DEFAULT and OBSTACLE_CLEARANCE.If a value smaller then the minumal swing height is chosen (e.g. 0.0)~%# the swing height will be changed to a default value.~%float64 swing_height~%~%# In case the trajectory type is set to CUSTOM two swing waypoints can be specified here. The~%# waypoints define sole positions.The controller will compute times and velocities at the waypoints.~%# This is a convinient way to shape the trajectory of the swing. If full control over the~%# swingtrajectory is desired use the trajectory type WAYPOINTS instead. The position waypoints are~%# expected in the trajectory frame.~%geometry_msgs/Point[] position_waypoints~%~%# In case the trajectory type is set to WAYPOINTS, swing waypoints can be specified here. The~%# waypoints do not include thestart point (which is set to the current foot state at lift-off) and the~%# touch down point (which is specified by the location and orientation fields).All waypoints are for~%# the sole frame and expressed in the trajectory frame. The maximum number of points can be found in~%# the Footstep class.~%ihmc_msgs/SE3TrajectoryPointRosMessage[] swing_trajectory~%~%# In case the trajectory type is set to WAYPOINTS, this value can be used to specify the trajectory~%# blend duration  in seconds. If greater than zero, waypoints that fall within the valid time window~%# (beginning at the start of the swing phase and spanning  the desired blend duration) will be~%# adjusted to account for the initial error between the actual and expected position and orientation~%# of the swing foot. Note that the expectedInitialLocation and expectedInitialOrientation fields must~%# be defined in order to enable trajectory blending.~%float64 swing_trajectory_blend_duration~%~%# The swingDuration is the time a foot is not in ground contact during a step. If the value of this~%# field is invalid (not positive) it will be replaced by a default swingDuration.~%float64 swing_duration~%~%# The transferDuration is the time spent with the feet in ground contact before a step. If the value~%# of this field is invalid (not positive) it will be replaced by a default transferDuration.~%float64 transfer_duration~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%# This message utilizes \"enums\". Enum value information for this message follows.~%~%# \"robot_side\" enum values:~%uint8 LEFT=0 # refers to the LEFT side of a robot~%uint8 RIGHT=1 # refers to the RIGHT side of a robot~%~%# \"trajectory_type\" enum values:~%uint8 DEFAULT=0 # The controller will execute a default trajectory.~%uint8 OBSTACLE_CLEARANCE=1 # The controller will attempt to step on/off an obstacle.~%uint8 CUSTOM=2 # In this mode two trajectory position waypoints can be specified.~%uint8 WAYPOINTS=3 # The swing trajectory is fully defined by the given waypoints.~%~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%================================================================================~%MSG: ihmc_msgs/Point2dRosMessage~%## Point2dRosMessage~%# This message represents a point on a 2d plane. The coordinates are referred to as \"x\" and \"y\"~%# The first coordinate of the point in a 2D plane~%float64 x~%~%# The second coordinate of the point in a 2D plane~%float64 y~%~%~%================================================================================~%MSG: ihmc_msgs/SE3TrajectoryPointRosMessage~%## SE3TrajectoryPointRosMessage~%# This class is used to build trajectory messages in taskspace. It holds the necessary information for~%# one trajectory point. Feel free to look at EuclideanTrajectoryPointMessage (translational) and~%# EuclideanTrajectoryPointMessage (rotational)~%~%# Time at which the trajectory point has to be reached. The time is relative to when the trajectory~%# starts.~%float64 time~%~%# Define the desired 3D position to be reached at this trajectory point.~%geometry_msgs/Point position~%~%# Define the desired 3D orientation to be reached at this trajectory point.~%geometry_msgs/Quaternion orientation~%~%# Define the desired 3D linear velocity to be reached at this trajectory point.~%geometry_msgs/Vector3 linear_velocity~%~%# Define the desired 3D angular velocity to be reached at this trajectory point.~%geometry_msgs/Vector3 angular_velocity~%~%# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id~%# in the top level message is used, the unique id in nested messages is ignored. Use~%# /output/last_received_message for feedback about when the last message was received. A message with~%# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.~%int64 unique_id~%~%~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%# It is only meant to represent a direction. Therefore, it does not~%# make sense to apply a translation to it (e.g., when applying a ~%# generic rigid transformation to a Vector3, tf2 will only apply the~%# rotation). If you want your data to be translatable too, use the~%# geometry_msgs/Point message instead.~%~%float64 x~%float64 y~%float64 z~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <FootstepDataRosMessage>))
  (cl:+ 0
     1
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'location))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'orientation))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'predicted_contact_points) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
     1
     8
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'position_waypoints) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'swing_trajectory) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
     8
     8
     8
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <FootstepDataRosMessage>))
  "Converts a ROS message object to a list"
  (cl:list 'FootstepDataRosMessage
    (cl:cons ':robot_side (robot_side msg))
    (cl:cons ':location (location msg))
    (cl:cons ':orientation (orientation msg))
    (cl:cons ':predicted_contact_points (predicted_contact_points msg))
    (cl:cons ':trajectory_type (trajectory_type msg))
    (cl:cons ':swing_height (swing_height msg))
    (cl:cons ':position_waypoints (position_waypoints msg))
    (cl:cons ':swing_trajectory (swing_trajectory msg))
    (cl:cons ':swing_trajectory_blend_duration (swing_trajectory_blend_duration msg))
    (cl:cons ':swing_duration (swing_duration msg))
    (cl:cons ':transfer_duration (transfer_duration msg))
    (cl:cons ':unique_id (unique_id msg))
))
