(cl:in-package kinect_tracker-msg)
(cl:export '(X-VAL
          X
          Y-VAL
          Y
          Z-VAL
          Z
          W-VAL
          W
          JOINT_SIDE-VAL
          JOINT_SIDE
          JOINT_TYPE-VAL
          JOINT_TYPE
))