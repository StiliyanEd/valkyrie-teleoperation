; Auto-generated. Do not edit!


(cl:in-package kinect_tracker-msg)


;//! \htmlinclude JointListRosMessage.msg.html

(cl:defclass <JointListRosMessage> (roslisp-msg-protocol:ros-message)
  ((joint_list
    :reader joint_list
    :initarg :joint_list
    :type (cl:vector kinect_tracker-msg:JointRosMessage)
   :initform (cl:make-array 0 :element-type 'kinect_tracker-msg:JointRosMessage :initial-element (cl:make-instance 'kinect_tracker-msg:JointRosMessage))))
)

(cl:defclass JointListRosMessage (<JointListRosMessage>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <JointListRosMessage>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'JointListRosMessage)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name kinect_tracker-msg:<JointListRosMessage> is deprecated: use kinect_tracker-msg:JointListRosMessage instead.")))

(cl:ensure-generic-function 'joint_list-val :lambda-list '(m))
(cl:defmethod joint_list-val ((m <JointListRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader kinect_tracker-msg:joint_list-val is deprecated.  Use kinect_tracker-msg:joint_list instead.")
  (joint_list m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <JointListRosMessage>) ostream)
  "Serializes a message object of type '<JointListRosMessage>"
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'joint_list))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'joint_list))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <JointListRosMessage>) istream)
  "Deserializes a message object of type '<JointListRosMessage>"
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'joint_list) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'joint_list)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'kinect_tracker-msg:JointRosMessage))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<JointListRosMessage>)))
  "Returns string type for a message object of type '<JointListRosMessage>"
  "kinect_tracker/JointListRosMessage")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'JointListRosMessage)))
  "Returns string type for a message object of type 'JointListRosMessage"
  "kinect_tracker/JointListRosMessage")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<JointListRosMessage>)))
  "Returns md5sum for a message object of type '<JointListRosMessage>"
  "bdff74395fb559c45c81cd24f90d6e4d")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'JointListRosMessage)))
  "Returns md5sum for a message object of type 'JointListRosMessage"
  "bdff74395fb559c45c81cd24f90d6e4d")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<JointListRosMessage>)))
  "Returns full string definition for message of type '<JointListRosMessage>"
  (cl:format cl:nil "kinect_tracker/JointRosMessage[] joint_list~%================================================================================~%MSG: kinect_tracker/JointRosMessage~%#The position of the join as measured by the kinect~%float64 x~%float64 y~%float64 z~%float64 w~%uint8 joint_side~%uint8 joint_type~%~%# \"joint_side\" enum values:~%uint8 LEFT=0  # refers to the LEFT side of the person~%uint8 RIGHT=1 # refers to the RIGHT side of the person~%~%# \"joint_type\" enum values:~%int64 HEAD=0~%int64 SHOULDER=1~%int64 ELBOW=2~%int64 HAND=3~%int64 TORSO=4~%int64 HIP=5~%int64 KNEE=6~%int64 FOOT=7~%int64 NECK=8~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'JointListRosMessage)))
  "Returns full string definition for message of type 'JointListRosMessage"
  (cl:format cl:nil "kinect_tracker/JointRosMessage[] joint_list~%================================================================================~%MSG: kinect_tracker/JointRosMessage~%#The position of the join as measured by the kinect~%float64 x~%float64 y~%float64 z~%float64 w~%uint8 joint_side~%uint8 joint_type~%~%# \"joint_side\" enum values:~%uint8 LEFT=0  # refers to the LEFT side of the person~%uint8 RIGHT=1 # refers to the RIGHT side of the person~%~%# \"joint_type\" enum values:~%int64 HEAD=0~%int64 SHOULDER=1~%int64 ELBOW=2~%int64 HAND=3~%int64 TORSO=4~%int64 HIP=5~%int64 KNEE=6~%int64 FOOT=7~%int64 NECK=8~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <JointListRosMessage>))
  (cl:+ 0
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'joint_list) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <JointListRosMessage>))
  "Converts a ROS message object to a list"
  (cl:list 'JointListRosMessage
    (cl:cons ':joint_list (joint_list msg))
))
