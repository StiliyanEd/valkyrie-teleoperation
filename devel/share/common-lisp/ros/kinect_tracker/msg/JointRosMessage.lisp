; Auto-generated. Do not edit!


(cl:in-package kinect_tracker-msg)


;//! \htmlinclude JointRosMessage.msg.html

(cl:defclass <JointRosMessage> (roslisp-msg-protocol:ros-message)
  ((x
    :reader x
    :initarg :x
    :type cl:float
    :initform 0.0)
   (y
    :reader y
    :initarg :y
    :type cl:float
    :initform 0.0)
   (z
    :reader z
    :initarg :z
    :type cl:float
    :initform 0.0)
   (w
    :reader w
    :initarg :w
    :type cl:float
    :initform 0.0)
   (joint_side
    :reader joint_side
    :initarg :joint_side
    :type cl:fixnum
    :initform 0)
   (joint_type
    :reader joint_type
    :initarg :joint_type
    :type cl:fixnum
    :initform 0))
)

(cl:defclass JointRosMessage (<JointRosMessage>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <JointRosMessage>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'JointRosMessage)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name kinect_tracker-msg:<JointRosMessage> is deprecated: use kinect_tracker-msg:JointRosMessage instead.")))

(cl:ensure-generic-function 'x-val :lambda-list '(m))
(cl:defmethod x-val ((m <JointRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader kinect_tracker-msg:x-val is deprecated.  Use kinect_tracker-msg:x instead.")
  (x m))

(cl:ensure-generic-function 'y-val :lambda-list '(m))
(cl:defmethod y-val ((m <JointRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader kinect_tracker-msg:y-val is deprecated.  Use kinect_tracker-msg:y instead.")
  (y m))

(cl:ensure-generic-function 'z-val :lambda-list '(m))
(cl:defmethod z-val ((m <JointRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader kinect_tracker-msg:z-val is deprecated.  Use kinect_tracker-msg:z instead.")
  (z m))

(cl:ensure-generic-function 'w-val :lambda-list '(m))
(cl:defmethod w-val ((m <JointRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader kinect_tracker-msg:w-val is deprecated.  Use kinect_tracker-msg:w instead.")
  (w m))

(cl:ensure-generic-function 'joint_side-val :lambda-list '(m))
(cl:defmethod joint_side-val ((m <JointRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader kinect_tracker-msg:joint_side-val is deprecated.  Use kinect_tracker-msg:joint_side instead.")
  (joint_side m))

(cl:ensure-generic-function 'joint_type-val :lambda-list '(m))
(cl:defmethod joint_type-val ((m <JointRosMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader kinect_tracker-msg:joint_type-val is deprecated.  Use kinect_tracker-msg:joint_type instead.")
  (joint_type m))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<JointRosMessage>)))
    "Constants for message type '<JointRosMessage>"
  '((:LEFT . 0)
    (:RIGHT . 1)
    (:HEAD . 0)
    (:SHOULDER . 1)
    (:ELBOW . 2)
    (:HAND . 3)
    (:TORSO . 4)
    (:HIP . 5)
    (:KNEE . 6)
    (:FOOT . 7)
    (:NECK . 8))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'JointRosMessage)))
    "Constants for message type 'JointRosMessage"
  '((:LEFT . 0)
    (:RIGHT . 1)
    (:HEAD . 0)
    (:SHOULDER . 1)
    (:ELBOW . 2)
    (:HAND . 3)
    (:TORSO . 4)
    (:HIP . 5)
    (:KNEE . 6)
    (:FOOT . 7)
    (:NECK . 8))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <JointRosMessage>) ostream)
  "Serializes a message object of type '<JointRosMessage>"
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'x))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'y))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'z))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'w))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'joint_side)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'joint_type)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <JointRosMessage>) istream)
  "Deserializes a message object of type '<JointRosMessage>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'x) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'y) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'z) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'w) (roslisp-utils:decode-double-float-bits bits)))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'joint_side)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'joint_type)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<JointRosMessage>)))
  "Returns string type for a message object of type '<JointRosMessage>"
  "kinect_tracker/JointRosMessage")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'JointRosMessage)))
  "Returns string type for a message object of type 'JointRosMessage"
  "kinect_tracker/JointRosMessage")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<JointRosMessage>)))
  "Returns md5sum for a message object of type '<JointRosMessage>"
  "51e9c6e154e50ccdfb8fdc9513317849")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'JointRosMessage)))
  "Returns md5sum for a message object of type 'JointRosMessage"
  "51e9c6e154e50ccdfb8fdc9513317849")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<JointRosMessage>)))
  "Returns full string definition for message of type '<JointRosMessage>"
  (cl:format cl:nil "#The position of the join as measured by the kinect~%float64 x~%float64 y~%float64 z~%float64 w~%uint8 joint_side~%uint8 joint_type~%~%# \"joint_side\" enum values:~%uint8 LEFT=0  # refers to the LEFT side of the person~%uint8 RIGHT=1 # refers to the RIGHT side of the person~%~%# \"joint_type\" enum values:~%int64 HEAD=0~%int64 SHOULDER=1~%int64 ELBOW=2~%int64 HAND=3~%int64 TORSO=4~%int64 HIP=5~%int64 KNEE=6~%int64 FOOT=7~%int64 NECK=8~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'JointRosMessage)))
  "Returns full string definition for message of type 'JointRosMessage"
  (cl:format cl:nil "#The position of the join as measured by the kinect~%float64 x~%float64 y~%float64 z~%float64 w~%uint8 joint_side~%uint8 joint_type~%~%# \"joint_side\" enum values:~%uint8 LEFT=0  # refers to the LEFT side of the person~%uint8 RIGHT=1 # refers to the RIGHT side of the person~%~%# \"joint_type\" enum values:~%int64 HEAD=0~%int64 SHOULDER=1~%int64 ELBOW=2~%int64 HAND=3~%int64 TORSO=4~%int64 HIP=5~%int64 KNEE=6~%int64 FOOT=7~%int64 NECK=8~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <JointRosMessage>))
  (cl:+ 0
     8
     8
     8
     8
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <JointRosMessage>))
  "Converts a ROS message object to a list"
  (cl:list 'JointRosMessage
    (cl:cons ':x (x msg))
    (cl:cons ':y (y msg))
    (cl:cons ':z (z msg))
    (cl:cons ':w (w msg))
    (cl:cons ':joint_side (joint_side msg))
    (cl:cons ':joint_type (joint_type msg))
))
