
(cl:in-package :asdf)

(defsystem "kinect_tracker-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "JointRosMessage" :depends-on ("_package_JointRosMessage"))
    (:file "_package_JointRosMessage" :depends-on ("_package"))
    (:file "JointListRosMessage" :depends-on ("_package_JointListRosMessage"))
    (:file "_package_JointListRosMessage" :depends-on ("_package"))
  ))