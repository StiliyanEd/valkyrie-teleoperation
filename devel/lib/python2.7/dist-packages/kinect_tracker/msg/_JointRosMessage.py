# This Python file uses the following encoding: utf-8
"""autogenerated by genpy from kinect_tracker/JointRosMessage.msg. Do not edit."""
import sys
python3 = True if sys.hexversion > 0x03000000 else False
import genpy
import struct


class JointRosMessage(genpy.Message):
  _md5sum = "51e9c6e154e50ccdfb8fdc9513317849"
  _type = "kinect_tracker/JointRosMessage"
  _has_header = False #flag to mark the presence of a Header object
  _full_text = """#The position of the join as measured by the kinect
float64 x
float64 y
float64 z
float64 w
uint8 joint_side
uint8 joint_type

# "joint_side" enum values:
uint8 LEFT=0  # refers to the LEFT side of the person
uint8 RIGHT=1 # refers to the RIGHT side of the person

# "joint_type" enum values:
int64 HEAD=0
int64 SHOULDER=1
int64 ELBOW=2
int64 HAND=3
int64 TORSO=4
int64 HIP=5
int64 KNEE=6
int64 FOOT=7
int64 NECK=8
"""
  # Pseudo-constants
  LEFT = 0
  RIGHT = 1
  HEAD = 0
  SHOULDER = 1
  ELBOW = 2
  HAND = 3
  TORSO = 4
  HIP = 5
  KNEE = 6
  FOOT = 7
  NECK = 8

  __slots__ = ['x','y','z','w','joint_side','joint_type']
  _slot_types = ['float64','float64','float64','float64','uint8','uint8']

  def __init__(self, *args, **kwds):
    """
    Constructor. Any message fields that are implicitly/explicitly
    set to None will be assigned a default value. The recommend
    use is keyword arguments as this is more robust to future message
    changes.  You cannot mix in-order arguments and keyword arguments.

    The available fields are:
       x,y,z,w,joint_side,joint_type

    :param args: complete set of field values, in .msg order
    :param kwds: use keyword arguments corresponding to message field names
    to set specific fields.
    """
    if args or kwds:
      super(JointRosMessage, self).__init__(*args, **kwds)
      #message fields cannot be None, assign default values for those that are
      if self.x is None:
        self.x = 0.
      if self.y is None:
        self.y = 0.
      if self.z is None:
        self.z = 0.
      if self.w is None:
        self.w = 0.
      if self.joint_side is None:
        self.joint_side = 0
      if self.joint_type is None:
        self.joint_type = 0
    else:
      self.x = 0.
      self.y = 0.
      self.z = 0.
      self.w = 0.
      self.joint_side = 0
      self.joint_type = 0

  def _get_types(self):
    """
    internal API method
    """
    return self._slot_types

  def serialize(self, buff):
    """
    serialize message into buffer
    :param buff: buffer, ``StringIO``
    """
    try:
      _x = self
      buff.write(_struct_4d2B.pack(_x.x, _x.y, _x.z, _x.w, _x.joint_side, _x.joint_type))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(locals().get('_x', self)))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(locals().get('_x', self)))))

  def deserialize(self, str):
    """
    unpack serialized message in str into this message instance
    :param str: byte array of serialized message, ``str``
    """
    try:
      end = 0
      _x = self
      start = end
      end += 34
      (_x.x, _x.y, _x.z, _x.w, _x.joint_side, _x.joint_type,) = _struct_4d2B.unpack(str[start:end])
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill


  def serialize_numpy(self, buff, numpy):
    """
    serialize message with numpy array types into buffer
    :param buff: buffer, ``StringIO``
    :param numpy: numpy python module
    """
    try:
      _x = self
      buff.write(_struct_4d2B.pack(_x.x, _x.y, _x.z, _x.w, _x.joint_side, _x.joint_type))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(locals().get('_x', self)))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(locals().get('_x', self)))))

  def deserialize_numpy(self, str, numpy):
    """
    unpack serialized message in str into this message instance using numpy for array types
    :param str: byte array of serialized message, ``str``
    :param numpy: numpy python module
    """
    try:
      end = 0
      _x = self
      start = end
      end += 34
      (_x.x, _x.y, _x.z, _x.w, _x.joint_side, _x.joint_type,) = _struct_4d2B.unpack(str[start:end])
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill

_struct_I = genpy.struct_I
_struct_4d2B = struct.Struct("<4d2B")
