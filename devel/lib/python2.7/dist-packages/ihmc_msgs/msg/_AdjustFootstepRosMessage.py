# This Python file uses the following encoding: utf-8
"""autogenerated by genpy from ihmc_msgs/AdjustFootstepRosMessage.msg. Do not edit."""
import sys
python3 = True if sys.hexversion > 0x03000000 else False
import genpy
import struct

import geometry_msgs.msg
import ihmc_msgs.msg

class AdjustFootstepRosMessage(genpy.Message):
  _md5sum = "849a098cda5c5330c4c2e2dd6b32be3c"
  _type = "ihmc_msgs/AdjustFootstepRosMessage"
  _has_header = False #flag to mark the presence of a Header object
  _full_text = """## AdjustFootstepRosMessage
# The intent of this message is to adjust a footstep when the robot is executing it (a foot is
# currently swinging to reach the footstep to be adjusted).

# Specifies which foot is expected to be executing the footstep to be adjusted.
uint8 robot_side

# Specifies the adjusted position of the footstep. It is expressed in world frame.
geometry_msgs/Point location

# Specifies the adjusted orientation of the footstep. It is expressed in world frame.
geometry_msgs/Quaternion orientation

# predictedContactPoints specifies the vertices of the expected contact polygon between the foot and
# the world. A value of null or an empty list will default to keep the contact points used for the
# original footstep. Contact points  are expressed in sole frame. This ordering does not matter. For
# example: to tell the controller to use the entire foot, the predicted contact points would be:
# predicted_contact_points: - {x: 0.5 * foot_length, y: -0.5 * toe_width} - {x: 0.5 * foot_length, y:
# 0.5 * toe_width} - {x: -0.5 * foot_length, y: -0.5 * heel_width} - {x: -0.5 * foot_length, y: 0.5 *
# heel_width} 
ihmc_msgs/Point2dRosMessage[] predicted_contact_points

# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id
# in the top level message is used, the unique id in nested messages is ignored. Use
# /output/last_received_message for feedback about when the last message was received. A message with
# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.
int64 unique_id


# This message utilizes "enums". Enum value information for this message follows.

# "robot_side" enum values:
uint8 LEFT=0 # refers to the LEFT side of a robot
uint8 RIGHT=1 # refers to the RIGHT side of a robot


================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

================================================================================
MSG: geometry_msgs/Quaternion
# This represents an orientation in free space in quaternion form.

float64 x
float64 y
float64 z
float64 w

================================================================================
MSG: ihmc_msgs/Point2dRosMessage
## Point2dRosMessage
# This message represents a point on a 2d plane. The coordinates are referred to as "x" and "y"
# The first coordinate of the point in a 2D plane
float64 x

# The second coordinate of the point in a 2D plane
float64 y

"""
  # Pseudo-constants
  LEFT = 0
  RIGHT = 1

  __slots__ = ['robot_side','location','orientation','predicted_contact_points','unique_id']
  _slot_types = ['uint8','geometry_msgs/Point','geometry_msgs/Quaternion','ihmc_msgs/Point2dRosMessage[]','int64']

  def __init__(self, *args, **kwds):
    """
    Constructor. Any message fields that are implicitly/explicitly
    set to None will be assigned a default value. The recommend
    use is keyword arguments as this is more robust to future message
    changes.  You cannot mix in-order arguments and keyword arguments.

    The available fields are:
       robot_side,location,orientation,predicted_contact_points,unique_id

    :param args: complete set of field values, in .msg order
    :param kwds: use keyword arguments corresponding to message field names
    to set specific fields.
    """
    if args or kwds:
      super(AdjustFootstepRosMessage, self).__init__(*args, **kwds)
      #message fields cannot be None, assign default values for those that are
      if self.robot_side is None:
        self.robot_side = 0
      if self.location is None:
        self.location = geometry_msgs.msg.Point()
      if self.orientation is None:
        self.orientation = geometry_msgs.msg.Quaternion()
      if self.predicted_contact_points is None:
        self.predicted_contact_points = []
      if self.unique_id is None:
        self.unique_id = 0
    else:
      self.robot_side = 0
      self.location = geometry_msgs.msg.Point()
      self.orientation = geometry_msgs.msg.Quaternion()
      self.predicted_contact_points = []
      self.unique_id = 0

  def _get_types(self):
    """
    internal API method
    """
    return self._slot_types

  def serialize(self, buff):
    """
    serialize message into buffer
    :param buff: buffer, ``StringIO``
    """
    try:
      _x = self
      buff.write(_struct_B7d.pack(_x.robot_side, _x.location.x, _x.location.y, _x.location.z, _x.orientation.x, _x.orientation.y, _x.orientation.z, _x.orientation.w))
      length = len(self.predicted_contact_points)
      buff.write(_struct_I.pack(length))
      for val1 in self.predicted_contact_points:
        _x = val1
        buff.write(_struct_2d.pack(_x.x, _x.y))
      buff.write(_struct_q.pack(self.unique_id))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(locals().get('_x', self)))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(locals().get('_x', self)))))

  def deserialize(self, str):
    """
    unpack serialized message in str into this message instance
    :param str: byte array of serialized message, ``str``
    """
    try:
      if self.location is None:
        self.location = geometry_msgs.msg.Point()
      if self.orientation is None:
        self.orientation = geometry_msgs.msg.Quaternion()
      if self.predicted_contact_points is None:
        self.predicted_contact_points = None
      end = 0
      _x = self
      start = end
      end += 57
      (_x.robot_side, _x.location.x, _x.location.y, _x.location.z, _x.orientation.x, _x.orientation.y, _x.orientation.z, _x.orientation.w,) = _struct_B7d.unpack(str[start:end])
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      self.predicted_contact_points = []
      for i in range(0, length):
        val1 = ihmc_msgs.msg.Point2dRosMessage()
        _x = val1
        start = end
        end += 16
        (_x.x, _x.y,) = _struct_2d.unpack(str[start:end])
        self.predicted_contact_points.append(val1)
      start = end
      end += 8
      (self.unique_id,) = _struct_q.unpack(str[start:end])
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill


  def serialize_numpy(self, buff, numpy):
    """
    serialize message with numpy array types into buffer
    :param buff: buffer, ``StringIO``
    :param numpy: numpy python module
    """
    try:
      _x = self
      buff.write(_struct_B7d.pack(_x.robot_side, _x.location.x, _x.location.y, _x.location.z, _x.orientation.x, _x.orientation.y, _x.orientation.z, _x.orientation.w))
      length = len(self.predicted_contact_points)
      buff.write(_struct_I.pack(length))
      for val1 in self.predicted_contact_points:
        _x = val1
        buff.write(_struct_2d.pack(_x.x, _x.y))
      buff.write(_struct_q.pack(self.unique_id))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(locals().get('_x', self)))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(locals().get('_x', self)))))

  def deserialize_numpy(self, str, numpy):
    """
    unpack serialized message in str into this message instance using numpy for array types
    :param str: byte array of serialized message, ``str``
    :param numpy: numpy python module
    """
    try:
      if self.location is None:
        self.location = geometry_msgs.msg.Point()
      if self.orientation is None:
        self.orientation = geometry_msgs.msg.Quaternion()
      if self.predicted_contact_points is None:
        self.predicted_contact_points = None
      end = 0
      _x = self
      start = end
      end += 57
      (_x.robot_side, _x.location.x, _x.location.y, _x.location.z, _x.orientation.x, _x.orientation.y, _x.orientation.z, _x.orientation.w,) = _struct_B7d.unpack(str[start:end])
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      self.predicted_contact_points = []
      for i in range(0, length):
        val1 = ihmc_msgs.msg.Point2dRosMessage()
        _x = val1
        start = end
        end += 16
        (_x.x, _x.y,) = _struct_2d.unpack(str[start:end])
        self.predicted_contact_points.append(val1)
      start = end
      end += 8
      (self.unique_id,) = _struct_q.unpack(str[start:end])
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill

_struct_I = genpy.struct_I
_struct_2d = struct.Struct("<2d")
_struct_q = struct.Struct("<q")
_struct_B7d = struct.Struct("<B7d")
