# This Python file uses the following encoding: utf-8
"""autogenerated by genpy from ihmc_msgs/LastReceivedMessage.msg. Do not edit."""
import sys
python3 = True if sys.hexversion > 0x03000000 else False
import genpy
import struct


class LastReceivedMessage(genpy.Message):
  _md5sum = "1bfe87dec4247b73097478bd3aa9364d"
  _type = "ihmc_msgs/LastReceivedMessage"
  _has_header = False #flag to mark the presence of a Header object
  _full_text = """## LastReceivedMessage
# Last Received Message echo's back the ID and type of the last message received by the IHMC ROS API.
# The type of the last message received
string type

# The Unique ID of the last message received.
int64 unique_id

# The timestamp at which the message was received.
int64 receive_timestamp

# The time since a message was received
float64 time_since_last_received

"""
  __slots__ = ['type','unique_id','receive_timestamp','time_since_last_received']
  _slot_types = ['string','int64','int64','float64']

  def __init__(self, *args, **kwds):
    """
    Constructor. Any message fields that are implicitly/explicitly
    set to None will be assigned a default value. The recommend
    use is keyword arguments as this is more robust to future message
    changes.  You cannot mix in-order arguments and keyword arguments.

    The available fields are:
       type,unique_id,receive_timestamp,time_since_last_received

    :param args: complete set of field values, in .msg order
    :param kwds: use keyword arguments corresponding to message field names
    to set specific fields.
    """
    if args or kwds:
      super(LastReceivedMessage, self).__init__(*args, **kwds)
      #message fields cannot be None, assign default values for those that are
      if self.type is None:
        self.type = ''
      if self.unique_id is None:
        self.unique_id = 0
      if self.receive_timestamp is None:
        self.receive_timestamp = 0
      if self.time_since_last_received is None:
        self.time_since_last_received = 0.
    else:
      self.type = ''
      self.unique_id = 0
      self.receive_timestamp = 0
      self.time_since_last_received = 0.

  def _get_types(self):
    """
    internal API method
    """
    return self._slot_types

  def serialize(self, buff):
    """
    serialize message into buffer
    :param buff: buffer, ``StringIO``
    """
    try:
      _x = self.type
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      if python3:
        buff.write(struct.pack('<I%sB'%length, length, *_x))
      else:
        buff.write(struct.pack('<I%ss'%length, length, _x))
      _x = self
      buff.write(_struct_2qd.pack(_x.unique_id, _x.receive_timestamp, _x.time_since_last_received))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(locals().get('_x', self)))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(locals().get('_x', self)))))

  def deserialize(self, str):
    """
    unpack serialized message in str into this message instance
    :param str: byte array of serialized message, ``str``
    """
    try:
      end = 0
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.type = str[start:end].decode('utf-8')
      else:
        self.type = str[start:end]
      _x = self
      start = end
      end += 24
      (_x.unique_id, _x.receive_timestamp, _x.time_since_last_received,) = _struct_2qd.unpack(str[start:end])
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill


  def serialize_numpy(self, buff, numpy):
    """
    serialize message with numpy array types into buffer
    :param buff: buffer, ``StringIO``
    :param numpy: numpy python module
    """
    try:
      _x = self.type
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      if python3:
        buff.write(struct.pack('<I%sB'%length, length, *_x))
      else:
        buff.write(struct.pack('<I%ss'%length, length, _x))
      _x = self
      buff.write(_struct_2qd.pack(_x.unique_id, _x.receive_timestamp, _x.time_since_last_received))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(locals().get('_x', self)))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(locals().get('_x', self)))))

  def deserialize_numpy(self, str, numpy):
    """
    unpack serialized message in str into this message instance using numpy for array types
    :param str: byte array of serialized message, ``str``
    :param numpy: numpy python module
    """
    try:
      end = 0
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.type = str[start:end].decode('utf-8')
      else:
        self.type = str[start:end]
      _x = self
      start = end
      end += 24
      (_x.unique_id, _x.receive_timestamp, _x.time_since_last_received,) = _struct_2qd.unpack(str[start:end])
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill

_struct_I = genpy.struct_I
_struct_2qd = struct.Struct("<2qd")
