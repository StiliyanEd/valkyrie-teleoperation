#include "valkyrie_imitation/helper.h"
#include<cmath>

point::point(){
    w = x = y = z = 0.0;
}
point::point(float _w, float _x, float _y, float _z){
    x = _x;
    y = _y;
    z = _z;
    w = _w;
}

point point::operator-(const point& b){
    return point(w - b.w, x - b.x, y - b.y, z - b.z);
}

double constrainAngle(double x){
    x = fmod(x + M_PI, M_PI * 2);
    if (x < 0)
        x += M_PI * 2;
    return x - M_PI;
}

void toEulerAngle(const point& q, double& roll, double& pitch, double& yaw)
{
	// roll (x-axis rotation)
	double sinr = +2.0 * (q.w * q.x + q.y * q.z);
	double cosr = +1.0 - 2.0 * (q.x * q.x + q.y * q.y);
	roll = atan2(sinr, cosr);

	// pitch (y-axis rotation)
	double sinp = +2.0 * (q.w * q.y - q.z * q.x);
	if (fabs(sinp) >= 1)
		pitch = copysign(M_PI / 2, sinp); // use 90 degrees if out of range
	else
		pitch = asin(sinp);

	// yaw (z-axis rotation)
	double siny = +2.0 * (q.w * q.z + q.x * q.y);
	double cosy = +1.0 - 2.0 * (q.y * q.y + q.z * q.z);  
	yaw = atan2(siny, cosy);
}