#include "ros/ros.h"
#include "std_msgs/String.h"
#include "kinect_tracker/JointRosMessage.h"
#include "kinect_tracker/JointListRosMessage.h"
#include "valkyrie_imitation/helper.h"
#include "ihmc_msgs/TrajectoryPoint1DRosMessage.h"
#include "ihmc_msgs/OneDoFJointTrajectoryRosMessage.h"
#include "ihmc_msgs/ArmTrajectoryRosMessage.h"
#include "ihmc_msgs/AbortWalkingRosMessage.h"
#include <string>
#include <vector>
#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds
#include <cmath>

using namespace kinect_tracker;
using namespace ihmc_msgs;

void justSleep(int sec){
    std::this_thread::sleep_for (std::chrono::seconds(sec));
}

point joints[2][10];
double const eps = 1e-1;
//Single joints, like head, are LEFT
/*
# "joint_side" enum values:
uint8 LEFT=0  # refers to the LEFT side of the person
uint8 RIGHT=1 # refers to the RIGHT side of the person

# "joint_type" enum values:
int64 HEAD=0
int64 SHOULDER=1
int64 ELBOW=2
int64 HAND=3
int64 TORSO=4
int64 HIP=5
int64 KNEE=6
int64 FOOT=7
int64 NECK=8
*/

void appendTrajectoryPoint(ArmTrajectoryRosMessage &msg, double time, std::vector<double> &positions){
    while (msg.joint_trajectory_messages.size() < 7){
        msg.joint_trajectory_messages.push_back(OneDoFJointTrajectoryRosMessage());
    }
    int sz = positions.size();
    OneDoFJointTrajectoryRosMessage odf;
    for(int i = 0; i < sz; i++){
        TrajectoryPoint1DRosMessage pt;
        pt.time = time;
        pt.position = positions[i];
        pt.velocity = 0;
        msg.joint_trajectory_messages[i].trajectory_points.push_back(pt);
    }
}

void imitatorCallback(const JointListRosMessage::ConstPtr& msg)
{
    for(JointRosMessage joint: msg->joint_list){
        joints[joint.joint_side][joint.joint_type] = point(joint.w, joint.x, joint.y, joint.z);
        
    }
    // ROS_INFO("I heard: [%f] %f", joints[0][].x, joint.x);
}

//Change radians to joint values
double convertRange(double end, double start, double val){
    double oldMax = 3.141592, oldMin = 0.8;

    double newRange = end - start, oldRange = oldMax - oldMin;
    return ((val - oldMin) * newRange) / oldRange + start;
}

double getJointAngle(point start, point origin, point end){
    point v1 = start - origin;
    point v2 = end - origin;
    double dot = v1.x * v2.x + v1.y * v2.y;
    double det = v1.x * v2.y - v1.y * v2.x;
    return atan2(det, dot);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "imitator");
    ros::NodeHandle handle;
    ros::Subscriber sub = handle.subscribe("tracker", 1000, imitatorCallback);
    std::string robotName;
    ros::param::get("/ihmc_ros/robot_name", robotName);
    ROS_INFO_STREAM(robotName);
    //Abort walking, forcing the robot to switch to double support
    ros::Publisher stopWalking = handle.advertise<AbortWalkingRosMessage>("/ihmc_ros/" + robotName + "/control/abort_walking", 1);
    AbortWalkingRosMessage ab;
    ab.unique_id = -1;
    // justSleep(10);
    stopWalking.publish(ab);

    ros::Publisher armTrajPub = handle.advertise<ArmTrajectoryRosMessage>("/ihmc_ros/" + robotName +"/control/arm_trajectory", 1);
    ros::Rate loop_rate(5);
    double prev = 0.0;
    while(ros::ok()){
        ros::spinOnce();
        // double rShoulderRoll = getJointAngle(joints[0][1], joints[1][1], joints[1][2]);
        // double rElbowPitch   = getJointAngle(joints[1][1], joints[1][2], joints[1][3]);
        // rShoulderRoll = -constrainAngle(rShoulderRoll - M_PI);
        // rElbowPitch   = constrainAngle(rElbowPitch + M_PI);
        double roll, pitch, yaw;
        ROS_INFO("roll: %lf, pitch: %lf, yaw: %lf\n", roll, pitch, yaw);
            
        if(fabs(rShoulderRoll-prev) > eps){
            ROS_INFO("Elbow pitch: %lf", rElbowPitch);
            ArmTrajectoryRosMessage msg;
            msg.robot_side = ArmTrajectoryRosMessage::RIGHT;
            std::vector<double> test = {0.0, rShoulderRoll, 0.0, rElbowPitch, 0.0, 0.0, 0.0};
            appendTrajectoryPoint(msg, 2.0, test);
            msg.unique_id = -1;
            armTrajPub.publish(msg);
            loop_rate.sleep();
            prev = rShoulderRoll;
        }
    }
    return 0;
}