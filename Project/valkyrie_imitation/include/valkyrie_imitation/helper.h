#ifndef _VALKYRIE_IMITATION_HELPER_H_
#define _VALKYRIE_IMITATION_HELPER_H_
struct point{
    float x, y, z, w;
    point();
    point(float _w, float _x, float _y, float _z);
    point operator-(const point& b);
};

double constrainAngle(double x);
void toEulerAngle(const point& q, double& roll, double& pitch, double& yaw);
#endif