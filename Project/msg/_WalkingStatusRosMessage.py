# This Python file uses the following encoding: utf-8
"""autogenerated by genpy from ihmc_msgs/WalkingStatusRosMessage.msg. Do not edit."""
import sys
python3 = True if sys.hexversion > 0x03000000 else False
import genpy
import struct


class WalkingStatusRosMessage(genpy.Message):
  _md5sum = "b0a73288471d327ae20e972dca1096b9"
  _type = "ihmc_msgs/WalkingStatusRosMessage"
  _has_header = False #flag to mark the presence of a Header object
  _full_text = """## WalkingStatusRosMessage
# This class is used to report the status of walking.

# Status of walking. Either STARTED, COMPLETED, or ABORT_REQUESTED.
uint8 status

# A unique id for the current message. This can be a timestamp or sequence number. Only the unique id
# in the top level message is used, the unique id in nested messages is ignored. Use
# /output/last_received_message for feedback about when the last message was received. A message with
# a unique id equals to 0 will be interpreted as invalid and will not be processed by the controller.
int64 unique_id


# This message utilizes "enums". Enum value information for this message follows.

# "status" enum values:
uint8 STARTED=0 # The robot has begun its initial transfer/sway at the start of a walking plan
uint8 COMPLETED=1 # The robot has finished its final transfer/sway at the end of a walking plan
uint8 ABORT_REQUESTED=2 # A walking abort has been requested

"""
  # Pseudo-constants
  STARTED = 0
  COMPLETED = 1
  ABORT_REQUESTED = 2

  __slots__ = ['status','unique_id']
  _slot_types = ['uint8','int64']

  def __init__(self, *args, **kwds):
    """
    Constructor. Any message fields that are implicitly/explicitly
    set to None will be assigned a default value. The recommend
    use is keyword arguments as this is more robust to future message
    changes.  You cannot mix in-order arguments and keyword arguments.

    The available fields are:
       status,unique_id

    :param args: complete set of field values, in .msg order
    :param kwds: use keyword arguments corresponding to message field names
    to set specific fields.
    """
    if args or kwds:
      super(WalkingStatusRosMessage, self).__init__(*args, **kwds)
      #message fields cannot be None, assign default values for those that are
      if self.status is None:
        self.status = 0
      if self.unique_id is None:
        self.unique_id = 0
    else:
      self.status = 0
      self.unique_id = 0

  def _get_types(self):
    """
    internal API method
    """
    return self._slot_types

  def serialize(self, buff):
    """
    serialize message into buffer
    :param buff: buffer, ``StringIO``
    """
    try:
      _x = self
      buff.write(_struct_Bq.pack(_x.status, _x.unique_id))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(locals().get('_x', self)))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(locals().get('_x', self)))))

  def deserialize(self, str):
    """
    unpack serialized message in str into this message instance
    :param str: byte array of serialized message, ``str``
    """
    try:
      end = 0
      _x = self
      start = end
      end += 9
      (_x.status, _x.unique_id,) = _struct_Bq.unpack(str[start:end])
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill


  def serialize_numpy(self, buff, numpy):
    """
    serialize message with numpy array types into buffer
    :param buff: buffer, ``StringIO``
    :param numpy: numpy python module
    """
    try:
      _x = self
      buff.write(_struct_Bq.pack(_x.status, _x.unique_id))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(locals().get('_x', self)))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(locals().get('_x', self)))))

  def deserialize_numpy(self, str, numpy):
    """
    unpack serialized message in str into this message instance using numpy for array types
    :param str: byte array of serialized message, ``str``
    :param numpy: numpy python module
    """
    try:
      end = 0
      _x = self
      start = end
      end += 9
      (_x.status, _x.unique_id,) = _struct_Bq.unpack(str[start:end])
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill

_struct_I = genpy.struct_I
_struct_Bq = struct.Struct("<Bq")
