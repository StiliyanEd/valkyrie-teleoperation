#!/usr/bin/env python

import copy
import time
import rospy

from numpy import append
from ihmc_msgs.msg import ChestTrajectoryRosMessage, ArmTrajectoryRosMessage, FootTrajectoryRosMessage
from ihmc_msgs.msg import SO3TrajectoryPointRosMessage, WholeBodyTrajectoryRosMessage, HandTrajectoryRosMessage
from ihmc_msgs.msg import HighLevelStateRosMessage, HighLevelStateChangeStatusRosMessage
from geometry_msgs.msg import Quaternion, Vector3
from tf.transformations import quaternion_from_euler

ZERO_VECTOR = [0.0, -1.0, 2.0, 1.0, 0.0, 0.0, 0.0]
ELBOW_BENT_UP = [-1.2, 0.6, 0.0, 2.0, 0.0, 0.0, 0.3]

ROBOT_NAME = None
def getChest(torsoOrient):
    # print torsoOrient
    msg = ChestTrajectoryRosMessage()
    msg.unique_id = -1
    timeCount = 1
    for pos in torsoOrient:
        tmp = quaternion_from_euler(pos[0], pos[1], pos[2])
        q = Quaternion()
        q.x = tmp[0]
        q.y = tmp[1]
        q.z = tmp[2]
        q.w = tmp[3]
        traj = SO3TrajectoryPointRosMessage()
        traj.orientation = q;
        traj.time = timeCount;
        timeCount += 1
        traj.unique_id = -1
        msg.taskspace_trajectory_points.append(traj)
    return msg

def getHandTrajectory(side):
    msg = HandTrajectoryRosMessage()
    msg.unique_id = 0
    msg.robot_side = side
    return msg

def getFootTrajectory(side):
    msg = FootTrajectoryRosMessage()
    msg.unique_id = 0
    msg.robot_side = side
    return msg

def getArm(side):
    msg = ArmTrajectoryRosMessage()
    msg.unique_id = 0
    msg.robot_side = side
    return msg


def receivedState(msg):
    print msg

def sendBodyTrajector(par):
    msg = WholeBodyTrajectoryRosMessage()
    msg.unique_id = -1
    rightArmMsg =  getArm(ArmTrajectoryRosMessage.RIGHT)
    leftArmMsg  =  getArm(ArmTrajectoryRosMessage.LEFT)
    chestMsg    =  getChest(par)
    # print rightArmMsg.robot_side, leftArmMsg.robot_side
    msg.left_arm_trajectory_message = leftArmMsg
    msg.right_arm_trajectory_message = rightArmMsg
    msg.chest_trajectory_message = chestMsg
    msg.left_foot_trajectory_message = getFootTrajectory(ArmTrajectoryRosMessage.LEFT)
    msg.right_foot_trajectory_message = getFootTrajectory(ArmTrajectoryRosMessage.RIGHT)
    msg.left_hand_trajectory_message = getHandTrajectory(ArmTrajectoryRosMessage.LEFT)
    msg.right_hand_trajectory_message = getHandTrajectory(ArmTrajectoryRosMessage.RIGHT)
    bodyTrajectoryPublisher.publish(msg)

if __name__ == '__main__':
    try:
        rospy.init_node('ihmc_arm_demo1')

        ROBOT_NAME = rospy.get_param('/ihmc_ros/robot_name')
        print ROBOT_NAME
        chestTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/chest_trajectory".format(ROBOT_NAME), ChestTrajectoryRosMessage, queue_size=1)
        bodyTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/whole_body_trajectory".format(ROBOT_NAME), WholeBodyTrajectoryRosMessage, queue_size=1)
        # stateSub = rospy.Subscriber("/ihmc_ros/{0}/output/high_level_state_change".format(ROBOT_NAME), HighLevelStateChangeStatusRosMessage, receivedState)
        # statePub = rospy.Publisher("/ihmc_ros/{0}/control/high_level_state".format(ROBOT_NAME), HighLevelStateRosMessage, queue_size=1)
        rate = rospy.Rate(10) # 10hz
        time.sleep(1)
        if not rospy.is_shutdown():
            # changeState()
            #1 - yaw, 2 - pitch, 3 -rol
            sendBodyTrajector([(0.2, 0.0, 0.0), (0.2, 0.50, 0.0), (-0.2, 0.50, 0.0), (-0.2, 0.0, 0.0), (0.0, 0.0, 0.0)])
            time.sleep(2)

    except rospy.ROSInterruptException:
        pass
