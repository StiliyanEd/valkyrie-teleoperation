#!/usr/bin/env python

import copy
import time
import rospy
import math
import numpy as np
from pykdl_utils.joint_kinematics import JointKinematics 
from pykdl_utils.kdl_parser import kdl_tree_from_urdf_model
from urdf_parser_py.urdf import URDF, Robot

if __name__ == '__main__':
    try:
        # rospy.init_node('test_node')
        robot = URDF.from_parameter_server()
        # with open('val.urdf', 'r') as myfile:
            # robot = Robot.from_xml_string(myfile.read())
        import random
        base_link = robot.get_root()
        # end_link = robot.link_map.keys()[random.randint(0, len(robot.link_map)-1)]
        end_link = "leftPalm"
        print "Root link: %s; Random end link: %s" % (base_link, end_link)
        # time.sleep(1)
        js_kin = JointKinematics(robot, base_link, end_link)
        names = js_kin.get_joint_names() 
        angles = js_kin.get_joint_angles()
        for i, name in enumerate(names):
            print name, angles[i]
        # kdl_pose = js_kin.forward()
        # print "FK:", kdl_pose
        # tROBOT_NAME = rospy.get_param('/ihmc_ros/robot_name')
        ROBOT_NAME = 'valkyrie'

    except rospy.ROSInterruptException:
        pass
