#!/usr/bin/env python

import copy
import time
import rospy
import math
import numpy as np

from numpy import append

from sensor_msgs.msg import JointState 

interest = ['leftHipYaw', 'leftHipRoll', 'leftHipPitch']
interest2 = ['leftAnkleYaw', 'leftAnkleRoll', 'leftAnklePitch']
interest3 = ['leftKneeYaw', 'leftKneeRoll', 'leftKneePitch']
first = True
def callback(data):
    global first

    for i, joint in enumerate(data.name):
        if joint in interest3 + interest2 + interest:
            if first:
                with open("data/joints.txt", "a") as mf:
                    mf.write("%s," % (joint))
                continue
            with open("data/joints.txt", "a") as mf:
                mf.write("%.3f," % (data.position[i]))
    first = False
    with open("data/joints.txt", "a") as mf:
        mf.write('\n')
    rate.sleep()

if __name__ == '__main__':
    try:
        rospy.init_node('copycat_node')
        # ROBOT_NAME = rospy.get_param('/ihmc_ros/robot_name')
        ROBOT_NAME = 'valkyrie'
 
        rate = rospy.Rate(10) # 10hz
        # robot = URDF.from_parameter_server()
        # kin1 = JointKinematics(robot, base_link, 'rightPalm')
        # make sure the simulation is running otherwise wait
        # if armTrajectoryPublisher.get_num_connections() == 0:
        #     rospy.loginfo('waiting for subscriber...')
        #     while armTrajectoryPublisher.get_num_connections() == 0:
        #         rate.sleep()
        rospy.Subscriber("/ihmc_ros/valkyrie/output/joint_states", JointState, callback, queue_size=1)
        rospy.spin()

    except rospy.ROSInterruptException:
        pass
