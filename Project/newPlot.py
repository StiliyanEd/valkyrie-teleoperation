import sys
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import math

inputFile = sys.argv[1] #source file path
dict = {}
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
fig.canvas.set_window_title(inputFile)
maxVal = minVal = 0
count = 1
joint_type = 0
joint_side = -1
with open(inputFile, 'r') as f:
    x = y = z = 0

    for line in f:
        line = line.strip()
        if line.startswith('LEFT'):
            joint_side = 0
            joint_type = 0
            count += 1
            continue
        if line.startswith('RIGHT'):
            joint_side = 1
            joint_type = 0
            continue
        if len(line) == 0:
            joint_side = -1
        if joint_side == -1:
            continue
        if len(sys.argv) > 2:
            if count < int(sys.argv[2]):
                continue
            if count > int(sys.argv[2]):
                break
        x, y, z = line.split()
        x = float(x)
        y = float(y)
        z = float(z)
        dict[(joint_side, joint_type)] = (x, z, y) 
        joint_type += 1
    
        if x > maxVal:
            maxVal = x;
        if y > maxVal:
            maxVal = y;
        if z > maxVal:
            maxVal = z;

        if x < minVal:
            minVal = x
        if y < minVal:
            minVal = y
        if z < minVal:
            minVal = z

        ax.scatter([x], [z], [y])        

lines = []
lines.append([dict[(0, 0)], dict[(0, 8)]]) #head2neck
lines.append([dict[(0, 8)], dict[(0, 1)]]) #neck2Lsh
lines.append([dict[(0, 8)], dict[(1, 1)]]) #neck2Rsh
lines.append([dict[(0, 1)], dict[(0, 2)]]) #Lsh2Lel
lines.append([dict[(1, 1)], dict[(1, 2)]]) #Rsh2Rel
lines.append([dict[(0, 2)], dict[(0, 3)]]) #Lel2Lh
lines.append([dict[(1, 2)], dict[(1, 3)]]) #Rel2Rh
lines.append([dict[(0, 1)], dict[(0, 4)]]) #2torso
lines.append([dict[(1, 1)], dict[(0, 4)]]) #2torso
lines.append([dict[(0, 4)], dict[(0, 5)]]) #2hip
lines.append([dict[(0, 4)], dict[(1, 5)]]) #2hip
lines.append([dict[(0, 5)], dict[(0, 6)]]) #2knee
lines.append([dict[(1, 5)], dict[(1, 6)]]) #2knee
lines.append([dict[(0, 6)], dict[(0, 7)]]) #2foot
lines.append([dict[(1, 6)], dict[(1, 7)]]) #2foot

for line in lines:
    xs = [line[0][0], line[1][0]]
    ys = [line[0][1], line[1][1]]
    zs = [line[0][2], line[1][2]]
    ax.plot(xs, ys, zs)
ax.scatter([0], [0], [0]) #origin
ax.plot([0, 50], [0, 0], [0, 0])
ax.plot([0, 0], [0, -50], [0, 0])
ax.plot([0, 0], [0, 0], [0, 50])


ax.set_xlabel('X Label')
ax.set_ylabel('Y Label')
ax.set_zlabel('Z Label')

ax.set_xlim(minVal, maxVal)
ax.set_ylim(minVal, maxVal)
ax.set_zlim(minVal, maxVal)

# ax.set_xlim(-maxX, maxX)
# ax.set_ylim(-maxY, maxY)
# ax.set_zlim(-maxZ, maxZ)

print math.atan2(dict[(0, 5)][1], x) - math.pi / 2
plt.show()