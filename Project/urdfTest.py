from pykdl_utils.kdl_kinematics import KDLKinematics
from pykdl_utils.kdl_parser import kdl_tree_from_urdf_model
from see import see
from urdf_parser_py.urdf import URDF, Robot
import numpy as np

with open('val.urdf', 'r') as myfile:
    # data=myfile.read().replace('\n', '')
    robot = Robot.from_xml_string(myfile.read())
# pykdl_utils setup
# robot_urdf = URDF.from_xml_string(data)
tree = kdl_tree_from_urdf_model(robot)
# base_link = robot.get_root()
base_link = 'pelvis'
end_link = "torso"
chain = tree.getChain(base_link, end_link)

num_non_fixed_joints = 0
for j in robot.joint_map:
    if robot.joint_map[j].joint_type != 'fixed':
        num_non_fixed_joints += 1
print "URDF non-fixed joints: %d;" % num_non_fixed_joints,
print "KDL joints: %d" % tree.getNrOfJoints()
print "URDF joints: %d; KDL segments: %d" %(len(robot.joint_map),
                                            tree.getNrOfSegments())

chain = tree.getChain(base_link, end_link)
print "Root link: %s; Random end link: %s" % (base_link, end_link)
for i in range(chain.getNrOfSegments()):
    print chain.getSegment(i).getName()


# print see(chain)
kdl_kin = KDLKinematics(robot, base_link, end_link)
# print kdl_kin.
q = kdl_kin.random_joint_angles()
print q
pose = kdl_kin.forward(q) # forward kinematics (returns homogeneous 4x4 numpy.mat)
print pose
# kdl_kin = KDLKinematics(robot, base_link, "leftShoulderRollLink")
# print kdl_kin.forward(q)
# q_ik = kdl_kin.inverse(pose, q+0.5) # inverse kinematics
# if q_ik is not None:
#     pose_sol = kdl_kin.forward(q_ik) # should equal pose
# # J = kdl_kin.jacobian(q)
# print 'q:', q
# print 'q_ik:', q_ik
# print 'pose:', pose
# if q_ik is not None:
#     print 'pose_sol:', pose_sol
# # print 'J:', J
