#!/usr/bin/env python

import copy
import time
import rospy
import math
import numpy as np

from numpy import append

from ihmc_msgs.msg import ArmTrajectoryRosMessage
from ihmc_msgs.msg import HandTrajectoryRosMessage
from ihmc_msgs.msg import FootTrajectoryRosMessage
from ihmc_msgs.msg import OneDoFJointTrajectoryRosMessage
from ihmc_msgs.msg import TrajectoryPoint1DRosMessage
from ihmc_msgs.msg import ChestTrajectoryRosMessage
from ihmc_msgs.msg import SO3TrajectoryPointRosMessage
from ihmc_msgs.msg import WholeBodyTrajectoryRosMessage
from geometry_msgs.msg import Quaternion
from kinect_tracker.msg import JointRosMessage
from kinect_tracker.msg import JointListRosMessage
from tf.transformations import euler_from_quaternion
from tf.transformations import quaternion_from_euler
from helper import *
from pykdl_utils.joint_kinematics import JointKinematics 
from pykdl_utils.kdl_parser import kdl_tree_from_urdf_model
from urdf_parser_py.urdf import URDF, Robot

'''Single joints, like head, are LEFT
   Torso orientation is RIGHT
# "joint_side" enum values:
uint8 LEFT=0  # refers to the LEFT side of the person
uint8 RIGHT=1 # refers to the RIGHT side of the person

# "joint_type" enum values:
int64 HEAD=0
int64 SHOULDER=1
int64 ELBOW=2
int64 HAND=3
int64 TORSO=4
int64 HIP=5
int64 KNEE=6
int64 FOOT=7
int64 NECK=8
'''

ELBOW_BENT_UP = [0.0, 1.0, 0.0, -0.11, 0.0, 0.0, 0.0]
base_link = 'pelvis'
ROBOT_NAME = None

def sendArmTrajectory(val, side):
    msg = ArmTrajectoryRosMessage()
    msg.robot_side = side
    msg = appendTrajectoryPoint(msg, 2.0, val)
    msg.unique_id = -1
    # rospy.loginfo('publishing right trajectory')
    # armTrajectoryPublisher.publish(msg)
    return msg

def appendTrajectoryPoint(arm_trajectory, time, positions):
    if not arm_trajectory.joint_trajectory_messages:
        arm_trajectory.joint_trajectory_messages = [copy.deepcopy(OneDoFJointTrajectoryRosMessage()) for i in range(len(positions))]
    for i, pos in enumerate(positions):
        point = TrajectoryPoint1DRosMessage()
        point.time = time
        point.position = pos
        point.velocity = 0
        arm_trajectory.joint_trajectory_messages[i].trajectory_points.append(point)
    return arm_trajectory


def sendAngles(side, joints):
    angles = [0.0] * 7 #Angles Arm Right
    sR = angle_between(joints[0][8] - joints[side][1], joints[side][2] - joints[side][1]) #shoulder roll
    angles[1] = sR - math.pi
    if joints[side][2][1] < joints[side][1][1]: #or joints[side][2][2]:
        angles[1] = math.pi - sR
    angles[1] = clamp(angles[1], -1.265, 1.518)
    sP = math.asin(unit_vector(joints[side][2] - joints[side][1])[1]) #shoulder pitch
    angles[0] = abs(-(math.pi/2 +  sP)) 
    if whichSide(joints[side][1], joints[side][8], joints[0][4], joints[side][2]):
        angles[0] = -angles[0]
    if side == 1:
        print "pitch", angles[0]
        # if joints[side][3][1] > joints[side][1][1]:
            # angles[2] = -3.1
    angles[0] = clamp(angles[0], -2.84, 1.99)
    eP = angle_between(joints[side][1] - joints[side][2], joints[side][3] - joints[side][2]) #elbow
    angles[3] = clamp(math.pi - eP, -0.11, 2.17)

    if side == 0:
        angles[1] = -angles[1]
        angles[3] = -angles[3]
    # print "arms", side, angles
    return sendArmTrajectory(angles, side)

def sendChestAngles(joints):
    # print torsoOrient
    torso = joints[0][4]
    yaw = math.atan2(torso[0], torso[1])
    pitch = -math.atan2(torso[1], math.sqrt(torso[0] ** 2 + torso[2] ** 2)) + math.pi / 2
    #forward +, backward -
    pitch = abs(pitch)
    if pitch > 0.14:
        pitch -= 0.14
    if torso[2] < 0:
         pitch = -pitch
    pitch = clamp(pitch, -0.15, 0.50)
    yaw = abs(yaw)
    if yaw > 0.05:
        yaw -= 0.05
    elif torso[0] > 0:
        yaw = -yaw
    yaw = clamp(yaw, -0.15, 0.15)
    print pitch, yaw
    msg = ChestTrajectoryRosMessage()
    msg.unique_id = -1
    torsoOrient = quaternion_from_euler(yaw, 0.0, 0.0)
    q = Quaternion()
    q.x = torsoOrient[0]
    q.y = torsoOrient[1]
    q.z = torsoOrient[2]
    q.w = torsoOrient[3]
    traj = SO3TrajectoryPointRosMessage()
    traj.orientation = q;
    traj.time = 2;
    msg.taskspace_trajectory_points.append(traj)
    # chestTrajectoryPublisher.publish(msg)
    return msg

def getHandTrajectory(side):
    msg = HandTrajectoryRosMessage()
    msg.unique_id = 0
    msg.robot_side = side
    return msg

def getFootTrajectory(side):
    msg = FootTrajectoryRosMessage()
    msg.unique_id = 0
    msg.robot_side = side
    return msg


def sendBodyTrajector(joints):
    msg = WholeBodyTrajectoryRosMessage()
    msg.unique_id = -1
    rightArmMsg =  sendAngles(ArmTrajectoryRosMessage.RIGHT, joints)
    leftArmMsg  =  sendAngles(ArmTrajectoryRosMessage.LEFT, joints)
    chestMsg    =  sendChestAngles(joints)
    # print rightArmMsg.robot_side, leftArmMsg.robot_side
    msg.left_arm_trajectory_message = leftArmMsg
    msg.right_arm_trajectory_message = rightArmMsg
    msg.chest_trajectory_message = chestMsg
    msg.left_foot_trajectory_message = getFootTrajectory(ArmTrajectoryRosMessage.LEFT)
    msg.right_foot_trajectory_message = getFootTrajectory(ArmTrajectoryRosMessage.RIGHT)
    msg.left_hand_trajectory_message = getHandTrajectory(ArmTrajectoryRosMessage.LEFT)
    msg.right_hand_trajectory_message = getHandTrajectory(ArmTrajectoryRosMessage.RIGHT)
    bodyTrajectoryPublisher.publish(msg)

joints = []
count = 2
prevP = []
prevQ = []
kinList = []
jointsOfInterest = [('ShoulderPitchLink', 1), ('ShoulderYawLink', 2), ('Palm', 3), ('torso', 4)]
dataPath = '/home/stiliyan/catkin_ws/Project/data/'
def callback(data):
    print "yo"
    global count, prevP, joints, prevQ
    for joint in data.joint_list:
        if joint.w == 0.0:
            joints[joint.joint_side][joint.joint_type] = np.array([joint.x, joint.y, joint.z])
            # print euler_from_quaternion([joint.w, joint.x, joint.y, joint.z])

    #rescale joints
    chains = [[-1, 4, 8, 1, 2, 3], [4, 8, 0]]
    chainsL = [['pelvisToTorso', 'torsoToNeck','neckToShoulder', 'shoulderToElbow', 'elbowToPalm'], ['torsoToNeck', 'neckToHead']]
    unchangedJoints = copy.deepcopy(joints)
    for i in xrange(len(chains)):
        for j in xrange(len(chains[i])):
            if j == 0:
                continue
            #do left side
            cur = chains[i][j]
            prevI = chains[i][j - 1]
            if prevI ==  -1:
                prev = np.array([0, 0, 0])
                prevOld = np.array([0, 0, 0])
            else:
                prev = joints[0][prevI]
                prevOld = unchangedJoints[0][prevI]
            ln = scaleLengths[chainsL[i][j - 1]] * 1000
            if cur == 2:
                ln = 300
            joints[0][cur] = rescale(joints[0][cur], prevOld, prev, ln)
            if cur not in [0, 4, 8]: #do right side
                if prevI ==  -1:
                    prev = np.array([0, 0, 0])
                    prevOld = np.array([0, 0, 0])
                else:
                    if prevI not in [0, 4, 8]:
                        prev = joints[1][prevI]
                        prevOld = unchangedJoints[1][prevI]
                    else:
                        prev = joints[0][prevI]
                        prevOld = unchangedJoints[0][prevI]
                joints[1][cur] = rescale(joints[1][cur], prevOld, prev, ln)

    sendBodyTrajector(joints)
    rate.sleep()
    # tmp = raw_input("any key: ")
 
    # count = 0
    # for side in [0, 1]:
    #     for j, k in jointsOfInterest:
    #         if j == 'torso':
    #             if side == 1:
    #                 continue
    #         pose = kinList[count].forward()
    #         pos = np.array([pose[1, 3], pose[2, 3], pose[0, 3]])
    #         pos2 = joints[side][k] / 1000.
    #         dist = distance(pos, pos2)
    #         crp = np.linalg.norm(np.cross(pos, pos2))
    #         print kinList[count].end_link, dist, crp, pos, pos2
    #         if tmp != '1':
    #             with open(dataPath + "dist.csv", "a") as mf:
    #                 mf.write("%.3f," % (dist))
    #             with open(dataPath + "cross.csv", "a") as mf:
    #                 mf.write("%.3f," % (crp))
    #         count += 1
    # if tmp != '-1':
    #     with open(dataPath + "dist.csv", "a") as mf:
    #         mf.write('\n')
    #     with open(dataPath + "cross.csv", "a") as mf:
    #         mf.write('\n')
    

if __name__ == '__main__':
    try:
        rospy.init_node('copycat_node')
        # ROBOT_NAME = rospy.get_param('/ihmc_ros/robot_name')
        ROBOT_NAME = 'valkyrie'
        bodyTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/whole_body_trajectory".format(ROBOT_NAME), WholeBodyTrajectoryRosMessage, queue_size=1)
 
        rate = rospy.Rate(1) # 10hz
        robot = URDF.from_parameter_server()
        for side in ['left', 'right']:
            for j, k in jointsOfInterest:
                name = ''
                if j == 'torso':
                    if side == 'right':
                        continue
                    name = 'torso'
                else:
                    name = side + j
                # with open(dataPath + "dist.csv", "a") as mf:
                #     mf.write("%s," % (name))
                # with open(dataPath + "cross.csv", "a") as mf:
                #     mf.write("%s," % (name))
                kinList.append(JointKinematics(robot, base_link, name))

        # with open(dataPath + "dist.csv", "a") as mf:
        #     mf.write('\n')
        # with open(dataPath + "cross.csv", "a") as mf:
        #     mf.write('\n')

        # kin1 = JointKinematics(robot, base_link, 'rightPalm')
        # make sure the simulation is running otherwise wait
        # if armTrajectoryPublisher.get_num_connections() == 0:
        #     rospy.loginfo('waiting for subscriber...')
        #     while armTrajectoryPublisher.get_num_connections() == 0:
        #         rate.sleep()
        joints.append([np.zeros(3)] * 9)
        joints.append([np.zeros(3)] * 9)
        rospy.Subscriber("tracker", JointListRosMessage, callback, queue_size=1)
        rospy.spin()

    except rospy.ROSInterruptException:
        pass
