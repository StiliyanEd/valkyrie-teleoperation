import numpy as np 
import numpy.linalg as la
from math import pi, sqrt

scaleLengths = {'pelvisToTorso' : 0.04693,
'torsoToNeck' : 0.339062,
'neckToHead' : 0.225702,
'neckToShoulder' : 0.065598,
'shoulderToElbow' : 0.2499,
'elbowToPalm' : 0.457917
}

#vec: the joint to be rescaled
#prev: the joint before it in the chain
#length: the length of the link
def rescale(vec, prevOld, prev, length):
    return unit_vector(vec - prevOld) * length + prev

def constrainAngle(x):
    x = (x + pi) % pi
    if x < 0:
        x += (pi * 2)
    return x - pi

def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    return vector / np.linalg.norm(vector)

def angle_between(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2'::

            >>> angle_between((1, 0, 0), (0, 1, 0))
            1.5707963267948966
            >>> angle_between((1, 0, 0), (1, 0, 0))
            0.0
            >>> angle_between((1, 0, 0), (-1, 0, 0))
            3.141592653589793
    """
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))

def clamp(n, minn, maxn):
    return max(min(maxn, n), minn)

def distance(v1, v2):
    return sqrt(np.sum((v1 -v2) ** 2))

def whichSide(A, B, C, X):
    B1 = B - A
    C1 = C - A
    X1 = X - A
    M = np.mat([B1, C1, X1])
    return np.linalg.det(M) > 0